var RoleController = require('./../controllers/RoleController');
var ZipcodeController = require('./../controllers/ZipcodeController');
var CountryController = require('./../controllers/CountryController');
var DeviseController = require('./../controllers/DeviseController');

Promise.all([
	RoleController.updateRoleAndRight(),
	ZipcodeController.updateZipcode(),
	CountryController.updateCountries(),
    DeviseController.updateDevises(),
	])
.then(
    function (data) {
        process.exit()
    },function (err) {
        console.log(err);
        process.exit()
    }
);