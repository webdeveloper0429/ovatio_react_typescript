"use script";

var Sequelize = require('sequelize');

var model = {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	manifestationId: {
		type: Sequelize.INTEGER,
	},
	companyId: {
		type: Sequelize.INTEGER
	},
	rate: {
		type: Sequelize.TEXT
	},
	createdAt: {
		type: Sequelize.DATE,
		defaultValue : new Date()
	},
	updatedAt: {
		type: Sequelize.DATE,
		defaultValue : new Date()
	}
};

module.exports = function(sequelize, DataTypes) {
	var coInsurer = sequelize.define('co_insurers', model);
	coInsurer.associate = function(models){
		models.co_insurers.belongsTo(models.manifestation, {as: 'coInsurerList', foreignKey: 'manifestationId'});
	}
	return coInsurer;
};


