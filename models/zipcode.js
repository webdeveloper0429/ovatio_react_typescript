"use script";

var Sequelize = require('sequelize');

var model = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    codepays: {
        type: Sequelize.TEXT,
    },
    cp: {
        type: Sequelize.TEXT,
    },
    ville: {
        type: Sequelize.TEXT,
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue : new Date()
    },
    updatedAt: {
        type: Sequelize.DATE,
        defaultValue : new Date()
    }
};

module.exports = function(sequelize, DataTypes) {
    var Zipcode = sequelize.define('zipcode', model);
    return Zipcode;
};