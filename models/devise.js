'use strict';
module.exports = function(sequelize, DataTypes) {
  var devise = sequelize.define('devise', {
    name: DataTypes.STRING,
    symbole: DataTypes.STRING,
    code: DataTypes.STRING,
    isActive: DataTypes.INTEGER,
    order: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return devise;
};