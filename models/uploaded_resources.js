"use script";

var Sequelize = require('sequelize');

var model = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    fieldname: {
        type: Sequelize.STRING,
    },
    originalname: {
        type: Sequelize.STRING
    },
    encoding: {
        type: Sequelize.STRING
    },
    mimetype: {
        type: Sequelize.STRING
    },
    destination: {
        type: Sequelize.STRING
    },
    filename: {
        type: Sequelize.STRING
    },
    path: {
        type: Sequelize.STRING
    },
    size: {
        type: Sequelize.STRING
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue : new Date()
    },
    updatedAt: {
        type: Sequelize.DATE,
        defaultValue : new Date()
    }
};

module.exports = function(sequelize, DataTypes) {
	var Uploaded_resource = sequelize.define('uploaded_resource', model,{
        indexes: [
            {
                unique: true,
                fields: ['filename']
            }
        ]
    });
	return Uploaded_resource;
};
