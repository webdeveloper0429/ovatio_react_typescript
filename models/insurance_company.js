"use script";

var Sequelize = require('sequelize');

var model = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: Sequelize.BOOLEAN,
        default: true,
    },
    businessName: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    rate: {
        type: Sequelize.FLOAT,
    },
    siret: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    brokerId: {
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.STRING
    },
    zipCode: {
        type: Sequelize.STRING
    },
    city: {
        type: Sequelize.STRING
    },
    countryId: {
        type: Sequelize.INTEGER
    },
    addressAccounting: {
        type: Sequelize.STRING
    },
    zipCodeAccounting: {
        type: Sequelize.STRING
    },
    cityAccounting: {
        type: Sequelize.STRING
    },
    countryAccountingId: {
        type: Sequelize.INTEGER
    },
    legalNotice: {
        type: Sequelize.STRING
    },
    avatarId:{
        type: Sequelize.STRING
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: new Date()
    },
    updatedAt: {
        type: Sequelize.DATE,
        defaultValue: new Date()
    }
};

module.exports = function (sequelize, DataTypes) {
    var InsuranceCompany = sequelize.define('insuranceCompany', model, {
        tableName: 'insurance_companies'
    });
    InsuranceCompany.associate = function (models) {
        InsuranceCompany.belongsToMany(models.user, {
            as: 'contacts',
            through: 'insurance_companies_users',
            foreignKey: 'insuranceCompanyId'
        })
        InsuranceCompany.hasMany(models.manifestation, {
            as: 'manifestationsAsMainInsurer',
            foreignKey: 'mainInsurerId'
        })
        InsuranceCompany.hasMany(models.manifestation, {
            as: 'manifestationsAsCoInsurer',
            foreignKey: 'coInsurerId'
        })
        InsuranceCompany.hasMany(models.version, {
            foreignKey: 'insurance_id'
        })

        InsuranceCompany.belongsTo(models.country);
        InsuranceCompany.belongsTo(models.country, { as: 'countryAccounting'});
    };
    return InsuranceCompany;
};
