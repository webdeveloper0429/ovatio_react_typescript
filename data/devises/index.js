module.exports = [
    {"name": "EURO", "symbole": "€", "code": "EUR", "isActive": 1, "order" : 1},
    {"name": "US DOLLAR", "symbole": "$", "code": "USD", "isActive": 1, "order" : 2},
    {"name": "CANADIAN DOLLAR", "code": "CAD", "isActive": 1, "order" : 3},
    {"name": "AUSTRALIAN DOLLAR", "code": "AUD", "isActive": 1, "order" : 4},
    {"name": "JAPANESE YEN", "code": "JPY", "isActive": 1, "order" : 5},
    {"name": "BRITISH POUND", "code": "GBP", "isActive": 1, "order" : 6},
    {"name": "FRANC SUISSE", "code": "CHF", "isActive": 1, "order" : 7},
    {"name": "CHINESE YUAN RENMINBI", "code": "CNY", "isActive": 1, "order" : 8},
    {"name": "SINGAPORE DOLLAR", "code": "SGD", "isActive": 1, "order" : 9},
    {"name": "HONG KONG DOLLAR", "code": "HKD", "isActive": 1, "order" : 10},
    {"name": "RUSSIAN RUBLE", "code": "RUB", "isActive": 1, "order" : 11},
    {"name": "NORVEGIAN KRONE", "code": "NOK", "isActive": 1, "order" : 12},
    {"name": "SWEDISH KRONE", "code": "SEK", "isActive": 1, "order" : 12},
    {"name": "DANISH KRONE", "code": "DKK", "isActive": 1, "order" : 13}
];