module.exports = [

    [
        {
            "country_id": 1,
            "country_code": "AF",
            "country_value": "Afghanistan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 2,
            "country_code": "AL",
            "country_value": "Albania",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 3,
            "country_code": "AQ",
            "country_value": "Antarctica",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 4,
            "country_code": "DZ",
            "country_value": "Algeria",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 5,
            "country_code": "AS",
            "country_value": "American Samoa",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 6,
            "country_code": "AD",
            "country_value": "Andorra",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 7,
            "country_code": "AO",
            "country_value": "Angola",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 8,
            "country_code": "AG",
            "country_value": "Antigua and Barbuda",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 9,
            "country_code": "AZ",
            "country_value": "Azerbaijan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 10,
            "country_code": "AR",
            "country_value": "Argentina",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 11,
            "country_code": "AU",
            "country_value": "Australia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 12,
            "country_code": "AT",
            "country_value": "Austria",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 13,
            "country_code": "BS",
            "country_value": "Bahamas",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 14,
            "country_code": "BH",
            "country_value": "Bahrain",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 15,
            "country_code": "BD",
            "country_value": "Bangladesh",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 16,
            "country_code": "AM",
            "country_value": "Armenia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 17,
            "country_code": "BB",
            "country_value": "Barbados",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 18,
            "country_code": "BE",
            "country_value": "Belgium",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 19,
            "country_code": "BM",
            "country_value": "Bermuda",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 20,
            "country_code": "BT",
            "country_value": "Bhutan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 21,
            "country_code": "BO",
            "country_value": "Bolivia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 22,
            "country_code": "BA",
            "country_value": "Bosnia and Herzegovina",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 23,
            "country_code": "BW",
            "country_value": "Botswana",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 24,
            "country_code": "BV",
            "country_value": "Bouvet Island",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 25,
            "country_code": "BR",
            "country_value": "Brazil",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 26,
            "country_code": "BZ",
            "country_value": "Belize",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 27,
            "country_code": "IO",
            "country_value": "British Indian Ocean Territory",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 28,
            "country_code": "SB",
            "country_value": "Solomon Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 29,
            "country_code": "VG",
            "country_value": "British Virgin Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 30,
            "country_code": "BN",
            "country_value": "Brunei Darussalam",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 31,
            "country_code": "BG",
            "country_value": "Bulgaria",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 32,
            "country_code": "MM",
            "country_value": "Myanmar",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 33,
            "country_code": "BI",
            "country_value": "Burundi",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 34,
            "country_code": "BY",
            "country_value": "Belarus",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 35,
            "country_code": "KH",
            "country_value": "Cambodia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 36,
            "country_code": "CM",
            "country_value": "Cameroon",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 37,
            "country_code": "CA",
            "country_value": "Canada",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 38,
            "country_code": "CV",
            "country_value": "Cape Verde",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 39,
            "country_code": "KY",
            "country_value": "Cayman Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 40,
            "country_code": "CF",
            "country_value": "Central African",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 41,
            "country_code": "LK",
            "country_value": "Sri Lanka",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 42,
            "country_code": "TD",
            "country_value": "Chad",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 43,
            "country_code": "CL",
            "country_value": "Chile",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 44,
            "country_code": "CN",
            "country_value": "China",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 45,
            "country_code": "TW",
            "country_value": "Taiwan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 46,
            "country_code": "CX",
            "country_value": "Christmas Island",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 47,
            "country_code": "CC",
            "country_value": "Cocos (Keeling) Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 48,
            "country_code": "CO",
            "country_value": "Colombia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 49,
            "country_code": "KM",
            "country_value": "Comoros",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 50,
            "country_code": "YT",
            "country_value": "Mayotte",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 51,
            "country_code": "CG",
            "country_value": "Republic of the Congo",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 52,
            "country_code": "CD",
            "country_value": "The Democratic Republic Of The Congo",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 53,
            "country_code": "CK",
            "country_value": "Cook Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 54,
            "country_code": "CR",
            "country_value": "Costa Rica",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 55,
            "country_code": "HR",
            "country_value": "Croatia",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 56,
            "country_code": "CU",
            "country_value": "Cuba",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 57,
            "country_code": "CY",
            "country_value": "Cyprus",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 58,
            "country_code": "CZ",
            "country_value": "Czech Republic",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 59,
            "country_code": "BJ",
            "country_value": "Benin",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 60,
            "country_code": "DK",
            "country_value": "Denmark",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 61,
            "country_code": "DM",
            "country_value": "Dominica",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 62,
            "country_code": "DO",
            "country_value": "Dominican Republic",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 63,
            "country_code": "EC",
            "country_value": "Ecuador",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 64,
            "country_code": "SV",
            "country_value": "El Salvador",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 65,
            "country_code": "GQ",
            "country_value": "Equatorial Guinea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 66,
            "country_code": "ET",
            "country_value": "Ethiopia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 67,
            "country_code": "ER",
            "country_value": "Eritrea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 68,
            "country_code": "EE",
            "country_value": "Estonia",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 69,
            "country_code": "FO",
            "country_value": "Faroe Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 70,
            "country_code": "FK",
            "country_value": "Falkland Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 71,
            "country_code": "GS",
            "country_value": "South Georgia and the South Sandwich Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 72,
            "country_code": "FJ",
            "country_value": "Fiji",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 73,
            "country_code": "FI",
            "country_value": "Finland",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 74,
            "country_code": "AX",
            "country_value": "Ã…land Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 75,
            "country_code": "FR",
            "country_value": "France",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 76,
            "country_code": "GF",
            "country_value": "French Guiana",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 77,
            "country_code": "PF",
            "country_value": "French Polynesia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 78,
            "country_code": "TF",
            "country_value": "French Southern Territories",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 79,
            "country_code": "DJ",
            "country_value": "Djibouti",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 80,
            "country_code": "GA",
            "country_value": "Gabon",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 81,
            "country_code": "GE",
            "country_value": "Georgia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 82,
            "country_code": "GM",
            "country_value": "Gambia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 83,
            "country_code": "PS",
            "country_value": "Occupied Palestinian Territory",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 84,
            "country_code": "DE",
            "country_value": "Germany",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 85,
            "country_code": "GH",
            "country_value": "Ghana",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 86,
            "country_code": "GI",
            "country_value": "Gibraltar",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 87,
            "country_code": "KI",
            "country_value": "Kiribati",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 88,
            "country_code": "GR",
            "country_value": "Greece",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 89,
            "country_code": "GL",
            "country_value": "Greenland",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 90,
            "country_code": "GD",
            "country_value": "Grenada",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 91,
            "country_code": "GP",
            "country_value": "Guadeloupe",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 92,
            "country_code": "GU",
            "country_value": "Guam",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 93,
            "country_code": "GT",
            "country_value": "Guatemala",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 94,
            "country_code": "GN",
            "country_value": "Guinea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 95,
            "country_code": "GY",
            "country_value": "Guyana",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 96,
            "country_code": "HT",
            "country_value": "Haiti",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 97,
            "country_code": "HM",
            "country_value": "Heard Island and McDonald Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 98,
            "country_code": "VA",
            "country_value": "Vatican City State",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 99,
            "country_code": "HN",
            "country_value": "Honduras",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 100,
            "country_code": "HK",
            "country_value": "Hong Kong",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 101,
            "country_code": "HU",
            "country_value": "Hungary",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 102,
            "country_code": "IS",
            "country_value": "Iceland",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 103,
            "country_code": "IN",
            "country_value": "India",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 104,
            "country_code": "ID",
            "country_value": "Indonesia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 105,
            "country_code": "IR",
            "country_value": "Islamic Republic of Iran",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 106,
            "country_code": "IQ",
            "country_value": "Iraq",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 107,
            "country_code": "IE",
            "country_value": "Ireland",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 108,
            "country_code": "IL",
            "country_value": "Israel",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 109,
            "country_code": "IT",
            "country_value": "Italy",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 110,
            "country_code": "CI",
            "country_value": "CÃ´te d'Ivoire",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 111,
            "country_code": "JM",
            "country_value": "Jamaica",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 112,
            "country_code": "JP",
            "country_value": "Japan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 113,
            "country_code": "KZ",
            "country_value": "Kazakhstan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 114,
            "country_code": "JO",
            "country_value": "Jordan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 115,
            "country_code": "KE",
            "country_value": "Kenya",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 116,
            "country_code": "KP",
            "country_value": "Democratic People's Republic of Korea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 117,
            "country_code": "KR",
            "country_value": "Republic of Korea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 118,
            "country_code": "KW",
            "country_value": "Kuwait",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 119,
            "country_code": "KG",
            "country_value": "Kyrgyzstan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 120,
            "country_code": "LA",
            "country_value": "Lao People's Democratic Republic",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 121,
            "country_code": "LB",
            "country_value": "Lebanon",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 122,
            "country_code": "LS",
            "country_value": "Lesotho",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 123,
            "country_code": "LV",
            "country_value": "Latvia",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 124,
            "country_code": "LR",
            "country_value": "Liberia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 125,
            "country_code": "LY",
            "country_value": "Libyan Arab Jamahiriya",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 126,
            "country_code": "LI",
            "country_value": "Liechtenstein",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 127,
            "country_code": "LT",
            "country_value": "Lithuania",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 128,
            "country_code": "LU",
            "country_value": "Luxembourg",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 129,
            "country_code": "MO",
            "country_value": "Macao",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 130,
            "country_code": "MG",
            "country_value": "Madagascar",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 131,
            "country_code": "MW",
            "country_value": "Malawi",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 132,
            "country_code": "MY",
            "country_value": "Malaysia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 133,
            "country_code": "MV",
            "country_value": "Maldives",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 134,
            "country_code": "ML",
            "country_value": "Mali",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 135,
            "country_code": "MT",
            "country_value": "Malta",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 136,
            "country_code": "MQ",
            "country_value": "Martinique",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 137,
            "country_code": "MR",
            "country_value": "Mauritania",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 138,
            "country_code": "MU",
            "country_value": "Mauritius",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 139,
            "country_code": "MX",
            "country_value": "Mexico",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 140,
            "country_code": "MC",
            "country_value": "Monaco",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 141,
            "country_code": "MN",
            "country_value": "Mongolia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 142,
            "country_code": "MD",
            "country_value": "Republic of Moldova",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 143,
            "country_code": "MS",
            "country_value": "Montserrat",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 144,
            "country_code": "MA",
            "country_value": "Morocco",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 145,
            "country_code": "MZ",
            "country_value": "Mozambique",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 146,
            "country_code": "OM",
            "country_value": "Oman",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 147,
            "country_code": "NA",
            "country_value": "Namibia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 148,
            "country_code": "NR",
            "country_value": "Nauru",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 149,
            "country_code": "NP",
            "country_value": "Nepal",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 150,
            "country_code": "NL",
            "country_value": "Netherlands",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 151,
            "country_code": "AN",
            "country_value": "Netherlands Antilles",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 152,
            "country_code": "AW",
            "country_value": "Aruba",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 153,
            "country_code": "NC",
            "country_value": "New Caledonia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 154,
            "country_code": "VU",
            "country_value": "Vanuatu",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 155,
            "country_code": "NZ",
            "country_value": "New Zealand",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 156,
            "country_code": "NI",
            "country_value": "Nicaragua",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 157,
            "country_code": "NE",
            "country_value": "Niger",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 158,
            "country_code": "NG",
            "country_value": "Nigeria",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 159,
            "country_code": "NU",
            "country_value": "Niue",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 160,
            "country_code": "NF",
            "country_value": "Norfolk Island",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 161,
            "country_code": "NO",
            "country_value": "Norway",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 162,
            "country_code": "MP",
            "country_value": "Northern Mariana Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 163,
            "country_code": "UM",
            "country_value": "United States Minor Outlying Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 164,
            "country_code": "FM",
            "country_value": "Federated States of Micronesia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 165,
            "country_code": "MH",
            "country_value": "Marshall Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 166,
            "country_code": "PW",
            "country_value": "Palau",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 167,
            "country_code": "PK",
            "country_value": "Pakistan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 168,
            "country_code": "PA",
            "country_value": "Panama",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 169,
            "country_code": "PG",
            "country_value": "Papua New Guinea",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 170,
            "country_code": "PY",
            "country_value": "Paraguay",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 171,
            "country_code": "PE",
            "country_value": "Peru",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 172,
            "country_code": "PH",
            "country_value": "Philippines",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 173,
            "country_code": "PN",
            "country_value": "Pitcairn",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 174,
            "country_code": "PL",
            "country_value": "Poland",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 175,
            "country_code": "PT",
            "country_value": "Portugal",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 176,
            "country_code": "GW",
            "country_value": "Guinea-Bissau",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 177,
            "country_code": "TL",
            "country_value": "Timor-Leste",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 178,
            "country_code": "PR",
            "country_value": "Puerto Rico",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 179,
            "country_code": "QA",
            "country_value": "Qatar",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 180,
            "country_code": "RE",
            "country_value": "RÃ©union",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 181,
            "country_code": "RO",
            "country_value": "Romania",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 182,
            "country_code": "RU",
            "country_value": "Russian Federation",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 183,
            "country_code": "RW",
            "country_value": "Rwanda",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 184,
            "country_code": "SH",
            "country_value": "Saint Helena",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 185,
            "country_code": "KN",
            "country_value": "Saint Kitts and Nevis",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 186,
            "country_code": "AI",
            "country_value": "Anguilla",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 187,
            "country_code": "LC",
            "country_value": "Saint Lucia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 188,
            "country_code": "PM",
            "country_value": "Saint-Pierre and Miquelon",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 189,
            "country_code": "VC",
            "country_value": "Saint Vincent and the Grenadines",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 190,
            "country_code": "SM",
            "country_value": "San Marino",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 191,
            "country_code": "ST",
            "country_value": "Sao Tome and Principe",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 192,
            "country_code": "SA",
            "country_value": "Saudi Arabia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 193,
            "country_code": "SN",
            "country_value": "Senegal",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 194,
            "country_code": "SC",
            "country_value": "Seychelles",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 195,
            "country_code": "SL",
            "country_value": "Sierra Leone",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 196,
            "country_code": "SG",
            "country_value": "Singapore",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 197,
            "country_code": "SK",
            "country_value": "Slovakia",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 198,
            "country_code": "VN",
            "country_value": "Vietnam",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 199,
            "country_code": "SI",
            "country_value": "Slovenia",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 200,
            "country_code": "SO",
            "country_value": "Somalia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 201,
            "country_code": "ZA",
            "country_value": "South Africa",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 202,
            "country_code": "ZW",
            "country_value": "Zimbabwe",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 203,
            "country_code": "ES",
            "country_value": "Spain",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 204,
            "country_code": "EH",
            "country_value": "Western Sahara",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 205,
            "country_code": "SD",
            "country_value": "Sudan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 206,
            "country_code": "SR",
            "country_value": "Suriname",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 207,
            "country_code": "SJ",
            "country_value": "Svalbard and Jan Mayen",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 208,
            "country_code": "SZ",
            "country_value": "Swaziland",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 209,
            "country_code": "SE",
            "country_value": "Sweden",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 210,
            "country_code": "CH",
            "country_value": "Switzerland",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 211,
            "country_code": "SY",
            "country_value": "Syrian Arab Republic",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 212,
            "country_code": "TJ",
            "country_value": "Tajikistan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 213,
            "country_code": "TH",
            "country_value": "Thailand",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 214,
            "country_code": "TG",
            "country_value": "Togo",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 215,
            "country_code": "TK",
            "country_value": "Tokelau",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 216,
            "country_code": "TO",
            "country_value": "Tonga",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 217,
            "country_code": "TT",
            "country_value": "Trinidad and Tobago",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 218,
            "country_code": "AE",
            "country_value": "United Arab Emirates",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 219,
            "country_code": "TN",
            "country_value": "Tunisia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 220,
            "country_code": "TR",
            "country_value": "Turkey",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 221,
            "country_code": "TM",
            "country_value": "Turkmenistan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 222,
            "country_code": "TC",
            "country_value": "Turks and Caicos Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 223,
            "country_code": "TV",
            "country_value": "Tuvalu",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 224,
            "country_code": "UG",
            "country_value": "Uganda",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 225,
            "country_code": "UA",
            "country_value": "Ukraine",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 226,
            "country_code": "MK",
            "country_value": "The Former Yugoslav Republic of Macedonia",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 227,
            "country_code": "EG",
            "country_value": "Egypt",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 228,
            "country_code": "GB",
            "country_value": "United Kingdom",
            "country_order": 0,
            "country_eu": 1
        },
        {
            "country_id": 229,
            "country_code": "IM",
            "country_value": "Isle of Man",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 230,
            "country_code": "TZ",
            "country_value": "United Republic Of Tanzania",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 231,
            "country_code": "US",
            "country_value": "United States",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 232,
            "country_code": "VI",
            "country_value": "U.S. Virgin Islands",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 233,
            "country_code": "BF",
            "country_value": "Burkina Faso",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 234,
            "country_code": "UY",
            "country_value": "Uruguay",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 235,
            "country_code": "UZ",
            "country_value": "Uzbekistan",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 236,
            "country_code": "VE",
            "country_value": "Venezuela",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 237,
            "country_code": "WF",
            "country_value": "Wallis and Futuna",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 238,
            "country_code": "WS",
            "country_value": "Samoa",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 239,
            "country_code": "YE",
            "country_value": "Yemen",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 240,
            "country_code": "CS",
            "country_value": "Serbia and Montenegro",
            "country_order": 0,
            "country_eu": "NULL"
        },
        {
            "country_id": 241,
            "country_code": "ZM",
            "country_value": "Zambia",
            "country_order": 0,
            "country_eu": "NULL"
        }
    ]
    
]