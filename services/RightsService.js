/**
 * @module Service Right
 */


var models  = require('./../models');

/**
 * Find right or create it
 * @param right {json} - json data of right
 * @returns {Promise}
 */
module.exports.findOrCreate = function (right) {
	return  models.right.findOrCreate(
		{
			where: {label: right.label},
			defaults: {
				nameFr: right.nameFr
			}
		}).then(
			function (data) {
				rightDao = data[0];
				rightDao.nameFr = right.nameFr;
				return rightDao.save();
			}
	)
};


/**
 * Find right by label
 * @param label {string} - label of right
 * @returns {Promise}
 */
module.exports.findByLabel = function (label) {
	return  models.right.find(
		{
			where: {label: label},
			defaults: {
				nameFr: right.nameFr
			}
		}).then(
			function (data) {
				rightDao = data[0];
				rightDao.nameFr = right.nameFr;
				return rightDao.save();
			}
	)
};
