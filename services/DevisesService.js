/**
 * @module Service Devises
 */
var models  = require('./../models');

/**
 * find or create devises
 * @param data {json} - json data of devises
 * @returns {Promise}
 */
module.exports.findOrCreate = function (data) {
	return models.devise.findOrCreate({
		where: {code: data.code},
			defaults: {
				name: data.name,
				symbole: data.symbole,
				code: data.code,
				isActive: data.isActive,
				order: data.order,
			}
		}
	)
};
