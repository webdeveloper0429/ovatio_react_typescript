/**
 * @module Service Zipcode
 */
var models  = require('./../models');

/**
 * find or create zipcode
 * @param data {json} - json data of zipcode
 * @returns {Promise}
 */
module.exports.findOrCreate = function (data) {
	return models.zipcode.findOrCreate({
		where: {cp: data.cp},
			defaults: {
				id: data.id,
				codepays: data.codepays,
				cp: data.cp,
				ville: data.ville,
			}
		}
	)
};
