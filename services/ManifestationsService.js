/**
 * @module Service Manifestation
 */

var models = require('./../models');
var User = require('./../models/user');
var sha1 = require('sha1');
var sequelize = require('sequelize');
var ManifestationsPeopleService = require('./ManifestationsPeopleService')
var ManifestationsTechnicianService = require('./ManifestationsTechnicianService')
var ManifestationsCoInsurerService = require('./ManifestationsCoInsurerService')

const TYPE_PROJECT = 0
const TYPE_CONTRACT = 1
const ONGOING = 1
const CANCELED = 2
const TRANSFORMED = 3

/**
 * Hydrate function of the manfiestation
 * @param dao {object} - dao of the manifestation
 * @param data {json} - json of the hydrate manifestation
 * @returns {*}
 */
function hydrate(dao, data) {
	"use strict";
	if (dao && data) {
		dao.eventName = data.eventName || null;
		dao.totalDeclaredBudget = data.totalDeclaredBudget || null;
		dao.type = data.type || null;
		dao.accountManagerId = data.accountManagerId || null;
		dao.policyLanguage = data.policyLanguage || null;
		dao.currency = data.currency || null;
		dao.isBusinessProvider = data.isBusinessProvider || null;
		dao.businessProviderId = data.businessProviderId || null;
		dao.businessProviderFeesRate = data.businessProviderFeesRate || null;
		dao.hasAdditionalInsuredParty = data.hasAdditionalInsuredParty || null;
		dao.additionalInsuredPartyName = data.additionalInsuredPartyName || null;
		dao.specialConditions = data.specialConditions || null;
		dao.endOfGuaranteeRule = data.endOfGuaranteeRule || null;
		dao.endOfGuaranteeRuleOnePercentDurationOfTheShow = data.endOfGuaranteeRuleOnePercentDurationOfTheShow || null;
		dao.endOfGuaranteeRuleOneHasPlayed = data.endOfGuaranteeRuleOneHasPlayed || null;
		dao.endOfGuaranteeRuleOneHasOption = data.endOfGuaranteeRuleOneHasOption || null;
		dao.endOfGuaranteeRuleTwoActNumberIsOver = data.endOfGuaranteeRuleTwoActNumberIsOver || null;
		dao.endOfGuaranteeRuleThreeTimeIsOver = data.endOfGuaranteeRuleThreeTimeIsOver || null;
		dao.endOfGuaranteeRuleFourCondition = data.endOfGuaranteeRuleFourCondition || null;
		dao.isCoInsurance = data.isCoInsurance || null;
		dao.mainInsurerId = data.mainInsurerId || null;
		dao.rateInsurer = data.rateInsurer || null;
		dao.coInsurerId = data.coInsurerId || null;
		dao.hasExpert = data.hasExpert || null;
		dao.expertId = data.expertId || null;
		dao.averageClause = data.averageClause || null;
		dao.averagePremiumClause = data.averagePremiumClause || null;
		dao.contractualIndemnityLimit = data.contractualIndemnityLimit || null;
		dao.contractualIndemnityLimitRate = data.contractualIndemnityLimitRate || null;
		dao.averageFinancialCommitment = data.averageFinancialCommitment || null;
		dao.subscriptionDeadLine = data.subscriptionDeadLine || new Date();
		dao.inceptionDate = data.inceptionDate || new Date();
		dao.eventCategory = data.eventCategory || null;
		dao.eventStartDate = data.eventStartDate || new Date();
		dao.eventEndDate = data.eventEndDate || null;
		dao.artist = data.artist || null;
		dao.dateType = data.dateType || null;
		dao.numberOfInsuredDates = data.numberOfInsuredDates || null;
		dao.isPersonUnavailability = data.isPersonUnavailability || null;
		dao.numberPersonInsured = data.numberPersonInsured || null;
		dao.numberTechnicianInsured = data.numberTechnicianInsured || null;
		dao.isTechnicianInsuredNamed = data.isTechnicianInsuredNamed || null;
		dao.totalBudget = data.totalBudget || null;
		dao.unavailabilityRate = data.unavailabilityRate || null;
		dao.hasSpecialDispositionExtension = data.hasSpecialDispositionExtension || null;
		dao.specialDisposition = data.specialDisposition || null;
		dao.hasUnavailabiliytExtension = data.hasUnavailabiliytExtension || null;
		dao.isFeeDeducted = data.isFeeDeducted || null;
		dao.artisteFee = data.artisteFee || null;
		dao.insuredBudget = data.insuredBudget || null;
		dao.hasBadWeatherExtension = data.hasBadWeatherExtension || null;
		dao.badWeatherRate = data.badWeatherRate || null;
		dao.percentBudgetToBadWeather = data.percentBudgetToBadWeather || null;
		dao.weatherBudget = data.weatherBudget || null;
		dao.hasCrowShortageExtension = data.hasCrowShortageExtension || null;
		dao.franchise = data.franchise || null;
		dao.hasBombingExtension = data.hasBombingExtension || null;
		dao.bombingRate = data.bombingRate || null;
		dao.bombingNumberOfDay = data.bombingNumberOfDay || null;
		dao.bombingNumberOfKilometre = data.bombingNumberOfKilometre || null;
		dao.bombingHasOptionThreat = data.bombingHasOptionThreat || null;
		dao.bombingHasOptionRecommendation = data.bombingHasOptionRecommendation || null;
		dao.hasMoralReasonExtension = data.hasMoralReasonExtension || null;
		dao.hasExpertFeeExtension = data.hasExpertFeeExtension || null;
		dao.expertWarrantyAmount = data.expertWarrantyAmount || null;
		dao.expertFranchise = data.expertFranchise || null;
		dao.BNSType = data.BNSType || null;
		dao.applicableFranchise = data.applicableFranchise || null;
		dao.comprehensiveInsuranceRate = data.comprehensiveInsuranceRate || null;
		dao.BNS = data.BNS || null;
		dao.franchiseAmount = data.franchiseAmount || null;
		dao.exposedBudgetUnavailability = data.exposedBudgetUnavailability || null;
		dao.exposedBudgetBadWeather = data.exposedBudgetBadWeather || null;
		dao.exposedBudgetBombing = data.exposedBudgetBombing || null;
		dao.excludingTaxRateUnavailability = data.excludingTaxRateUnavailability || null;
		dao.excludingTaxRateBadWeather = data.excludingTaxRateBadWeather || null;
		dao.excludingTaxRateBombing = data.excludingTaxRateBombing || null;
		dao.ovationCommission = data.ovationCommission || null;
		dao.ovationBSNCommission = data.ovationBSNCommission || null;
		dao.isTechnicianIndispensable = data.isTechnicianIndispensable || null;
		dao.taxRate = data.taxRate || null;
		dao.category = data.category;
		dao.customCategory = data.customCategory || null;
		dao.eventDate = data.eventDate;
		dao.eventEndDate = data.eventEndDate;
		dao.artist = data.artist;
		dao.dateType = data.dateType;
		dao.numberInsured = data.numberInsured;
		dao.budget = data.budget;
		dao.clientId = data.clientId;
		dao.status = data.status;
		dao.updatedAt = new Date();
		dao.projectNumber = data.projectNumber || null;
		dao.contractNumber = data.contractNumber || null;
		return dao;
	} else {}
}

/**
 * Get all manifestation
 * @param query {json} - json of the query (offset, limit, order, where)
 * @returns {Promise.<Array.<Manifestation>>}
 */
module.exports.readAll = function(query) {
	var queryParams = {};
	if (query['offset'])
		queryParams.offset = parseInt(query['offset']);
	if (query['limit'])
		queryParams.limit = parseInt(query['limit']);
	var sortable = ['deletedAt', 'businessName', 'address', 'zipCode', 'city'];
	if (sortable.indexOf(query['sort']) > -1)
		queryParams.order = [
			[
				query['sort'], query['order'] === 'DESC'
					? 'DESC'
					: 'ASC'
			]
		];
	if (query['search'])
		queryParams.where = {
			eventName: {
				$like: '%' + query['search'] + '%'
			}
		};
	return models.manifestation.findAll(queryParams);
};

/**
 * make incule query
 * @param model {model} - model of sequelize
 * @param asTag {string} - string of attribute linked
 * @returns {json}
 */
function prepareInclude(model, asTag) {
	return {
		attributes: ['businessName'],
		model: model,
		as: asTag,
		include: [
			{
				attributes: [],
				model: models.user,
				as: 'contacts',
			}
		]
	}
}

/**
 * make queryParam object
 * @param role {json} - role data
 * @param userId {number} - id of user
 * @returns {json}
 */
function prepare(role, userId) {
	let model;
	var queryParams = {
		where: {},
		include: [],
		subQuery: false // apply the LIMIT to the whole response instead of nesting it
	};
	if (role.label === "COMPANY") {
		queryParams.where['$or'] = [
			{'$mainInsurer.contacts.id$': {$eq: userId}},
			{'$coInsurer.contacts.id$': {$eq: userId}}
		]
		queryParams.include.push(prepareInclude(models.insuranceCompany, 'mainInsurer'))
		queryParams.include.push(prepareInclude(models.insuranceCompany, 'coInsurer'))
	}
	if (role.label === "EXPERT") {
		queryParams.where['$and'] = [
			{'expertId': {$ne: null}},
			{'$expert.contacts.id$': {$eq: userId}}
		]
		queryParams.include.push(prepareInclude(models.expert, 'expert'))
	}
	return queryParams
}

/**
 * read manifestation by user id
 * @param query {json} - attributes: offset, sortable, search, limit
 * @param userId {number} - id of user
 * @param role {json} - role data
 * @returns {Promise}
 */
module.exports.readMany = function(query, userId, role) {
	var queryParams = prepare(role, userId);
	if (query['offset'])
		queryParams.offset = parseInt(query['offset']);
	var sortable = ['type', 'eventName', 'BNS'];
	if (sortable.indexOf(query['sort']) > -1)
		queryParams.order = [
			[
				query['sort'], query['order'] === 'DESC'
					? 'DESC'
					: 'ASC'
			]
		];
	if (query['search']) {
		queryParams.where['eventName'] = {
			$like: '%' + query['search'] + '%'
		};
	}
	if (query['type'] == TYPE_PROJECT || query['type'] == TYPE_CONTRACT)
		queryParams.where['sortPDF'] = {
		$eq: query['type']
		}
	if (query['status']) {
		var status = 0
		if (query['status'] == ONGOING || query['status'] == CANCELED || query['status'] == TRANSFORMED)
		status = query['status']
		if (status) {
		queryParams.where['status'] = { $eq: status }
		}
	}
	if (query['year']) {
		let dateFilter = sequelize.where(sequelize.fn('YEAR', sequelize.col('eventDate')), query['year'])
		if (queryParams.where['$and'])
		queryParams.where['$and'].push(dateFilter)
		else
		queryParams.where['$and'] = [dateFilter]
	}
	if (query['limit'])
		queryParams.limit = parseInt(query['limit']);
	queryParams.include.push({
		model: models.user,
		as: 'accountManager'
	})
    queryParams.include.push({
		association: 'mainInsurer',
		attributes: ["id"],
		include: {
			model: models.version,
			where: { isActive: true}
		}
    })
	return models.manifestation.findAll(queryParams);

};

/**
 * get count of manifestation
 * @param query {json} - attribute: showDisabled
 * @returns {Promise}
 */
module.exports.count = function(query) {
	var queryParams = {};
	if (query['showDisabled'])
		queryParams.paranoid = false;
	return models.manifestation.count(queryParams);
};

/**
 * Get Manifestation by Id
 * @param id {string} - id of the manifestation
 * @returns {Promise.<Array.<Manifestation>>}
 */
module.exports.readById = function(id) {
	return models.manifestation.findAll({
		where: {
			id: id
		},
		include: [
			{
				model: models.manifestation_dates,
				as: 'manifestationDates'
			}, {
				model: models.manifestation_person,
				as: 'personUnavailabilityList'
			}, {
				model: models.manifestation_technician,
				as: 'technicianUnavailabilityList'
			}, {
				model: models.co_insurers,
				as: 'coInsurerList'
			}
		]
	}).then(function(businessProviderDaoList) {
		if (businessProviderDaoList && businessProviderDaoList.length === 1) {
			return businessProviderDaoList[0];
		} else {
			throw {
				status: 404,
				err: 'Manifestation not found ( id : ' + id + ')'
			};
		}
	}).catch(function(err) {
		throw {status: 500, err: err, id: id};
	})

};

/**
 * save manifestation dates
 * @param id {number} - id of manifestation
 * @param data {array} - array of manifestation dates
 * @returns {*}
 */
const storeManifestationDates = (id, data) => {
	if (id && data["length"] && data.length > 0) {
		data.forEach((e, i) => {
			data[i].manifestationId = id;
			data[i].date = e.date.value;
			data[i].amount = +data[i].amount;
			// if (data[i].id) {
			//   delete data[i].createdAt;
			//   delete data[i].updatedAt;
			// }
		});
		models.manifestation_dates.bulkCreate(data, {
			fields: [
				'id',
				'manifestationId',
				'date',
				'country',
				'zipCode',
				'place',
				'scene',
				'damageBasis',
				'amount',
			],
			updateOnDuplicate: [
				'date',
				'country',
				'zipCode',
				'place',
				'scene',
				'damageBasis',
				'amount',
			]
		})
		.then( () => {})
		.catch(err => {
			throw {
				status: 500,
				err: err
			};
		})
	}
}

/**
 * Create manifestation
 * @param manifestation {object} - object of manifestation
 * @returns {*}
 */
module.exports.create = function(manifestation) {
	return models.manifestation.create({
		eventName: manifestation.eventName || null,
		totalDeclaredBudget: manifestation.totalDeclaredBudget || null,
		type: manifestation.type || null,
		accountManagerId: manifestation.accountManagerId || null,
		policyLanguage: manifestation.policyLanguage || null,
		currency: manifestation.currency || null,
		isBusinessProvider: manifestation.isBusinessProvider || null,
		businessProviderId: manifestation.businessProviderId || null,
		businessProviderFeesRate: manifestation.businessProviderFeesRate || null,
		hasAdditionalInsuredParty: manifestation.hasAdditionalInsuredParty || null,
		additionalInsuredPartyName: manifestation.additionalInsuredPartyName || null,
		specialConditions: manifestation.specialConditions || null,
		endOfGuaranteeRule: manifestation.endOfGuaranteeRule || null,
		endOfGuaranteeRuleOnePercentDurationOfTheShow: manifestation.endOfGuaranteeRuleOnePercentDurationOfTheShow || null,
		endOfGuaranteeRuleOneHasPlayed: manifestation.endOfGuaranteeRuleOneHasPlayed || null,
		endOfGuaranteeRuleOneHasOption: manifestation.endOfGuaranteeRuleOneHasOption || null,
		endOfGuaranteeRuleTwoActNumberIsOver: manifestation.endOfGuaranteeRuleTwoActNumberIsOver || null,
		endOfGuaranteeRuleThreeTimeIsOver: manifestation.endOfGuaranteeRuleThreeTimeIsOver || null,
		endOfGuaranteeRuleFourCondition: manifestation.endOfGuaranteeRuleFourCondition || null,
		isCoInsurance: manifestation.isCoInsurance || null,
		mainInsurerId: manifestation.mainInsurerId || null,
		rateInsurer: manifestation.rateInsurer || null,
		coInsurerId: manifestation.coInsurerId || null,
		hasExpert: manifestation.hasExpert || null,
		expertId: manifestation.expertId || null,
		averageClause: manifestation.averageClause || null,
		averagePremiumClause: manifestation.averagePremiumClause || null,
		contractualIndemnityLimit: manifestation.contractualIndemnityLimit || null,
		contractualIndemnityLimitRate: manifestation.contractualIndemnityLimitRate || null,
		averageFinancialCommitment: manifestation.averageFinancialCommitment || null,
		subscriptionDeadLine: manifestation.subscriptionDeadLine || new Date(),
		hasUnavailabiliytExtension: manifestation.hasUnavailabiliytExtension || null,
		inceptionDate: manifestation.inceptionDate || new Date(),
		eventCategory: manifestation.eventCategory || null,
		eventStartDate: manifestation.eventStartDate || new Date(),
		eventEndDate: manifestation.eventEndDate || null,
		artist: manifestation.artist || null,
		dateType: manifestation.dateType || null,
		numberOfInsuredDates: manifestation.numberOfInsuredDates || null,
		numberPersonInsured: manifestation.numberPersonInsured || null,
		numberTechnicianInsured: manifestation.numberTechnicianInsured || null,
		isTechnicianInsuredNamed: manifestation.isTechnicianInsuredNamed || null,
		totalBudget: manifestation.totalBudget || null,
		unavailabilityRate: manifestation.unavailabilityRate || null,
		hasSpecialDispositionExtension: manifestation.hasSpecialDispositionExtension || null,
		specialDisposition: manifestation.specialDisposition || null,
		isFeeDeducted: manifestation.isFeeDeducted || null,
		artisteFee: manifestation.artisteFee || null,
		insuredBudget: manifestation.insuredBudget || null,
		hasBadWeatherExtension: manifestation.hasBadWeatherExtension || null,
		badWeatherRate: manifestation.badWeatherRate || null,
		percentBudgetToBadWeather: manifestation.percentBudgetToBadWeather || null,
		weatherBudget: manifestation.weatherBudget || null,
		hasCrowShortageExtension: manifestation.hasCrowShortageExtension || null,
		franchise: manifestation.franchise || null,
		hasBombingExtension: manifestation.hasBombingExtension || null,
		bombingRate: manifestation.bombingRate || null,
		bombingNumberOfDay: manifestation.bombingNumberOfDay || null,
		bombingNumberOfKilometre: manifestation.bombingNumberOfKilometre || null,
		bombingHasOptionThreat: manifestation.bombingHasOptionThreat || null,
		bombingHasOptionRecommendation: manifestation.bombingHasOptionRecommendation || null,
		hasMoralReasonExtension: manifestation.hasMoralReasonExtension || null,
		hasExpertFeeExtension: manifestation.hasExpertFeeExtension || null,
		expertWarrantyAmount: manifestation.expertWarrantyAmount || null,
		expertFranchise: manifestation.expertFranchise || null,
		BNSType: manifestation.BNSType || null,
		applicableFranchise: manifestation.applicableFranchise || null,
		comprehensiveInsuranceRate: manifestation.comprehensiveInsuranceRate || null,
		BNS: manifestation.BNS || null,
		franchiseAmount: manifestation.franchiseAmount || null,
		exposedBudgetUnavailability: manifestation.exposedBudgetUnavailability || null,
		exposedBudgetBadWeather: manifestation.exposedBudgetBadWeather || null,
		exposedBudgetBombing: manifestation.exposedBudgetBombing || null,
		excludingTaxRateUnavailability: manifestation.excludingTaxRateUnavailability || null,
		excludingTaxRateBadWeather: manifestation.excludingTaxRateBadWeather || null,
		excludingTaxRateBombing: manifestation.excludingTaxRateBombing || null,
		ovationCommission: manifestation.ovationCommission || null,
		ovationBSNCommission: manifestation.ovationBSNCommission || null,
		taxRate: manifestation.taxRate || null,
		category: manifestation.category || null,
		customCategory: manifestation.customCategory || null,
		eventDate: manifestation.eventDate || null,
		eventEndDate: manifestation.eventEndDate || null,
		artist: manifestation.artist || null,
		dateType: manifestation.dateType || null,
		numberInsured: manifestation.numberInsured || null,
		budget: manifestation.budget || null,
		isPersonUnavailability: manifestation.isPersonUnavailability || null,
		isTechnicianIndispensable: manifestation.isTechnicianIndispensable || null,
		clientId: manifestation.clientId || null,
		status: manifestation.status || null,
		createdAt: new Date(),
		updatedAt: new Date(),
		projectNumber: manifestation.projectNumber,
		contractNumber: manifestation.contractNumber

	}).then(manifestationDao => {
		const id = manifestationDao.get('id');
		try {
			const data = manifestation.manifestationDates
			? JSON.parse(manifestation.manifestationDates)
			: [];
			storeManifestationDates(id, data);
		} catch (e) {
			throw {
				status: 500,
				err: 'Failed parsing manifestation dates JSON: ' + manifestation.manifestationDates
			};
		}

		ManifestationsPeopleService.updateManifestationPeople(manifestationDao, manifestation.personUnavailabilityList)
		.then(
			(manifestationPersonDaoList) => {
				return manifestationDao;
			}
		)
		.catch(err => {
			throw {
				status: 500,
				err: err
			};
		})
		ManifestationsTechnicianService.updateManifestationTechnician(manifestationDao, manifestation.technicianUnavailabilityList)
		.then(
			(manifestationPersonDaoList) => {
				return manifestationDao;
			}
		)
		.catch(err => {
			throw {
				status: 500,
				err: err
			};
		})
		ManifestationsCoInsurerService.updateManifestationCoInsurer(manifestationDao, manifestation.coInsurerList)
		.then(
			(manifestationPersonDaoList) => {
				return manifestationDao;
			}
		)
		.catch(err => {
			throw {
				status: 500,
				err: err
			};
		})
	}).catch(err => {
		throw {status: 500, err: err};
	});
};

/**
 * Edit Manifestation
 * @param id {number} - id of the manifestation
 * @param manifestation {json} - data of the manifestation
 * @returns {*}
 */
module.exports.editById = function(id, manifestation) {
	return this.readById(id).then(function(manifestationDao) {
		if (manifestationDao) {
			hydrate(manifestationDao, manifestation);
			return manifestationDao.save().then((manifestationDao) => {
				return Promise.all([
					ManifestationsPeopleService.updateManifestationPeople(manifestationDao, manifestation.personUnavailabilityList),
					ManifestationsTechnicianService.updateManifestationTechnician(manifestationDao, manifestation.technicianUnavailabilityList),
					ManifestationsCoInsurerService.updateManifestationCoInsurer(manifestationDao, manifestation.coInsurerList),
					new Promise(function(resolve, reject) {
						const data = manifestation.manifestationDates
							? JSON.parse(manifestation.manifestationDates)
							: [];
						storeManifestationDates(id, data);
						resolve()
					})
				]).then((data) => {
					return manifestationDao;
				})
			})
		}
	}).catch(function(err) {
		throw {status: 500, err: err};
	});
};

/**
 * Delete manifestation
 * @param id {number} - id of the manifestation
 * @returns {*}
 */
module.exports.deleteById = function(id) {
	// TODO: delete connected rows in manifestations_dates
	return models.manifestation.destroy({
		where: {
			id: id
		}
	})
	.then( () => {
		models.manifestation_dates.destroy({
				where: {
						manifestationId: id
				}
		});
	});
};

/**
 * update manifestation
 * @param id {number} - id of manifestation to be updated
 * @param data {object} - manifestation object
 * @returns {Promise}
 */
module.exports.update = function(id, data) {
		return models.manifestation.update(data, {
				where: {
						id: id
				}
		})
};
