/**
 * @module Service Uploaded Resource
 */

var models  = require('./../models');

/**
 * Get data by filename()
 * @param query {json} - attributes: filename etc
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.findByName = (name) => {
	var queryParams = {};
    queryParams.where = {
        filename: name
    };
	return models.uploaded_resource.findOne(queryParams);
};

/**
 * create new 
 * @param file {json} - new version data to insert
 * @returns {Promise}
 */
module.exports.create = function(file) {
	return models.uploaded_resource.create(file);
};
