/**
 * @module Service Version
 */

var models  = require('./../models');

/**
 * Get versions by orderting
 * @param query {json} - attributes: insrance_id, inceptionDate
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.get = (query) => {
	var queryParams = {};
	if (query['id']){
		queryParams.where = {
			id: parseInt(query['id'])
		};
	}
	if (query['insurance_id']){
		queryParams.where = {
			insurance_id: parseInt(query['insurance_id'])
		};
	}
	if (query['inceptionDate']){
		queryParams.where = {
			insurance_id: parseInt(query['insurance_id']),
			activation: {
				$lte: query['inceptionDate']
			}
		};
		queryParams.order = [['activation', 'DESC'], ['updatedAt', 'DESC']];
	}
	return models.version.findAll(queryParams).then((versions) => {
        let actives = versions.filter(version => version.isActive);
        let drafts = versions.filter(version => !version.activation);
        let locked = versions.filter(version => !version.isActive && version.activation);
        return actives.concat(...drafts, ...locked)
	});
};

/**
 * create new version
 * @param newVersion {json} - new version data to insert
 * @returns {Promise}
 */
module.exports.create = function(newVersion) {
	return models.version.create(newVersion);
};

/**
 * update version by id
 * @param id {numbner} - version id to be updated
 * @param data {json} - new version data to be updated
 * @returns {Promise}
 */
module.exports.update = function(id, data) {
	return models.version.update(data, {
		where: {
			id: id
		}
	})
};

/**
 * delete version by id
 * @param id {number} - version id to be deleted
 * @returns {Promise}
 */
module.exports.delete = function(id) {
	return models.version.destroy({
		where: {
			id: id
		}
	});
}

module.exports.getActive = function() {
	return models.version.findOne({
		where: {
			isActive: true
		}
	});
}

/**
 * activate the selected version and desactivate the old active
 */
module.exports.activate = function(id) {
    let versionToEdit = null;
	return models.version.findById(id).then((version) => {
		if(!version) throw {error: 'cannot find version'};
		versionToEdit=version;
    }).then(() => this.getActive()).then((oldVersion) => {
		if(oldVersion) {
			oldVersion.isActive = false;
            return oldVersion.save();
        }
	}).then(() => {
        versionToEdit.isActive = true;
        versionToEdit.activation = new Date();
        return versionToEdit.save();
    });
}

/**
 * active versions can only be edited by a user with the correct right
 *
 * @param versionId
 * @param hasEditRight
 * @returns {Promise<boolean>}
 */
module.exports.restrictEditionForActive = function(versionId, hasEditRight) {
    return models.version.findById(versionId).then((version) => {
        if(version.isActive && !hasEditRight) return false;
        return true;
    })

}