/**
 * @module Service Business provider
 */

var models = require('./../models');
var sha1 = require('sha1');
var editContact = require("./UsersService").editInsuranceCompanyContact;
var deleteContact = require("./UsersService").delete;

/**
 * Hydrate function of the Business Provider Model
 * @param data {json} - json of the hydrate business provider
 * @param oldDao
 * @returns {*}
 */
function hydrate(data) {
    let dao = {};

    dao.type = data.type;
    dao.businessName = data.businessName;
    dao.businessProviderLastName = data.businessProviderLastName;
    dao.businessProviderFirstName = data.businessProviderFirstName;
    dao.siret = data.siret;
    dao.status = data.status;
    dao.commissionPercent = data.commissionPercent;
    dao.address = data.address;
    dao.address2 = data.address2;
    dao.zipCode = data.zipCode;
    dao.city = data.city;
    dao.countryId = data.countryId;
    dao.email = data.email;
    dao.phone = data.phone;
    dao.orias = data.orias;
    return dao;
}

/**
 * Check for unicity on the new email if the provider is particular or on new SIRET if he is professional
 * Throw a 403 if fail
 *
 * @param newBusinessProvider
 * @param id
 * @returns {Promise.<boolean>}
 */
function checkForUnicity(newBusinessProvider, id) {

    let where = {};
    if(newBusinessProvider.type === 'professional') {
        where.siret = newBusinessProvider.siret;
    } else if(newBusinessProvider.type === 'particular') {
        where.email = newBusinessProvider.email;
    } else {
        throw 'Incorrect provider type: '+newBusinessProvider.type;
    }

    return models.business_provider.findOne({
        where:where,
        paranoid: false,
        include: []
    }).then((newBusinessProviderDao) => {
        if(!newBusinessProviderDao || (id && newBusinessProviderDao.id == id)) {
            return true
        }
        let errorField;
        if(newBusinessProvider.type === 'professional') {
            errorField = 'siret'
        } else if(newBusinessProvider.type === 'particular') {
            errorField = 'email'
        }
        throw {
            status: 403,
            err: {
                msg: 'duplicate entry',
                errorField: errorField
            },
        };
    })
}

function saveContacts(provider, contacts) {
    if (!contacts || !contacts.length) {
        return provider;
    }
    var promises = [];
    contacts.forEach(function (_contact) {
        if(_contact.isDeleted) {
            if(_contact.id){
                promises.push(deleteContact(_contact.id))
            }
        } else {
            promises.push(editContact(_contact).then(userDao => provider.addContact(userDao)))
        }
    });
    return Promise.all(promises).then(function () {
        return provider
    }, function (err) {
        console.error(err);
    })
}

/**
 * Create or update an business provider
 * @params id {number} - id of the Business Provider to edit
 * @params businessProvider {json} - json of the updated Business Provider
 * @returns {Promise}
 */
module.exports.edit = function (id, businessProvider) {
    let p = checkForUnicity(businessProvider, id);
    if (id) {
        p = p.then((success) => {
            return models.business_provider.update(hydrate(businessProvider), {where: {id: id}}).then(() => {
                return models.business_provider.findById(id)
            });
        });
    } else {
        p = p.then((success) => models.business_provider.create(hydrate(businessProvider, true)));
    }
    return p.then(newProvider => saveContacts(newProvider, businessProvider.contacts));
};

/**
 * @desc get list of business provider
 * @param query {json} - attribut : offset, limit, sortable ( deletedAt businessName address zipCode city ) search and id params
 * @returns {Promise.<Array.<businessProviderDao>>}
 */
module.exports.readAll = function (query) {
	var queryParams = {};
	if (query['offset'])
		queryParams.offset = parseInt(query['offset']);
	if (query['limit'])
		queryParams.limit = parseInt(query['limit']);
	var sortable = ['status', 'businessName', 'businessProviderLastName', 'email', 'phone', 'commissionPercent'];
	if (sortable.indexOf(query['sort']) > -1)
		queryParams.order = [[query['sort'], query['order'] === 'DESC' ? 'DESC' : 'ASC']];
	if (query['search']){
        let likeFilter = {
            $like: '%' + query['search'] + '%'
        };
        queryParams.where = {
        	$or: [
        		{
                    'businessName': likeFilter
                }, {
                    'businessProviderLastName': likeFilter
                }, {
                    'email': likeFilter
                }, {
                    'phone': likeFilter
                }, {
                    'commissionPercent': likeFilter
                },
			]
		}
	} else if (query['id'])
		queryParams.where = {
			id: {
				$eq: query['id']
			}
		}
	if (query['showDisabled'] == 'true')
		queryParams.paranoid = false;
	return models.business_provider.findAll(queryParams);
};

/**
 * Count function of the business provider
 * @param query {json} - select query
 * @returns {Promise<Integer>}
 */
module.exports.count = function(query) {
	var queryParams = {};
	if (query['showDisabled'])
		queryParams.paranoid = false;
	return models.business_provider.count(queryParams);
};

/**
 * Get Business Provider by Id
 * @param id {number} - id of the business provider
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.readById = function (id) {
	return models.business_provider.findOne({
		where: {
			id: id
		},
		paranoid: false,
        include: ['contacts']
	})
	.then(function (businessProviderDao) {
		if(!businessProviderDao)
            throw {
                status: 404,
                err: 'User not found ( id : ' + id + ')'
            };
		return businessProviderDao;
	})
	.catch(function (err) {
		throw {
			status: 500,
			err: err
		};
	})
};

/**
 * Delete BusinessProvider by id
 * @param id {number} id of business provider to be deleted
 * @returns {Promise}
 */
module.exports.deleteById = function (id) {
    return models.business_provider.destroy({
        where: {
            id: id
        }
    });
};

/**
 * Reactivate a Business Provider by its id
 * @param id {number} id of Business Provider to edit
 * @returns {Promise}
 */
module.exports.reactivateById = function (id) {
    return this.readById(id, false).then(
        function (businessProviderDao) {
            return businessProviderDao.restore();
        }
    )
        .catch(function (err) {
            throw {
                status: 500,
                err: err
            };
        });
};