/**
 * @module Service Paragraph
 */

var models  = require('./../models');

/**
 * Get paragraphs 
 * @param query {json} - attributes: order, version_id, paragraph_id
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.get = (query) => {
	var queryParams = {};
	queryParams.order = [['level', 'ASC'], ['subLevel', 'ASC']];
	if (query['version_id']){
		queryParams.where = {
			version_id: parseInt(query['version_id'])
		};
	}
	if (query['paragraph_id']){
		queryParams.where = {
			id: parseInt(query['paragraph_id'])
		};
	}
	return models.paragraph.findAll(queryParams);
};

/**
 * create paragraph
 * @param newParagraph {json} - json data of new paragraph
 * @returns {Promise}
 */
module.exports.create = function(newParagraph) {
	return models.paragraph.create(newParagraph);
};

/**
 * update paragraph by id
 * @param id {number} - id of paragraph to be updated
 * @param data {json} - json data of updated paragraph
 * @returns {Promise}
 */
module.exports.update = function(id, data) {
	return models.paragraph.update(data, {
		where: {
			id: id
		}
	})
};

/**
 * delete paragraph by id
 * @param id {number} - id of paragraph to be deleted
 * @returns {Promise}
 */
module.exports.delete = function(id) {
	return models.paragraph.destroy({
		where: {
			id: id
		}
	});
}

/**
 * copy project to contract(clear contract before copy)
 * @param versionId {number} - id of version to be deleted
 * @returns {Promise}
 */
module.exports.copyProject = function(versionId) {
	const query={
		where:{
			version_id:versionId,
			group: 0
		}
	};
	return models.paragraph.destroy({
		where: {
			version_id: versionId,
			group: 1
		}
	}).then(()=>{
		return models.paragraph.findAll(query).then((resp)=>{
			var promises = [];
			resp.map((paragraphDao, index)=>{
				promises.push(
					models.paragraph.create({
						version_id: versionId,
						group: 1,
						level: paragraphDao.level,
						subLevel: paragraphDao.subLevel,
						title: paragraphDao.title,
						condition: paragraphDao.condition,
						editor: paragraphDao.editor
					})
				);
			});
			return Promise.all(promises);
		});
	})
}

module.exports.copyFrToEn = function(versionId, group) {
    const query={
        where:{
            version_id:versionId,
			lang: 'fr',
            group: group
        }
    };
    return models.paragraph.destroy({
        where: {
            version_id: versionId,
            lang: 'en',
        }
    }).then(() => {
    	return models.paragraph.findAll(query).then((resp)=>{
            var promises = [];
            resp.map((paragraphDao, index)=>{
                promises.push(
                    models.paragraph.create({
                        version_id: versionId,
                        group: group,
                        level: paragraphDao.level,
                        subLevel: paragraphDao.subLevel,
                        title: paragraphDao.title,
                        condition: paragraphDao.condition,
                        editor: paragraphDao.editor,
                        lang: 'en'
                    })
                );
            });
            return Promise.all(promises);
        });
    });
}

module.exports.checkForParagraphs = function(versionId, lang) {
	let queryParams = {
		where: {
			version_id: parseInt(versionId),
			lang: lang,
		}
	}
	return models.paragraph.findAll(queryParams).then((list) => list.length > 0);
}


