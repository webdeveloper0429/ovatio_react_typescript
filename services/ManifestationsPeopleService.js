/**
 * @module Service Manifestation People
 */

var models = require('./../models');

/**
 * update manifestation people
 * @param manifestationDao {json} - manifestation dao
 * @param personUnavailabilityList {json} - list of manifestation people to be updated.
 * @returns {*}
 */
module.exports.updateManifestationPeople = function(manifestationDao, personUnavailabilityList) {
	try {
		return models.manifestation_person.findAll({
			where: {
				manifestationId: manifestationDao.id
			},
			include: []
		}).then(function(manifestationPersonDaoList) {
			var promises = [];
			if (personUnavailabilityList && personUnavailabilityList.length > 0) {
			personUnavailabilityList.forEach(function(people) {
				if (!people.id) {
					promises.push(models.manifestation_person.create({
						band: people.band || null,
						manifestationId: manifestationDao.id || null,
						function: people.function || null,
						replaceable: people.replaceable || null,
						age: people.age || null,
                        overAgeLimit: people.overAgeLimit || false,
						medicalExamination: people.medicalExamination || null,
						insuredPeople: people.insuredPeople || null,
						createdAt: new Date(),
						updatedAt: new Date()
					}))
				}
			});
			}
			if (manifestationPersonDaoList && manifestationPersonDaoList.length > 0) {
			manifestationPersonDaoList.forEach(function(manifestationPersonDao) {
				var isInList = false;
				if (personUnavailabilityList && personUnavailabilityList.length > 0) {
				personUnavailabilityList.forEach(function(people) {
					if (people.id && manifestationPersonDao.id == people.id) {
						isInList = true;
						manifestationPersonDao.band = people.band || null;
						manifestationPersonDao.function = people.function || null;
						manifestationPersonDao.replaceable = people.replaceable || null;
						manifestationPersonDao.age = people.age || null;
                        manifestationPersonDao.overAgeLimit = people.overAgeLimit || false;
						manifestationPersonDao.medicalExamination = people.medicalExamination || null;
						manifestationPersonDao.insuredPeople = people.insuredPeople || null;
						manifestationPersonDao.updatedAt = new Date();
						promises.push(manifestationPersonDao.save())
					}
				});
				}
				if (!isInList) {
					promises.push(models.manifestation_person.destroy({
						where: {
							id: manifestationPersonDao.id
						}
					}));
				}
			});
			}
			return Promise.all(promises).then(function(data) {
				return data;
			}, function(err) {
				console.error(e);
			})
		})
	} catch (e) {
		console.error(e);
	}
};
