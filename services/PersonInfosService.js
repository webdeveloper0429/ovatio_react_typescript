/**
 * @module Service Person Info
 */
var models  = require('./../models');

/**
 * Get person
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.getAll = function () {
	return models.person_info.findAll({});
};

/**
 * Create person Info
 * @param person_info {json} - person_info json data
 * @returns {Promise}
 */
module.exports.create = function (person_info) {
	return models.person_info.create({
		email: person_info.email,
		firstName: person_info.firstName,
		lastName: person_info.lastName,
		createdAt:new Date(),
		updatedAt:new Date()
	})
};

/**
 * findOrCreate Person Info
 * @param person_info {json} - person_info json data
 * @returns {Promise}
 */
module.exports.findOrCreate = function (person_info) {
	return models.person_info.findOrCreate(
		{where: {id: person_info.id},
			defaults: {
				email: person_info.email,
				firstName: person_info.firstName,
				lastName: person_info.lastName,
				createdAt:new Date(),
				updatedAt:new Date()
		}})
};
