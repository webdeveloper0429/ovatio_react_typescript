/**
 * @module Service Manifestation Dates
 */
var models = require('./../models');
var sha1 = require('sha1');

/**
 * delete manifestation dates by id
 * @param id {number} - id of manifestation dates to be deleted
 * @returns {Promise}
 */
module.exports.deleteById = function (id) {
	return models.manifestation_dates.destroy({
		where: {
			id: id
		}
	});
};

/**
 * delete manifestation dates by manifestation id
 * @param manifestationId {number} - id of manifestation
 * @returns {promise}
 */
module.exports.deleteByManifestationId = function (manifestationId) {
	return models.manifestation_dates.destroy({
		where: {
			manifestationId: manifestationId
		}
	});
};
