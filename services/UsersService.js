/**
 *@module Service User
 */

var path = require('path');
var env = process.env.NODE_ENV || 'development';
var config = require(path.dirname(__dirname) + '/config/config.json')[env];
const consts = require(path.dirname(__dirname) + '/consts.js');
var models = require('./../models');
var sha1 = require('sha1');
var pwGenerator = require('generate-password');
var mail = require('../modules/mail');


/**
 * Get all user
 * @param query {json} - attribut : offset, limit, sortable, search and id
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.getAll = function (query) {
    var queryParams = {};
    if (query['offset'])
        queryParams.offset = parseInt(query['offset']);
    if (query['limit'])
        queryParams.limit = parseInt(query['limit']);
    var sortable = ['deletedAt', 'businessName', 'address', 'zipCode', 'city'];
    if (sortable.indexOf(query['sort']) > -1)
		queryParams.order = [[query['sort'], query['order'] === 'DESC' ? 'DESC' : 'ASC']];
	queryParams.include = [
		{
			model: models.role,
			as: 'role',
			// include: [
			// 	{model: models.right, as: 'rightList'}
			// ]
		}
	]
    if (query['search'])
        queryParams.where = {
			"$or": [
				{
					email: {
						$like: '%' + query['search'] + '%'
					}
				},{
					"$role.label$": {
						$like: '%' + query['search'] + '%'
					}
				}
			]
		};
    else if (query['id'])
        queryParams.where = {
            id: {
                $eq: query['id']
            }
        }
    if (query['showDisabled'])
        queryParams.paranoid = false;


    return models.user.findAll(queryParams);
};

/**
 * find account manager user
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.findAccountManager = function () {
	var queryParams = {
		include: [
			{
				model: models.role,
				as: 'role',
				where : {
					'label' : "ACCOUNT_MANAGER"
				}
			}
		],

	};
	return models.user.findAll(queryParams);
};

/**
 * hydrate user object
 * @param dao {json} - user dao
 * @param data {json} - user data
 * @returns {json}
 */
function hydrate(dao, data) {
	"use strict";
	if (dao && data) {
		dao.genre = data.genre;
		dao.roleId = data.roleId;
		dao.lastName = data.lastName;
		dao.firstName = data.firstName;
		dao.address = data.address;
		dao.address2 = data.address2;
		dao.zipCode = data.zipCode;
		dao.city = data.city;
		dao.country = data.country;
		dao.email = data.email;
		dao.password = dao.password ? dao.password : sha1('apideo');
		dao.phone = data.phone;
		dao.mobilePhone = data.mobilePhone;
		dao.isGroupOvatio = data.isGroupOvatio;
		dao.updatedAt = new Date();
		dao.isActive = data.isActive;
		return dao;
	} else {

	}
}

/**
 * Create User
 * @param user {json} - new user data
 * @param personInfoDao {object} - person info dao object
 * @param roleDao {object} - role dao object
 * @returns {*}
 */
module.exports.create = function (user, personInfoDao, roleDao) {
	return models.user.create({
		genre: user.genre,
		roleId: user.roleId,
		lastName: user.lastName,
		firstName: user.firstName,
		address: user.address,
		address2: user.address2,
		zipCode: user.zipCode,
		city: user.city,
		email: user.email,
		phone: user.phone,
		mobilePhone: user.mobilePhone,
		isGroupOvatio: user.isGroupOvatio,
		password: user.password ? sha1(user.password) : null,
		createdAt: new Date(),
		updatedAt: new Date(),
		isActive: user.isActive,
	}).then(
		function (user) {
			if (personInfoDao) {
				user.setPersonInfo(personInfoDao);
			}
			if (roleDao) {
				user.setRole(roleDao);
			}
			return user;
	})
};

/**
 * Count function of the user
 * @param query {json} - select query
 * @returns {Promise<Integer>}
 */
module.exports.count = function (query) {
	var queryParams = {};
	return models.user.count(queryParams);
};

/**
 * Create user link to insurrance company
 * @param user {json} - user data
 * @param roleDao {json} - role dao
 * @returns {*}
 */
module.exports.editInsuranceCompanyContact = function (user, roleDao) {
	function hydrate(user, mode) {
		let u = {};
		u.genre = user.genre;
		u.lastName = user.lastName;
		u.firstName = user.firstName;
		u.address = user.address;
		u.zipCode = user.zipCode;
		u.city = user.city;
		u.email = user.email;
		u.phone = user.phone;
		u.mobilePhone = user.mobilePhone;
		u.isGroupOvatio = false;
		u.createdAt = u ? u.createdAt : new Date();
		u.updatedAt =  new Date();
		return u;
	}
	var p;
	if(user.id){
		p = models.user.update(hydrate(user, 'edit'), {where: {id: user.id}}).then(function () {
			return models.user.findById(user.id)
		})
	} else {
		p = models.user.create(hydrate(user, 'create'));
	}
	return p.then(function (userDao) {
		if (roleDao) {
			userDao.setRole(roleDao);
		}
		return userDao;
	});
};

/**
 * Get user by Id
 * @param id {number} - user id to get
 * @returns {json} - user data
 */
module.exports.readById = function (id) {
	return models.user.findAll({
		where: {
			id: id
		},
		include: [
			{
				model: models.role,
				as: 'role',
				include: [
					{model: models.right, as: 'rightList'}
				]
			}
		]
	})
		.then(function (userDao) {
			if (userDao && userDao.length === 1) {
				return userDao[0];
			} else {
				throw {
					status: 404,
					err: 'User not found ( id : ' + id + ')'
				};
			}
		})
		.catch(function (err) {
			throw {
				status: 500,
				err: err
			};
		})
};


/**
 * Edit User by id
 * @param id {number} - id of user to edit
 * @param data {json} - new user data 
 * @returns {json} - afected user data
 */
module.exports.editById = function (id, data) {
	var userJustActivated = false;
	var password = '';
	return this.readById(id).then(
		function (userDao) {
			if (userDao) {
				if (!userDao.isActive && !!data.isActive) {
					userJustActivated = true;
					password = pwGenerator.generate({
						length: consts.generated_password_length,
						numbers: true
					});
					userDao.password = sha1(password);
				}
				hydrate(userDao, data);
				return userDao.save()
					.then((updatedUserDao) => {
						sendNewPasswordEmail(updatedUserDao, userJustActivated, password)
						return updatedUserDao
					});
			}
		}
	)
		.catch(function (err) {
			throw {
				status: 500,
				err: err
			};
		});
};

/**
 * password reset function
 * @param updatedUserDao {json} - user data
 * @param userJustActivated {boolean} - boolean value of activated or not
 * @param password {string} - new password
 * @returns {*}
 */
function sendNewPasswordEmail(updatedUserDao, userJustActivated, password) {
	if (userJustActivated) {
		mail.sendEmail({
			subject: 'Votre compte ovatio a été activé',
			text: `Bonjour ${updatedUserDao.firstName},\n
				Votre compte Ovatio a été activé.\n
				Votre mot de passe est: ${password}\n
				\n
				Cordialement, l'équipe Ovatio\n
			`,
			template: 'activateUser',
			to: updatedUserDao.email,
			locals: {
				title: 'Votre compte ovatio a été activé',
				subtitle: `Bonjour ${updatedUserDao.firstName},<br>
					Votre compte Ovatio a été activé.<br>
					Votre mot de passe est: ${password}<br>
					<br>
					Cordialement, l'équipe Ovatio<br>
				`,
				baseUrl: config.baseUrl,
				linktext: 'Connexion'
			}
		});
	}
}

/**
 * delete user by id
 * @param id {number} - id of user to delete
 * @returns {Promise}
 */
module.exports.delete = function (id) {
	return models.user.destroy({
		where: {
			id: id
		}
	});
}

module.exports.getByEmail = function(email) {
    return models.user.findOne({ where: {
        email: {
            $eq: email
        }
    }})
}

module.exports.editPasswordById = function(userId, password) {
    return models.user.findById(userId).then((user) => {
        user.password = sha1(password);
        return user.save();
    })
}
