/**
 * @module Service Manifestation CoInsurer
 */

var models = require('./../models');

/**
 * update manifestation co-insurer
 * @param manifestationDao {json} - manifestation dao
 * @param coInsurerList {json} - list of manifestation co-insurer to be updated.
 * @returns {*}
 */
module.exports.updateManifestationCoInsurer = function(manifestationDao, coInsurerList) {
	try {
		return models.co_insurers.findAll({
			where: {
				manifestationId: manifestationDao.id
			},
			include: []
		}).then(function(coInsurerDaoList) {
			var promises = [];
			if (coInsurerList && coInsurerList.length > 0) {
				coInsurerList.forEach(function(coInsurer) {
					if (!coInsurer.id) {
						promises.push(models.co_insurers.create({
							manifestationId: manifestationDao.id || null,
							companyId: coInsurer.companyId || null,
							rate: coInsurer.rate || null,
							createdAt: new Date(),
							updatedAt: new Date()
						}))
					}
				});
			}
			if (coInsurerDaoList && coInsurerDaoList.length > 0) {
				coInsurerDaoList.forEach(function(coInsurerDao) {
					var isInList = false;
					if (coInsurerList && coInsurerList.length > 0) {
					coInsurerList.forEach(function(coInsurer) {
						if (coInsurer.id && coInsurerDao.id == coInsurer.id) {
							isInList = true;
							coInsurerDao.companyId = coInsurer.companyId || null;
							coInsurerDao.rate = coInsurer.rate || null;
							coInsurerDao.updatedAt = new Date();
							promises.push(coInsurerDao.save())
						}
					});
					}
					if (!isInList) {
						promises.push(models.co_insurers.destroy({
							where: {
								id: coInsurerDao.id
							}
						}));
					}
				});
			}
			return Promise.all(promises).then(function(data) {
				return data;
			}, function(err) {
				console.error(e);
			})
		})
	} catch (e) {
		console.error(e);
	}
};
