/**
 * @module Service Expert
 */
var models = require('./../models');
var editInsuranceCompanyContact = require("./UsersService").editInsuranceCompanyContact;
var deleteInsuranceCompanyContact = require("./UsersService").delete;

/**
 * Count function of the business provider
 * @param expert {json} - data of new expert
 * @param roleDao {json} - role dao
 * @returns {*}
 */
module.exports.create = (expert, roleDao) => {
	return models.expert.create(expert)
		.then((expertDao) => updateContacts(expertDao, expert, roleDao))
}

/**
 * update expert by id
 * @param id {numbner} - expert id to be updated
 * @param expert {json} - new expert data to be updated
 * @param roleDao {json} - role dao
 * @returns {*}
 */
module.exports.edit = (id, expert, roleDao) => {
	return models.expert.update(expert, {
		where: {
			id: id
		}
	})
		.then(() => models.expert.findById(id))
		.then((expertDao) => updateContacts(expertDao, expert, roleDao))
}

/**
 * update contacts 
 * @param expertDao {json} - expert dao
 * @param expert {json} - expert data
 * @param roleDao {json} - role dao
 * @returns {*}
 */
function updateContacts(expertDao, expert, roleDao) {
	if (expert.contacts && expert.contacts.length > 0) {
		var promises = [];
		expert.contacts.forEach(function (_contact) {
			if (_contact.isDeleted) {
				if (_contact.id) {
					promises.push(
						deleteInsuranceCompanyContact(_contact.id)
					)
				}
			} else {
				promises.push(editInsuranceCompanyContact(_contact, roleDao).then(
					function (userDao) {
						return expertDao.addContact(userDao);
					}
				))
			}

		});
		return Promise.all(promises).then(function () {
			return expertDao
		}, function (err) {
			console.error(err);
		})
	} else {
		return expertDao
	}
}

/**
 * Get a specific expert by ID
 * @param id - id of the expert
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.getById = function (id) {
	return models.expert.findOne({
		where: {
			id: id
		},
		include: [
			{
				model: models.user,
				as: 'contacts',
				required: false,
				where: {}
			}
		]
	});
};
