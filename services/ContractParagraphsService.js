/**
 * @module Service Contract Paragrpah
 */

var models  = require('./../models');

/**
 * get the contract paragrpahs via manifestation id
 * @param query {json} - id of manifestation ex: {manifestation_id: 3}
 * @returns {Promise.<Array.<Model>>}
 */
module.exports.get = (query) => {
	var queryParams = {};
	queryParams.order = [['level', 'ASC'], ['subLevel', 'ASC']];
	if (query['manifestation_id']){
		queryParams.where = {
			manifestation_id: parseInt(query['manifestation_id'])
		};
	}
	return models.contractParagraph.findAll(queryParams);
};

/**
 * create new contract paragraph 
 * @param newContractParagraph {json} - new contract paragraph data
 * @returns {Promise}
 */
module.exports.create = function(newContractParagraph) {
	return models.contractParagraph.create(newContractParagraph);
};

