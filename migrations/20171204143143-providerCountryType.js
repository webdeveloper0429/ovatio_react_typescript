'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {
        let promises = [
            queryInterface.createTable(
                'countries',
                {
                    id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    label: {
                        type: Sequelize.STRING
                    },
                    createdAt: {
                        type: Sequelize.DATE
                    },
                    updatedAt: {
                        type: Sequelize.DATE
                    }
                },
                {
                    charset: 'utf8'
                }
            ),
            queryInterface.addColumn('insurance_companies', 'countryId', Sequelize.INTEGER).then(() => {
                return queryInterface.removeColumn('insurance_companies', 'country')
            }).then(() => {
                return queryInterface.addColumn('insurance_companies', 'countryAccountingId', Sequelize.INTEGER)
            }).then(() => {
                return queryInterface.removeColumn('insurance_companies', 'countryAccounting')
            }),
            queryInterface.addColumn('business_providers', 'countryId', Sequelize.INTEGER).then(() => {
                return queryInterface.removeColumn('business_providers', 'country')
            }),
        ];

        return Promise.all(promises).then(() => {
            return queryInterface.addConstraint('business_providers', ['countryId'], {
                name: 'countryBusinessProviders',
                type: 'FOREIGN KEY',
                references: {
                    table: 'countries',
                    field: 'id'
                },
                onDelete: 'cascade',
                onUpdate: 'cascade'
             })
        }).then(() => {
            return queryInterface.addConstraint('insurance_companies', ['countryId'], {
                name: 'countryInsuranceCompany',
                type: 'FOREIGN KEY',
                references: {
                    table: 'countries',
                    field: 'id'
                },
                onDelete: 'cascade',
                onUpdate: 'cascade'
            })
        }).then(() => {
            return queryInterface.addConstraint('insurance_companies', ['countryAccountingId'], {
                name: 'countryAccountingInsuranceCompany',
                type: 'FOREIGN KEY',
                references: {
                    table: 'countries',
                    field: 'id'
                },
                onDelete: 'cascade',
                onUpdate: 'cascade'
            })
        })

  },

  down: (queryInterface, Sequelize) => {
  }
};
