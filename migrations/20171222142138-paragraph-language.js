'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('paragraphs', 'lang', {
        type: Sequelize.STRING,
        defaultValue: 'fr'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('paragraphs', 'lang');
  }
};
