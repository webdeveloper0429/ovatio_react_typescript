'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.renameColumn('business_providers', 'companyId', 'siret');

  },

  down: (queryInterface, Sequelize) => {
    queryInterface.renameColumn('business_providers', 'siret', 'companyId');
  }
};
