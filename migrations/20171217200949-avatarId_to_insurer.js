'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('insurance_companies', 'avatarid', Sequelize.STRING)
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn('insurance_companies', 'avatarid')
  }
};
