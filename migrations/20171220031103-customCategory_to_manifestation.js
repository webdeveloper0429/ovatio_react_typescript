'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('manifestations', 'customCategory', Sequelize.STRING)
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn('manifestations', 'customCategory')
  }
};
