'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('manifestation_persons', 'overAgeLimit', Sequelize.BOOLEAN)
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn('manifestation_persons', 'overAgeLimit')
  }
};
