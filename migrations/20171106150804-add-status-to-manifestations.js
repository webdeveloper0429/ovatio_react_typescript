'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('manifestations', 'status', {
      type: Sequelize.INTEGER,
      defaultValue: 1
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('manifestations', 'status')
  }
};
