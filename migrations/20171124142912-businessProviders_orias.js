'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      queryInterface.addColumn('business_providers', 'orias', Sequelize.STRING)
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn('business_providers', 'orias')
  }
};
