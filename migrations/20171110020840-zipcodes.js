'use strict';

module.exports = {
	up: function (queryInterface, Sequelize) {
		return queryInterface.createTable('zipcodes', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			codepays: {
				type: Sequelize.TEXT
			},
			cp: {
				type: Sequelize.TEXT
			},
			ville: {
				type: Sequelize.TEXT
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			}
		});
	},

	down: function (queryInterface, Sequelize) {
	  return queryInterface.dropTable('zipcodes');
	}
};
