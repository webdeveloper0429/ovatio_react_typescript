'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return Promise.all([
          queryInterface.changeColumn('versions', 'activation', Sequelize.DATE, {
              defaultValue: null
          }),
          queryInterface.addColumn('versions', 'isActive', Sequelize.BOOLEAN, {
              defaultValue: false
          })
      ]);
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn('versions', 'isActive');
  }
};
