'use strict';

module.exports = {
	up: function (queryInterface, Sequelize) {
		return queryInterface.createTable('uploaded_resources', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			fieldname: {
				type: Sequelize.STRING,
			},
			originalname: {
				type: Sequelize.STRING
			},
			encoding: {
				type: Sequelize.STRING
			},
			mimetype: {
				type: Sequelize.STRING
			},
			destination: {
				type: Sequelize.STRING
			},
			filename: {
				type: Sequelize.STRING
			},
			path: {
				type: Sequelize.STRING
			},
			size: {
				type: Sequelize.STRING
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			}
		},{
			indexes: [
				{
					unique: true,
					fields: ['filename']
				}
			]
		});
	},

	down: function (queryInterface, Sequelize) {
		return queryInterface.dropTable('uploaded_resources');
	}
};
