'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('manifestations', 'unavailabilityRate', Sequelize.FLOAT),
      queryInterface.changeColumn('manifestations', 'bombingNumberOfDay', Sequelize.FLOAT),
      queryInterface.changeColumn('manifestations', 'bombingNumberOfKilometre', Sequelize.FLOAT)
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('manifestations', 'unavailabilityRate', Sequelize.INTEGER),
      queryInterface.changeColumn('manifestations', 'bombingNumberOfDay', Sequelize.INTEGER),
      queryInterface.changeColumn('manifestations', 'bombingNumberOfKilometre', Sequelize.INTEGER)
    ])
  }
};
