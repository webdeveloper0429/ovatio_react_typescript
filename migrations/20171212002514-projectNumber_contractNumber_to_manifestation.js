'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('manifestations', 'projectNumber', Sequelize.STRING),
      queryInterface.addColumn('manifestations', 'contractNumber', Sequelize.STRING)
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('manifestations', 'projectNumber'),
      queryInterface.removeColumn('manifestations', 'contractNumber')
    ])
  }
};
