'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      queryInterface.addColumn('business_providers', 'deletedAt', Sequelize.DATE)
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn('business_providers', 'deletedAt')

  }
};
