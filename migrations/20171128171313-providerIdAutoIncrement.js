'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.removeConstraint('business_providers', 'businessProviderIsUser').then(() => {
              return queryInterface.removeConstraint('business_providers_users', 'pivotBusinessProviders')
          }
      ).then(() => {
          return queryInterface.changeColumn('business_providers', 'id', {
                  type: Sequelize.INTEGER,
                  autoIncrement: true
              })
          }
      ).then(() => {
          return queryInterface.addConstraint('business_providers_users', ['businessProviderId'], {
                  name: 'pivotBusinessProviders',
                  type: 'FOREIGN KEY',
                  references: { //Required field
                      table: 'business_providers',
                      field: 'id'
                  },
                  onDelete: 'cascade',
                  onUpdate: 'cascade'
              })
          }
      )

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.addConstraint('business_providers', ['id'], {
          name: 'businessProviderIsUser',
          type: 'FOREIGN KEY',
          references: { //Required field
              table: 'users',
              field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
      })
  }
};
