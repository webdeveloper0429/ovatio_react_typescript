'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable(
          'business_providers_users',
          {
              id: {
                  type: Sequelize.INTEGER,
                  primaryKey: true,
                  autoIncrement: true
              },
              businessProviderId: {
                  type: Sequelize.INTEGER,
              },
              userId: {
                  type: Sequelize.INTEGER,
              },
              createdAt: {
                  type: Sequelize.DATE
              },
              updatedAt: {
                  type: Sequelize.DATE
              }
          },
          {
              charset: 'utf8',                    // default: null
          }
      ).then(function () {
          return Promise.all(
              [
                  queryInterface.addConstraint('business_providers_users', ['businessProviderId'], {
                      name: 'pivotBusinessProviders',
                      type: 'FOREIGN KEY',
                      references: { //Required field
                          table: 'business_providers',
                          field: 'id'
                      },
                      onDelete: 'cascade',
                      onUpdate: 'cascade'
                  }),
                  queryInterface.addConstraint('business_providers_users', ['userId'], {
                      name: 'pivotBusinessUsers',
                      type: 'FOREIGN KEY',
                      references: { //Required field
                          table: 'users',
                          field: 'id'
                      },
                      onDelete: 'cascade',
                      onUpdate: 'cascade'
                  })
              ]
          )
      })
  },

  down: (queryInterface, Sequelize) => {
      return Promise.all(
          [
              queryInterface.removeConstraint('business_providers_users', 'pivotBusinessProviders'),
              queryInterface.removeConstraint('business_providers_users', 'pivotUsers'),

          ]
      ).then(
          function () {
              return queryInterface.dropTable('business_providers_users');

          }
      )
  }
};
