'use strict';

module.exports = {
	up: function (queryInterface, Sequelize) {
		return queryInterface.createTable('co_insurers', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			manifestationId: {
				type: Sequelize.INTEGER,
			},
			companyId: {
				type: Sequelize.INTEGER
			},
			rate: {
				type: Sequelize.TEXT
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue : new Date()
			}
		});
	},

	down: function (queryInterface, Sequelize) {
	  return queryInterface.dropTable('co_insurers');
	}
};
