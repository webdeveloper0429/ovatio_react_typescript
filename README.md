# Ovation

```
All development must be done in new branch from develop branch
```

## Installation

### Environment

Node : 6.11.0 ( LTS)

My SQL 5.7


### Npm

```
npm install
```

### Compile

```
webpack
```
watch version 
```
webpack --watch
```


### Config

Copy ./config/_config.json in ./config/config.json

Include your database configuration

To prevent Emails from being sent during the development process, we set *block_email_sending* = true.

If you need to send an email, just set it to false (and make sure that the mailjet credentials are correct).
If it isn't set, it defaults to false.


### Migration

To update right and role
```
npm run update
```

Before use migration, install sequelize cli : 

```
npm install sequelize-cli
or
npm install  -g equelize-cli

```

```
http://docs.sequelizejs.com/manual/tutorial/migrations.html#the-cli
```

To initialise dataBase and migrate it
```
npm run migrate
```

Generate Data
```
npm run seed
```

### Start

```
npm start
```

## Methodology

Each time models is create or modify, create a patch 
```
http://docs.sequelizejs.com/manual/tutorial/migrations.html#the-cli
```

### Documentation
```
"node_modules/.bin/jsdoc" -c doc.conf --verbose
```
### Window

set production mode
```
$env:NODE_ENV="production"
```


### MEP
```
git pull
export NODE_ENV=production
npm install
./node_module/.bin/webpack
./node_module/.bin/sequalize db:migrate
./node_module/.bin/pm2 restart 0
```