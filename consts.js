module.exports = {
    noreply_email_address: 'no-reply@ovatio.eu',
    ovatio_name: 'Ovatio',
    default_mail_subject: 'Aucun sujet',
    generated_password_length: 10,
    TYPE_PROJECT: 0,
    TYPE_CONTRACT: 1,
    ONGOING: 1,
    CANCELED: 2,
    TRANSFORMED: 3,
}
