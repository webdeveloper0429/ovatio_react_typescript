/**
 * @module Controller Business Provider
 */
var BusinessProvidersService = require('./../services/BusinessProvidersService');

/**
 * create business provider
 * @param data {json} - json data of businessProvider
 * @returns {Promise}
 */
module.exports.create = function (data) {
    return BusinessProvidersService.edit(null, data);
};
