/**
 * @module Controller Manifestation
 */
var path = require('path');
var env = process.env.NODE_ENV || 'development';
var config = require(path.dirname(__dirname) + '/config/config.json')[env];
const consts = require(path.dirname(__dirname) + '/consts.js');
var ClientService = require('./../services/ClientService');
var ParaService = require('../services/ParagraphService');
var models = require('./../models');
var mail = require('../modules/mail');
var fs = require('fs')
var ManifestationsService = require('./../services/ManifestationsService');

/**
 * create manifestation
 * @param data {json} - new manifestation data
 * @returns {Promise}
 */
module.exports.create = function (data) {
	return ManifestationsService.create(data);
}

module.exports.duplicate = function (id, dataChanges) {
    return ManifestationsService.readById(id)
        .then(function (data) {
            let obj = Object.assign({}, data.dataValues)
            obj = Object.assign(obj, dataChanges)
            obj.id = null
            return models.manifestation.create(obj)
        })
}

module.exports.clone = function (id, dataChanges) {
    let insertedId = null;
    return ParaService.checkForParagraphs(dataChanges.contractVersionId, dataChanges.lang).then((valid) => {
        if(!valid) throw {code: 1, status: 500, message: 'no pragraphs'};
        return this.duplicate(id, dataChanges)
    }).then(function (data) {
            insertedId = data.dataValues.id
            return ManifestationsService.update(id, { status: consts.TRANSFORMED })
        })
        .then(function () {
            return { insertedId: insertedId }
        })
}

module.exports.sendByMail = function (req, res) {
    var manifestation = {}
    ManifestationsService.readById(req.params.id)
        .then(function (data) {
            manifestation = data.dataValues
            return ClientService.getById(manifestation.clientId)
        })
        .then(function (data) {
            var client
            if (data && data.length > 0)
                client = data[0].dataValues
            return mail.sendEmail({
                subject: 'Votre contrat a été généré',
                text: `Bonjour ${client.clientName}
                \n\nPour faire suite à de votre demande, nous vous prions de trouver ci-joint les conditions d'assurance 
                    que nous sommes en mesure de vous proposer pour votre événement ${manifestation.eventName}.
                \n\nCette proposition a été établie à votre usage exclusif et confidentiel, 
                    compte tenu des éléments fournis par vos soins lors de nos divers échanges 
                    téléphoniques ou courriers et compte tenu de l'état du marché et des réponses 
                    disponibles dans les branches d'assurances concernées.
                \n\nNous vous adressons un projet d’assurance établi auprès formulé EN TOUS RISQUES SAUF,
                    c’est à dire que seules les exclusions listées au contrat, sont opposable à l’objet du contrat.
                    Ces exclusions se trouvent en page 4 du présent projet, ainsi qu’à la fin de chaque extension
                    de garantie souscrite dans le chapitre 6.
                \n\nNous vous invitons à vérifier les éléments qui constituent cette proposition et notamment 
                    sa conformité aux déclarations ayant servies de base à ce projet et, le cas échéant à nous 
                    faire part de toute modification qui vous semblerait opportune.
                    Au titre de la réalisation des prestations que nous vous proposons, nous nous engageons à réaliser nos prestations dans les règles de l’art et à exécuter les obligations à notre charge avec tout le soin en usage dans la profession. 
                    \n\n    Si cette proposition recueille votre agrément, nous vous remercions de bien vouloir nous la retourner datée, signée et dûment revêtue de la mention “Bon pour Accord” .
                    \n    \n    Dans l’attente de la suite à donner,\n    \n    Bien cordialement,
                    \n\n    L'équipe Ovatio`,
                template: 'contractAttachment',
                to: '07fortune@gmail.com',
                locals: {
                    clientName: client.businessName,
                    manifestationName: manifestation.eventName,
                    budget: manifestation.budget,
                    hasBombingExtension: manifestation.hasBombingExtension,
                    bombingNumberOfDay: manifestation.bombingNumberOfDay,
                    hasBadWeatherExtension: manifestation.hasBadWeatherExtension,
                    hasCrowShortageExtension: manifestation.hasCrowShortageExtension,
                    creationDate: manifestation.createdAt,
                    personnes: manifestation.personUnavailabilityList,
                    baseUrl: config.baseUrl

                },
                attachments: [{
                    "Content-Type": "application/pdf",
                    "Filename": "proposition.pdf",
                    "Content": fs.readFileSync(`${path.resolve()}/temp/${req.body.pdfName}.pdf`).toString('base64'),
                }]
            })
        })
        .then(function () {
            res.status(200).send('ok')
        })
        .catch(function (err) {
            console.log('error:', err)
            res.status(400).send(err)
        })
}
