var countriesData = require('./../data/country');
var waitUntil = require('wait-until');
var models  = require('./../models');


let findOrCreate = function (data) {
    return models.country.findOrCreate({
            where: {label: data.label},
            defaults: {
                id: data.id,
                label: data.label,
            }
        }
    )
};

module.exports.updateCountries = function(){
    const NUMBER_REQ_ALLOWED = 1;
    // 10 secondes between each try
    const INTERVAL = 10000;
    // try 100 time;
    const TIME = 100;
    var currentPromiseNumber = 0 ;
    var promises = [];
    countriesData.map(function(data){
        promises.push(
            new Promise(
                function (resolve, reject) {
                    waitUntil(INTERVAL, TIME, function condition() {
                        return (currentPromiseNumber < NUMBER_REQ_ALLOWED);
                    }, function done(result) {
                        currentPromiseNumber ++;
                        // result is true on success or false if the condition was never met
                        promises.push(findOrCreate(data).then(
                            data => {
                                currentPromiseNumber--;
                                resolve(data)
                            }
                        ).catch(
                            err => {
                                reject(err);
                            }
                        ))
                    });
                }
            )
        )
    });
    return Promise.all(promises);
};

