/**
 * @module Controller User
 */
var path = require('path');
var env = process.env.NODE_ENV || 'development';
var UsersService = require('./../services/UsersService');
var RolesService = require('./../services/RolesService');
var PersonInfosService = require('./../services/PersonInfosService');
var token = require('../modules/userToken');
var mail = require('../modules/mail');
var config = require(path.dirname(__dirname) + '/config/config.json')[env];

/**
 * Create User
 * @param user {json} - json data of user
 * @returns {Promise}
 */
module.exports.create = function (user) {
	var promises = [];
	if(user.person_info){
		promises.push(PersonInfosService.findOrCreate(user.person_info).then(
			function (data) {
				// personInfoDao = data[0]
				return data[0];
			}
		))
	}
	if(user.role){
		promises.push(RolesService.getByLabel(user.role.label).then(
			function (data) {
				return data;
			}
		))
	}
	return Promise.all(promises).then(
		function (data) {
			// personInfoDao = data[0]
			// role = data[1]
			return UsersService.create(user, data[0], data[1] )
		}
	)
};

function sendPwdResetMail(pwdResetToken, user) {
    mail.sendEmail({
        subject: 'Réinitialisation de mot de passe',
        text: `<p>Bonjour ${user.firstName},<br>
					Vous venez de demander à réinitialiser votre mot de passe.<br>
					Rendez vous sur <a href="${config.baseUrl}/resetPassword?token=${pwdResetToken}">cette page</a><br>
					<br>
					Cordialement, l'équipe Ovatio</p>
				`,
        template: 'resetPwd',
        to: user.email,
        locals: {
            title: 'Réinitialisation de mot de passe',
            subtitle: `<p>Bonjour ${user.firstName},<br>
					Vous venez de demander à réinitialiser votre mot de passe.<br>
					Rendez vous sur <a href="${config.baseUrl}/resetPassword?token=${pwdResetToken}">cette page</a><br>
					<br>
					Cordialement, l'équipe Ovatio</p>
				`,
            baseUrl: config.baseUrl,
        }
    });
}

module.exports.sendResetMail = function (body) {
    return UsersService.getByEmail(body.email).then(user => {
        if(!user) throw {status: 500, code: 1, message: 'L\'utilisateur n\'existe pas'};
        let pwdResetToken = token.registerUserPwdResetToken(user);

        sendPwdResetMail(pwdResetToken, user);

        return true;
    })
}

module.exports.resetPwd = function (body) {
    let userId = token.verifyUserPwdToken(body.token);

    if(!userId) return Promise.reject({status: 500, code: 1, message: 'Ce lien est invalide ou a expiré'});

    if(!body.password) return Promise.reject({status: 500, code: 2, message: 'Le mot de passe est obligatoire'});
    if(!body.confirm || body.password !== body.confirm) return Promise.reject({status: 500, code: 3, message: 'Les mots de passe ne correspondent pas.'});


    return UsersService.editPasswordById(userId, body.password).then(user => {
        token.deleteToken(body.token);
        return user;
    });
}
