/**
 * @module Controller Zipcode
 */
var ZipcodeService = require('./../services/ZipcodeService');
var zipCodeData = require('./../data/zipcode');
var waitUntil = require('wait-until');
/**
 * create zip code if it is not exist
 * @returns {Promise}
 */
module.exports.updateZipcode = function(){
    const NUMBER_REQ_ALLOWED = 1;
    // 10 secondes between each try
    const INTERVAL = 10000;
    // try 100 time;
    const TIME = 100;
    var currentPromiseNumber = 0 ;
    var promise_zipCode = [];
    zipCodeData.map(function(data){
        promise_zipCode.push(
            new Promise(
                function (resolve, reject) {
                    waitUntil(INTERVAL, TIME, function condition() {
                        return (currentPromiseNumber < NUMBER_REQ_ALLOWED);
                    }, function done(result) {
                        currentPromiseNumber ++;
                        // result is true on success or false if the condition was never met
                        promise_zipCode.push(ZipcodeService.findOrCreate(data).then(
                            data => {
                                currentPromiseNumber--;
                                resolve(data)
                            }
                        ).catch(
                            err => {
                                reject(err);
                            }
                        ))
                    });
                }
            )
        )
    });
    return Promise.all(promise_zipCode);
};

