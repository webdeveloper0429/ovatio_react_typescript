/**
 * @module Controller Devises
 */
var DevisesService = require('./../services/DevisesService');
var deviseListData = require('./../data/devises');
/**
 * create devises if not exist
 * @returns {Promise}
 */
module.exports.updateDevises = function () {
    let promisesItem = [];
    deviseListData.forEach( devise => {
        promisesItem.push(DevisesService.findOrCreate(devise))
    });
    return Promise.all(promisesItem);
};

