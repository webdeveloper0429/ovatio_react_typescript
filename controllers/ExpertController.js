/**
 * @module Controller Expert
 */

var db = require('../models')
var models = require('./../models')
var ExpertService = require('../services/ExpertService')
var RoleService = require('../services/RolesService')

/**
 * get count of expert
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.getCount = (req, res, next) => {
	models.expert.count().then(result => {
		return res.status(200).json(result)
	})
}

/**
 * get experts
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.loadExperts = (req, res, next) => {
	var queryParams = {}
	if (req.query.offset)
		queryParams.offset = parseInt(req.query.offset)
	if (req.query.limit)
		queryParams.limit = parseInt(req.query.limit)
	var sortable = ['businessName', 'address1', 'zipCode', 'city']
	if (sortable.indexOf(req.query.sort) > -1)
		queryParams.order = [[req.query.sort, req.query.order === 'DESC' ? 'DESC' : 'ASC']]
	if (req.query.search)
		queryParams.where = {
			businessName: {
				$like: '%' + req.query.search + '%'
			}
		}
	else if (req.query.id)
		queryParams.where = {
			id: {
				$eq: req.query.id
			}
		}

	models.expert.all(queryParams).then(results => {
		return res.status(200).json(results)
	}).catch(next)
}

/**
 * get experts
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.addExpert = (req, res, next) => {
	RoleService.getByLabel("EXPERT")
		.then(role => ExpertService.create(req.body.expert, role))
		.then(result => res.status(200).json(result))
		.catch(next)
}

/**
 * get expert by id
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.loadExpert = (req, res, next) => {
	ExpertService.getById(req.params.id).then(result => {
		return res.status(200).json(result)
	}).catch(next)
}

/**
 * update expert
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.updateExpert = (req, res, next) => {
	RoleService.getByLabel("EXPERT")
		.then(role => ExpertService.edit(req.params.id, req.body.expert, role))
		.then(result => res.status(200).json(result))
		.catch(next)
}

/**
 * delete expert
 * @param req {object} - express req object
 * @param res {object} - express res object
 * @param next {object} - express next object
 * @returns {json}
 */
module.exports.deleteExpert = (req, res, next) => {
	models.contact.destroy({
		where: {
			expert_id: req.params.id
		}
	}).then(
		models.expert.destroy({
			where: {
				id: req.params.id
			}
		}).then(result => {
			return res.status(200).json(result)
		}).catch(next)
		)
}
