/**
 * @module Routes File Upload
 */

var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var fs = require('fs');
var authenticate = require('./../../modules/authenticate');
var UploadedResourceService = require('./../../services/UploadedResourcesService');

const baseUrl = path.resolve();

const imageFilter = function (req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error("FileTypeErr"), false);
    }
    cb(null, true);
};

const UPLOAD_PATH = 'uploads/avatar';
const avatarUpload = multer({ dest: `${UPLOAD_PATH}/`, fileFilter: imageFilter });

/**
 *  get default cover picture.
 */
router.get('/cover',
    authenticate.isAuthenticated(),
    (req, res) => {
        res.sendFile(`${baseUrl}/client/img/profile_bg.jpg`);
    }
)

/**
 * Get avatar via filename
 * @param id {text} - file name
 */
router.get('/:id',
    authenticate.isAuthenticated(),
    (req, res) => {
        UploadedResourceService.findByName(req.params.id).then(function (data) {
            if (!data) {
                res.sendStatus(404);
                return;
            };
            res.setHeader('Content-Type', data.mimetype);
            fs.createReadStream(path.join(UPLOAD_PATH, data.filename)).pipe(res);
        }).catch(function (err) {
            res.status(404).send(err);
        });
    }
)

/**
 * upload file to server
 * @param avatar {file} - image file to save
 */
router.post('/',
    authenticate.isAuthenticated(),
    avatarUpload.single('avatar'),
    (req, res, next) => {
        UploadedResourceService.create(req.file).then(function (data) {
            res.status(200).send(data);
        }).catch(function (err) {
            res.status(404).send(err);
        });
    }
);

module.exports = router;