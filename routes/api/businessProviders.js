/**
 * @module Routes Business Provider
 */

var express = require('express');
var router = express.Router();
var BusinessProvidersService = require('./../../services/BusinessProvidersService');
var BusinessProviderController = require('./../../controllers/BusinessProviderController');
var authenticate = require('./../../modules/authenticate');

/**
 * Get all business provider
 */
router.get('/',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('GET_BUSINESS_PROVIDER_LIST'),
	function (req, res, next) {
		BusinessProvidersService.readAll(req.query).then(
			function (data) {
				res.status(200).send(data)
			}
		)
	});

/**
 * Get the number of all entries
 */
router.get('/count',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('GET_BUSINESS_PROVIDER_LIST'),
	function (req, res) {
		BusinessProvidersService.count(req.query).then(function (data) {
			res.status(200).send({
				count: data
			});
		}).catch(function (err) {
			res.status(404).send(err);
		})
	});

/**
 * Create business provider
 */
router.post('/',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('CREATE_BUSINESS_PROVIDER_LIST'),
	function (req, res, next) {
		BusinessProviderController.create(req.body).then(
			function (data) {
				res.status(200).send(data)
			}
		).catch((error)=>{
			res.status(error.status).send(error.err);
        })
	});

/**
 * Get business provider by Id
 */
router.get('/:id',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('GET_BUSINESS_PROVIDER'),
	function (req, res) {
		BusinessProvidersService.readById(req.params.id).then(
			function (data) {
				res.status(200).send(data)
			}, function (err) {
				res.status(err.status).send(err)
			}
		)
	});


/**
 * Update business provider by Id
 */
router.put('/:id',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('UPDATE_BUSINESS_PROVIDER'),
	function (req, res) {
		BusinessProvidersService.edit(req.params.id, req.body).then(
			function (data) {
				res.status(200).send(data)
			}
		).catch((error)=>{
            res.status(error.status).send(error.err);
        })
	});

/**
 * Delete business provider by Id
 */
router.delete('/:id',
	authenticate.isAuthenticated(),
	//authenticate.hasRight('DELETE_BUSINESS_PROVIDER'),
	function (req, res) {
		BusinessProvidersService.deleteById(req.params.id).then(
			function (data) {
				res.sendStatus(200).send()
			}
		)
	});

router['patch']('/:id',
    authenticate.isAuthenticated(),
    //authenticate.hasRight('UPDATE_BUSINESS_PROVIDER'),
    function (req, res) {
        BusinessProvidersService.reactivateById(req.params.id).then(
            function (data) {
                res.status(200).send()
            }
        )
    });

module.exports = router;
