/**
 * @module Routes Paragraph
 */
var express = require('express');
var router = express.Router();
var sequelize = require('sequelize');
var ParagraphService = require('./../../services/ParagraphService');
var authenticate = require('./../../modules/authenticate');

/**
 * Get all paragraphs
 */
router.get('/',
	authenticate.isAuthenticated(),
	function(req, res) {
		ParagraphService.get(req.query).then(function(data) {
			res.status(200).send(data);
		}).catch(function(err) {
			res.status(404).send(err);
		});
	}
);

/**
 * Create new paragraph
 */
router.post('/',
	authenticate.isAuthenticated(),
	function(req, res) {
		ParagraphService.create(req.body).then(function(data) {
			res.status(200).send(data);
		}).catch(function(err) {
			res.status(404).send(err);
		});
	}
);

/**
 * Edit paragraph by id
 */
router.put('/:id',
	authenticate.isAuthenticated(),
	function(req, res) {
		ParagraphService.update(req.params.id, req.body).then(function(data) {
			res.status(200).send(data);
		}).catch(function(err) {
			res.status(404).send(err);
		});
	}
);

/** 
 * Delete paragraph by id
 */
router.delete('/:id',
	authenticate.isAuthenticated(),
	function(req, res) {
		ParagraphService.delete(req.params.id).then(function(data) {
			res.status(200).send();
		}).catch(function(err) {
			res.status(404).send(err);
		});
	}
);

/** 
 * Delete paragraph for copy(project to contract)
 */
router.post('/copy',
authenticate.isAuthenticated(),
	function(req, res) {
		ParagraphService.copyProject(req.body.versionId).then(function() {
			res.status(200).send();
		}).catch(function(err) {
			res.status(404).send(err);
		});
	}
);

/**
 * Create english duplicate of french paragraphs
 */
router.post('/copyFrEn',
    authenticate.isAuthenticated(),
    function(req, res) {
        ParagraphService.copyFrToEn(req.body.versionId, req.body.group).then(function() {
            res.status(200).send();
        }).catch(function(err) {
            res.status(404).send(err);
        });
    }
);

module.exports = router;
