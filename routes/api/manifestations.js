/**
 * @module Routes Manifestation
 */

var express = require('express');
var router = express.Router();
var ManifestationsService = require('./../../services/ManifestationsService');
var ManifestationController = require('./../../controllers/ManifestationController');
var RoleService = require('./../../services/RolesService');
var authenticate = require('./../../modules/authenticate');

/**
 * Get all manifestation
 */
router.get('/',
	authenticate.isAuthenticated(),
	function (req, res, next) {
		RoleService.getById(req.user.roleId)
			.then(role => ManifestationsService.readMany(req.query, req.user.id, role))
			.then(manifestations => res.status(200).send(manifestations))
			.catch (err => res.status(404).send(err))
	});

/**
 * Get the count of all entries
 */
router.get('/count',
	authenticate.isAuthenticated(),
	function (req, res) {
		ManifestationsService.count(req.query).then(function (data) {
			res.status(200).send({
				count: data
			});
		}).catch(function (err) {
			res.status(404).send(err);
		})
	});

/**
 * Create manifestation
 */
router.post('/',
	authenticate.isAuthenticated(),
	function (req, res, next) {
		ManifestationController.create(req.body).then(
			function (data) {
				res.status(200).send(data)
			}
		)
	});

	router.post('/duplicate/:id',
    authenticate.isAuthenticated(),
    function (req, res, next) {
        ManifestationController.duplicate(req.params.id, req.body)
            .then(function (data) {
                    res.status(200).send(data)
                })
            .catch(function (err) {
                res.status(err.status).send(err)
            })
    })

router.post('/clone/:id',
    authenticate.isAuthenticated(),
    function (req, res, next) {
        ManifestationController.clone(req.params.id, req.body)
            .then(function (data) {
                    res.status(200).send(data)
                })
            .catch(function (err) {
                res.status(err.status).send(err)
            })
    })

router.post('/sendByMail/:id',
    authenticate.isAuthenticated(),
    ManifestationController.sendByMail)

/**
 * Get manifestation by Id
 */
router.get('/:id',
	authenticate.isAuthenticated(),
	function (req, res) {
		ManifestationsService.readById(req.params.id).then(
			function (data) {
				res.status(200).send(data)
			}, function (err) {
				res.status(err.status).send(err)
			}
		)
	});


/**
 * Update manifestation by Id
 */
router.put('/:id',
	authenticate.isAuthenticated(),
	function (req, res) {
		ManifestationsService.editById(req.params.id, req.body).then(
			function (data) {
				res.status(200).send(data)
			}
		)
		.catch(function (err) {
            res.status(err.status).send(err)
        })
	});

/**
 * Delete manifestation provider by Id
 */
router.delete('/:id',
	authenticate.isAuthenticated(),
	function (req, res) {
		ManifestationsService.deleteById(req.params.id).then(
			function (data) {
				res.status(200).send()
			}
		)
	});

/**
 * Update manifestation by id
 */
router.put('/update/:id', function(req, res) {
	ManifestationsService.update(req.params.id, req.body).then(function(data) {
		res.status(200).send(data);
	}).catch(function(err) {
		res.status(404).send(err);
	});
});

module.exports = router;
