var express = require('express');
var router = express.Router();

var models  = require('./../../models');

router.get('/',
    function (req, res) {
        models.country.findAll().then(function (data) {
            res.status(200).send(data);
        }).catch(function (err) {
            res.status(404).send(err);
        })
    });
module.exports = router;

