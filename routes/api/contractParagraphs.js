/**
 * @module Routes Contract Paragraph
 */
var express = require('express');
var router = express.Router();
var sequelize = require('sequelize');
var ContractParagraphService = require('./../../services/ContractParagraphsService');

/**
 * Get all contract paragraph
 */
router.get('/', function(req, res) {
	ContractParagraphService.get(req.query).then(function(data) {
		res.status(200).send(data);
	}).catch(function(err) {
		res.status(404).send(err);
	});
});

/**
 * Create contract paragraph
 */
router.post('/', function(req, res) {
	ContractParagraphService.create(req.body).then(function(data) {
		res.status(200).send(data);
	}).catch(function(err) {
		res.status(404).send(err);
	});
});

module.exports = router;
