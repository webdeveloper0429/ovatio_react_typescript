/**
 * @module Routes Version
 */
var express = require('express');
var router = express.Router();
var VersionService = require('./../../services/VersionService');
var authenticate = require('./../../modules/authenticate');

/**
 * Get all versions
 */
router.get('/', function(req, res) {
	VersionService.get(req.query).then(function(data) {
		res.status(200).send(data);
	}).catch(function(err) {
		res.status(500).send(err);
	});
});

/**
 * Create new version
 */
router.post('/', function(req, res) {
	VersionService.create(req.body).then(function(data) {
		res.status(200).send(data);
	}).catch(function(err) {
		res.status(500).send(err);
	});
});

/**
 * Update version by id
 */
router.put('/:id', function(req, res) {
    authenticate.findinRightList('EDIT_ACTIVE_VERSION', req).then((hasRight) => {
    	return VersionService.restrictEditionForActive(req.params.id, hasRight);
	}).then((validated) => {
		if(!validated) return res.status(403).send(false);

		return VersionService.update(req.params.id, req.body).then(function(data) {
            res.status(200).send(data);
        }).catch(function(err) {
            res.status(500).send(err);
        });
	})

});

router.put('/:id/activate', function(req, res) {
    VersionService.activate(req.params.id).then(function(data) {
        res.status(200).send(data);
    }).catch(function(err) {
        res.status(500).send(err);
    });
});

/**
 * Delete version by id
 */
router.delete('/:id', function(req, res) {
	VersionService.delete(req.params.id).then(function(data) {
		res.status(200).send();
	}).catch(function(err) {
		res.status(500).send(err);
	});
});

module.exports = router;
