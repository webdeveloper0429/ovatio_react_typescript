/**
 * @module Routes Devises
 */

var express = require('express');
var router = express.Router();
var models = require('./../../models')


/**
 * get all devises
 */
router.get('/', function(req, res, next) {
    models.devise.findAll().then(data => res.status(200).json(data)).catch(next)
});

module.exports = router;