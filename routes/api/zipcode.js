/**
 * @module Routes Zipcode
 */

var express = require('express');
var router = express.Router();
var db = require('../../models')
var models = require('./../../models')

/**
 * get zipcode validity
 */
router.post('/', function(req, res, next) {

	// TODO this should be in a service
	models.zipcode.findOne({
		where: {
			cp: req.body.zipcode
		}
	}).then(result => {

        return res.status(200).json({
            valid: result !== null
        })

	}).catch(next)

});

/**
 * get zipcode data by zipcode or city
 */
router.get('/', function(req, res, next) {

    // TODO this should be in a service
    let query = {where:{}};
    if(req.query.zipcode) {
    	query.where.cp = {$eq: req.query.zipcode};
    } else if(req.query.city) {
    	query.where.ville = {$eq: req.query.city};
    }
    models.zipcode.findOne(query).then(data => res.status(200).json(data)).catch(next)

});

module.exports = router;