var path = require('path');
var env = process.env.NODE_ENV || 'development';
var config = require(path.dirname(__dirname) + '/../config/config.json')[env];
const consts = require(path.dirname(__dirname) + '/../consts.js');
var mailjet;
const Email = require('email-templates');
var htmlToText = require('html-to-text');
const ejs = require('ejs');

module.exports.init = function () {
    return mailjet = require('node-mailjet').connect(config.mailjet_apikey.public, config.mailjet_apikey.private);
}

/**
 * option: { subject: string, text: string, html: string, to: string }
 */
module.exports.sendEmail = function (settings) {
    var templateDir = __dirname + '/template/' + settings.template;
    var template = new Email({
        transport: {
            jsonTransport: true
        },
        views: {
            root: templateDir,
            options: {
                extension: 'ejs'
            }
        }
    });

    return template.render('html', settings.locals)
        .then((compiledHtml) => {
            var text = htmlToText.fromString(compiledHtml, {});
            if (config.block_email_sending == true) {
                return resolve({});
            }

            var mailOptions = {
                'FromEmail': consts.noreply_email_address,
                'FromName': consts.ovatio_name,
                'Subject': settings.subject ? settings.subject : consts.default_mail_subject,
                'Text-part': settings.text ? settings.text : text,
                'Html-part': compiledHtml,
                'Recipients': [{ 'Email': settings.to ? settings.to : '' }],
                'Attachments': settings.attachments,
            };

            return new Promise((resolve, reject) => {
                mailjet
                    .post("send")
                    .request(mailOptions, function (err, result) {
                        if (err) {
                            console.log(err);
                            reject(err.statusCode);
                        }
                        else {
                            console.log(result);
                            resolve(result.body);
                        }

                    });
            })
        })
};
