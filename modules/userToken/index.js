var jwt = require('jsonwebtoken');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config.json')[env];

let tokens = [];

module.exports.registerUserPwdResetToken = function (user) {
    let token = jwt.sign({
            data: { userId: user.id}
        },
        config.secret,
        { expiresIn: 300 }
    );
    tokens.push(token);
    return token;
};

module.exports.verifyUserPwdToken = function (token) {
    if(tokens.indexOf(token) === -1) return null;

    try {
        let signInObject = jwt.verify(token, config.secret);
        return signInObject && signInObject.data ? signInObject.data.userId : null;
    } catch(err) {
        return null;
    }


};

module.exports.deleteToken = function (token) {
    let index = tokens.indexOf(token);
    tokens.splice(index, 1);
};
