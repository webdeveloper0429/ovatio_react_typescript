import {ValidationService} from "../services/validation";

export class Client {
	
	public isPro;
	//Pro Specific
	public businessName;
	public siretNumber;
	public statusId;
	public businessStatus;
	public vatNumber;
	public stdPhone;
	//Private Specific
	public title;
	public lastName;
	public firstName;
	public email;
	public phone;
	//Common
	public relationShipManagerId;
	public stateId;
	public address;
	public addressCompl;
	public postalCode;
	public city;
	public countryId;
	public businessProviderId;
	public contacts;


	constructor() {
		this.hydrate({});
	}

	hydrate(object){
		this.isPro = object.isPro? true : false;
		//Pro Specific
		this.businessName = object.businessName || "";
		this.siretNumber = object.siretNumber || "";
		this.statusId = object.statusId || 1;
		this.businessStatus = object.businessStatus || null;
		this.vatNumber = object.vatNumber || "";
		this.stdPhone = object.stdPhone || "";
		//Private Specific
		this.title = object.title || 0;
		this.lastName = object.lastName || "";
		this.firstName = object.firstName || "";
		this.email = object.email || "";
		this.phone = object.phone || "";
		//Common
		this.relationShipManagerId = object.relationShipManagerId || 0;
		this.stateId = object.stateId || 0;
		this.address = object.address || "";
		this.addressCompl = object.addressCompl || "";
		this.postalCode = object.postalCode || "";
		this.city = object.city || "";
		this.countryId = object.countryId || 0;
		this.businessProviderId = object.businessProviderId || 0;
		this.contacts = object.contacts || [];
	}
	validationFn(attributName, value){
		switch (attributName){
			case "businessName":
			case "statusId":
			case "title":
			case "firstName":
			case "lastName":
			case "relationShipManagerId":
			case "stateId":
			case "businessProviderId":
				return ValidationService.validationNotEmpty(value);
			case "stdPhone":
			case "phone":
				return ValidationService.validationPhoneNumber(value);
			case "siretNumber":
				return ValidationService.validationSiret(value);
			case "email":
				return ValidationService.validationEmail(value);
			default:
				return true;
		}
	}
	validateObject(){
		if(this.isPro){
			var res =   this.validationFn("businessName", this.businessName) && 
						this.validationFn("siretNumber", this.siretNumber) && 
						this.validationFn("statusId", this.statusId) && 
						this.validationFn("relationShipManagerId", this.relationShipManagerId) && 
						this.validationFn("stateId", this.stateId) && 
						this.validationFn("businessProviderId", this.businessProviderId)
		}
		else{
			var res = 	this.validationFn("title", this.title) && 
						this.validationFn("firstName", this.firstName) && 
						this.validationFn("lastName", this.lastName) && 
						this.validationFn("email", this.email) && 
						this.validationFn("relationShipManagerId", this.relationShipManagerId) && 
						this.validationFn("stateId", this.stateId) && 
						this.validationFn("businessProviderId", this.businessProviderId)
		}
		return res;
	}
	validateObjectSimple(){
		var res =   this.validationFn("businessName", this.businessName) && 
					this.validationFn("siretNumber", this.siretNumber)
		return res;
	}
}