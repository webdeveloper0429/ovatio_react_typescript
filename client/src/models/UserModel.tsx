import { ValidationService } from "../services/validation";

export class User {

	public id;
	public genre;
	public roleId;
	public lastName;
	public firstName;
	public address;
	public address2;
	public zipCode;
	public city;
	public email;
	public phone;
	public mobilePhone;
	public password;
	public isActive;
	public isDeleted;

	constructor() {
		this.hydrate({})
	}

	hydrate(object) {
		this.id = object.id || null;
		this.genre = object.genre || "";
		this.roleId = object.roleId || "";
		this.lastName = object.lastName || '';
		this.firstName = object.firstName || '';
		this.address = object.address || '';
		this.address2 = object.address2 || '';
		this.zipCode = object.zipCode || '';
		this.city = object.city || '';
		this.email = object.email || '';
		this.phone = object.phone || '';
		this.mobilePhone = object.mobilePhone || '';
		this.password = object.password || '';
		this.isActive = object.isActive ? 1 : 0;
		this.isDeleted = false;
	}

	validationFn(attributName, value) {
		switch (attributName) {
			case "roleId":
			case "firstName":
			case "lastName":
			case "city":
				return ValidationService.validationNotEmpty(value);
			case "phone":
				return ValidationService.validationPhoneNumber(value) || ValidationService.validationPhoneNumber(this.mobilePhone);
			case "mobilePhone":
				return ValidationService.validationPhoneNumber(value) || ValidationService.validationPhoneNumber(this.phone);
			case "zipCode":
				return ValidationService.validationZipcode(value);
			case "email":
				return ValidationService.validationEmail(value);
			default:
				return true;
		}
	}
	setDeleted() {
		this.isDeleted = true;
	}

	validateObject() {
		return this.validationFn('roleId', this.roleId)
			&& this.validationFn('firstName', this.firstName)
			&& this.validationFn('lastName', this.lastName)
			&& this.validationFn('city', this.city)
			&& this.validationFn('email', this.email)
			&& this.validationFn('phone', this.phone)
			&& this.validationFn('mobilePhone', this.mobilePhone)
	}
	validateObjectModal(){
		return this.validationFn('firstName', this.firstName)
			&& this.validationFn('lastName', this.lastName)
			&& this.validationFn('email', this.email)
			&& this.validationFn('city', this.city)
			&& this.validationFn('phone', this.phone)
			&& this.validationFn('mobilePhone', this.mobilePhone)
	}
}
