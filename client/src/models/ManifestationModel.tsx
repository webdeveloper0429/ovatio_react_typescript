import {ValidationService} from "../services/validation";

export class Manifestation{
	public eventName;
	public totalDeclaredBudget;
	public type;
	public accountManagerId;
	public policyLanguage;
	public currency;
	public businessProviderId;
	public isBusinessProvider;
	public businessProviderFeesRate;
	public hasAdditionalInsuredParty;
	public additionalInsuredPartyName;
	public specialConditions;
	public endOfGuaranteeRule;
	public endOfGuaranteeRuleOnePercentDurationOfTheShow;
	public endOfGuaranteeRuleOneHasPlayed;
	public endOfGuaranteeRuleOneHasOption;
	public endOfGuaranteeRuleTwoActNumberIsOver;
	public endOfGuaranteeRuleThreeTimeIsOver;
	public endOfGuaranteeRuleFourCondition;
	public isCoInsurance;
	public mainInsurerId;
	public rateInsurer;
	public coInsurerId;
	public hasExpert;
	public expertId;
	public averageClause;
	public averagePremiumClause;
	public contractualIndemnityLimit;
	public contractualIndemnityLimitRate;
	public averageFinancialCommitment;
	public subscriptionDeadLine;
	public inceptionDate;
	// public eventCategory;
	// public eventStartDate;
	// public eventEndDate;
	// public dateType;
	// public numberOfInsuredDates;
	// public totalBudget;
	public unavailabilityRate;
	public hasSpecialDispositionExtension;
	public specialDisposition;
	public hasUnavailabiliytExtension;
	public isFeeDeducted;
	public artisteFee;
	public insuredBudget;//more
	public hasBadWeatherExtension;
	public badWeatherRate;
	public percentBudgetToBadWeather;
	public weatherBudget;
	public hasCrowShortageExtension;
	public franchise;
	public hasBombingExtension;
	public bombingRate;
	public bombingNumberOfDay;
	public bombingNumberOfKilometre;
	public bombingHasOptionThreat;
	public bombingHasOptionRecommendation;
	public hasMoralReasonExtension;
	public hasExpertFeeExtension;
	public expertWarrantyAmount;
	public expertFranchise;
	public BNSType;
	public category;
	public customCategory;
	public eventDate;
	public eventEndDate;
	public artist;
	public dateType;
	public numberInsured;
	public budget;
	public manifestationDates //////////json
	public applicableFranchise;
	public comprehensiveInsuranceRate;
	public BNS;
	public franchiseAmount;

	// public notBNSContribution;
	// public BNSDisable;
	// public wholeContribution;
	// public scheduleData;
	
	public exposedBudgetUnavailability;
	public exposedBudgetBadWeather;
	public exposedBudgetBombing;
	public excludingTaxRateUnavailability;
	public excludingTaxRateBadWeather;
	public excludingTaxRateBombing;
	public ovationCommission;
	public ovationBSNCommission;
	public taxRate;
	public isPersonUnavailability;
	public numberPersonInsured;
	public isTechnicianIndispensable;
	public numberTechnicianInsured;
	public isTechnicianInsuredNamed;
	public personUnavailabilityList; //link table
	public technicianUnavailabilityList;	//link table
	public coInsurerList;
	public clientId;
	public status;
	public sortPDF;
	public projectNumber;
	public contractNumber;


	constructor(){
		this.hydrate({});
	}

	hydrate(object){
		this.eventName = object.eventName || '';
		this.totalDeclaredBudget = object.totalDeclaredBudget || '';
		this.type = object.type || '';
		this.accountManagerId = object.accountManagerId || '';
		this.policyLanguage = object.policyLanguage || 'french';
		this.currency = object.currency || '';
		this.businessProviderId = object.businessProviderId || '';
		this.isBusinessProvider = object.isBusinessProvider || '';
		this.businessProviderFeesRate = object.businessProviderFeesRate || '';
		this.hasAdditionalInsuredParty = object.hasAdditionalInsuredParty || '';
		this.additionalInsuredPartyName = object.additionalInsuredPartyName || '';
		this.specialConditions = object.specialConditions || '';
		this.endOfGuaranteeRule = object.endOfGuaranteeRule || '';
		this.endOfGuaranteeRuleOnePercentDurationOfTheShow = object.endOfGuaranteeRuleOnePercentDurationOfTheShow || '';
		this.endOfGuaranteeRuleOneHasPlayed = object.endOfGuaranteeRuleOneHasPlayed || '';
		this.endOfGuaranteeRuleOneHasOption = object.endOfGuaranteeRuleOneHasOption || '';
		this.endOfGuaranteeRuleTwoActNumberIsOver = object.endOfGuaranteeRuleTwoActNumberIsOver || '';
		this.endOfGuaranteeRuleThreeTimeIsOver = object.endOfGuaranteeRuleThreeTimeIsOver || '';
		this.endOfGuaranteeRuleFourCondition = object.endOfGuaranteeRuleFourCondition || '';
		this.isCoInsurance = object.isCoInsurance || '';
		this.mainInsurerId = object.mainInsurerId || '';
		this.rateInsurer = object.rateInsurer || '';
		this.coInsurerId = object.coInsurerId || '';
		this.hasExpert = object.hasExpert || '';
		this.expertId = object.expertId || '';
		this.averageClause = object.averageClause || '';
		this.averagePremiumClause = object.averagePremiumClause || '';
		this.contractualIndemnityLimit = object.contractualIndemnityLimit || '';
		this.contractualIndemnityLimitRate = object.contractualIndemnityLimitRate || '';
		this.averageFinancialCommitment = object.averageFinancialCommitment || '';
		this.subscriptionDeadLine = this.formatDate(object.subscriptionDeadLine || '');
		this.inceptionDate = object.inceptionDate || null;
		// this.eventCategory = object.eventCategory || null;
		// this.eventStartDate = object.eventStartDate || null;
		// this.eventEndDate = object.eventEndDate || null;
		// this.dateType = object.dateType || null;
		// this.numberOfInsuredDates = object.numberOfInsuredDates || null;
		// this.totalBudget = object.totalBudget || null;
		this.hasUnavailabiliytExtension = object.hasUnavailabiliytExtension || '';
		this.unavailabilityRate = object.unavailabilityRate || '';
		this.hasSpecialDispositionExtension = object.hasSpecialDispositionExtension || '';
		this.specialDisposition = object.specialDisposition || '';
		this.isFeeDeducted = object.isFeeDeducted || '';
		this.artisteFee = object.artisteFee || '';
		this.insuredBudget = object.insuredBudget || '';
		this.hasBadWeatherExtension = object.hasBadWeatherExtension || '';
		this.badWeatherRate = object.badWeatherRate || '';
		this.percentBudgetToBadWeather = object.percentBudgetToBadWeather || '';
		this.weatherBudget = object.weatherBudget || '';
		this.hasCrowShortageExtension = object.hasCrowShortageExtension || '';
		this.franchise = object.franchise || '';
		this.hasBombingExtension = object.hasBombingExtension || '';
		this.bombingRate = object.bombingRate || '';
		this.bombingNumberOfDay = object.bombingNumberOfDay || '';
		this.bombingNumberOfKilometre = object.bombingNumberOfKilometre || '';
		this.bombingHasOptionThreat = object.bombingHasOptionThreat || '';
		this.bombingHasOptionRecommendation = object.bombingHasOptionRecommendation || '';
		this.hasMoralReasonExtension = object.hasMoralReasonExtension || '';
		this.hasExpertFeeExtension = object.hasExpertFeeExtension || '';
		this.expertWarrantyAmount = object.expertWarrantyAmount || '';
		this.expertFranchise = object.expertFranchise || '';
		this.BNSType = object.BNSType || '';
		this.category = object.category || '';
		this.eventDate = object.eventDate || null;
		this.eventEndDate = object.eventEndDate || null;
		this.artist = object.artist || '';
		this.dateType = object.dateType || '';
		this.numberInsured = object.numberInsured || '';
		this.budget = object.budget || '';
		this.manifestationDates = object.manifestationDates || {};//json
		this.applicableFranchise = object.applicableFranchise || '';
		this.comprehensiveInsuranceRate = object.comprehensiveInsuranceRate || '';
		this.BNS = object.BNS || '';
		this.franchiseAmount = object.franchiseAmount || '';
		
		// this.notBNSContribution = object.notBNSContribution || '';
		// this.BNSDisable = object.BNSDisable || '';
		// this.wholeContribution = object.wholeContribution || '';
		// this.scheduleData = object.scheduleData || {};
		
		this.exposedBudgetUnavailability = object.exposedBudgetUnavailability || '';
		this.exposedBudgetBadWeather = object.exposedBudgetBadWeather || '';
		this.exposedBudgetBombing = object.exposedBudgetBombing || '';
		this.excludingTaxRateUnavailability = object.excludingTaxRateUnavailability || '';
		this.excludingTaxRateBadWeather = object.excludingTaxRateBadWeather || '';
		this.excludingTaxRateBombing = object.excludingTaxRateBombing || '';
		this.ovationCommission = object.ovationCommission || '';
		this.ovationBSNCommission = object.ovationBSNCommission || '';
		this.taxRate = object.taxRate || 9;
		this.isPersonUnavailability = object.isPersonUnavailability || '';
		this.numberPersonInsured = object.numberPersonInsured || 0;
		this.isTechnicianIndispensable = object.isTechnicianIndispensable || '';
		this.numberTechnicianInsured = object.numberTechnicianInsured || 0;
		this.isTechnicianInsuredNamed = object.isTechnicianInsuredNamed || '';
		this.personUnavailabilityList = object.personUnavailabilityList || []; //link table
		this.technicianUnavailabilityList = object.technicianUnavailabilityList || [];	//link table
		this.coInsurerList = object.coInsurerList || [];
		this.clientId = object.clientId || null;
		this.status = object.status || '';
		this.sortPDF = object.sortPDF || 0;
		this.projectNumber = object.projectNumber || '';
		this.contractNumber = object.contractNumber || '';
	}
    formatDate(date) {
    	if(date == '' || date == undefined){
    		return '';
    	}
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
	validationFn(attributName, value){
		switch (attributName){
			case "eventName":
			case "accountManagerId":
				return ValidationService.validationNotEmpty(value);
			default:
				return true;
		}
	}
	validateObject(){
		var res =   this.validationFn("eventName", this.eventName) && 
					this.validationFn("accountManagerId", this.accountManagerId);
		return res;
	}
	setScheduleDate(object){
		this.category = object.category;
		this.customCategory = object.customCategory;
		this.eventDate = object.eventDate.value;
		this.eventEndDate = object.eventEndDate.value;
		this.artist = object.artist;
		this.dateType = object.dateType;
		this.numberInsured = object.numberInsured;
		this.budget = object.budget;
		this.manifestationDates = JSON.stringify(object.dates);
	}
}
