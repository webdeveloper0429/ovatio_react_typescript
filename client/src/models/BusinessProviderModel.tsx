import {ValidationService} from "../services/validation";

export class BusinessProvider {
	
	public type;
	public businessName;
	public businessProviderLastName;
	public businessProviderFirstName;
	public siret;
	public status;
	public commissionPercent;
	public address;
	public address2;
	public zipCode;
	public city;
	public countryId;
	public email;
	public phone;
	public orias;
	public contacts;
	public deletedAt;
	

	constructor() {
		this.hydrate({});
	}

	hydrate(object){
		this.type = object.type || 'professional';
		this.businessName = object.businessName || '';
		this.businessProviderLastName = object.businessProviderLastName || '';
		this.businessProviderFirstName = object.businessProviderFirstName || '';
		this.siret = object.siret || '';
		this.status = object.status || 'inProgress';
		this.commissionPercent = object.commissionPercent || '';
		this.address = object.address || '';
		this.address2 = object.address2 || '';
		this.zipCode = object.zipCode || '';
		this.city = object.city || '';
		this.countryId = object.countryId || '';
		this.email = object.email || '';
		this.phone = object.phone || '';
		this.orias = object.orias || '';
		this.contacts = object.contacts || [];
		this.deletedAt = object.deletedAt || null;
	}
	validationFn(attributName, value){
		switch (attributName){
			case "businessName":
			case "orias":
			case "businessProviderLastName":
			case "businessProviderFirstName":
			case "status":
				return ValidationService.validationNotEmpty(value);
			case "siret":
				return ValidationService.validationSiret(value);
			case "zipcode":
				return ValidationService.validationZipcode(value);
			case "email":
				return ValidationService.validationEmail(value);
			case "phone":
				return ValidationService.validationPhoneNumber(value);
			default:
				return true;
		}
	}
	validateObject(){
		if(this.type == 'professional'){
			var res =   this.validationFn("businessName", this.businessName) &&
						this.validationFn("orias", this.orias) &&
						this.validationFn("siret", this.siret) &&
						this.validationFn("status", this.status) && 
						this.validationFn("email", this.email) && 
						this.validationFn("phone", this.phone) 
		}
		else{
			var res = 	this.validationFn("businessProviderLastName", this.businessProviderLastName) && 
						this.validationFn("businessProviderFirstName", this.businessProviderFirstName) && 
						this.validationFn("status", this.status) && 
						this.validationFn("email", this.email) && 
						this.validationFn("phone", this.phone) 
		}
		return res;
	}
}