import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {ClientService, getStates, getBusinessStatutses} from "../../services/client"
import {UserService} from "../../services/user";
import {browserHistory} from "react-router";
import {BusinessProviderService} from "../../services/businessProvider";
import AddUser from "../Global/addUser/AddUser";
import {Client} from "../../models/ClientModel";
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomCountrySelect from './../Global/input/CustomCountrySelect';
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import CustomSwitchInput from './../Global/input/CustomSwitchInput';
import { toast } from 'react-toastify';

export interface ClientEditProps {
	params: any;
	t:any;
}

@translate(['client', 'form','countries', 'toaster'], {wait: true})
export class ClientEdit extends React.Component<ClientEditProps, any> {
	public state: any;
	public stateData: any;
	public countries: any;

	constructor(props: ClientEditProps) {
		super(props);
		this.toggleState = this.toggleState.bind(this);
		this.saveClient = this.saveClient.bind(this);
		this.setClient = this.setClient.bind(this);

        this.state = {
			canSubmit: false,
			client: new Client(),
			helperData:{
				statusListArr: [],
				relationShipManagerListArr: [],
				stateListArr: [],
				businessProviderListArr: []
			}
		};
		getBusinessStatutses().then(
			resp => {
				resp.data.map((row)=>{
					this.state.helperData.statusListArr.push({
						'value': row.id,
						'label': row.label
					})
				})
				this.setState({helperData: this.state.helperData});
			}
		);

		this.stateData = getStates();
		this.stateData.map((row)=>{
			this.state.helperData.stateListArr.push({
				'value': row.id,
				'label': row.label
			})
		})

		UserService.findUsersInOvatioGroup().then(
			resp => {
				resp.data.map((row)=>{
					this.state.helperData.relationShipManagerListArr.push({
						'value': row.id,
						'label': `${row.firstName} ${row.lastName}`
					})
				})
				this.setState({helperData: this.state.helperData});
			}
		);

		BusinessProviderService.find({}).then(
			resp => {
				resp.data.map((row)=>{
					this.state.helperData.businessProviderListArr.push({
						'value': row.id,
						'label': row.businessName
					})
				})
				this.setState({helperData: this.state.helperData});
			}
		);
	}

	componentDidMount() {
		this.loadDataFromServer();
	}

	loadDataFromServer() {
		if (this.props.params.id) {
			ClientService.getOneClient(this.props.params.id).then(
				(resp) => {
					let row = resp.data[0];
					this.state.client.hydrate(row);
					this.setState({ client: this.state.client });
				}
			);
		}
	}

	toggleState(event) {
		let client = this.state.client
		client.isPro = event.target.value == 1
		this.setState({	client: client });
	}

	setClient(attr, val) {
		this.state.client[attr] = val;
		this.setState({client: this.state.client});
	}



    saveClient(event: any){
		event.preventDefault();
		if(this.state.canSubmit){
			let promise:any;
			if(this.props.params.id){
				promise = ClientService.update(this.props.params.id, this.state.client).then(
					(resp)=>{
						browserHistory.push('/client')
					}
				);
			}
			else{
                promise = ClientService.insert(this.state.client).then(
					(resp)=>{
						browserHistory.push('/client')
					}
				);
			}
            promise.then(() => toast.success(this.props.t('toaster:saveSuccess'))).catch(() => toast.error(this.props.t('toaster:error')))
		}
	}
	checkForm() {
		this.state.canSubmit = this.state.client.validateObject();
	}
	render() {
		const {t} : any = this.props
		this.checkForm()
		return (
			<div className="content-w">
				<div className="content-i">
					<div className="content-box">
						<div className="row">
							<div className="col-sm-12">
								<div className="element-wrapper">
									<h6 className="element-header">
										{
											(this.props.params.id ? t('client:editClientTitle')
												: t('client:createClientTitle'))
										}
									</h6>
									<div className="element-box">
										<form id="formValidate">
											<CustomSwitchInput name="isPro"
															   model={this.state.client.isPro}
															   value1={1}
															   value1Label={t('common:pro')}
															   value2={0}
															   value2Label={t('common:private')}
															   onChangeFn={(newValue) => this.setClient("isPro", newValue)}
											></CustomSwitchInput>
											{	this.state.client.isPro ?
											<div>
												<div className="row">
													<div className="col-md-4">
														<CustomInputText
															name="businessName"
															label={t('form:businessName')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("businessName", newValue)}
															model={this.state.client.businessName}
															errorLabel={t('form:businessNameError')}
														></CustomInputText>
													</div>
													<div className="col-md-4">
														<CustomInputText
															name="siretNumber"
															label={t('form:siretNumber')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("siretNumber", newValue)}
															model={this.state.client.siretNumber}
															errorLabel={t('form:siretNumberError')}
														></CustomInputText>
													</div>
													<div className="col-md-4">
														<CustomInputSelect
															name="statusId"
															label={t('form:statusId')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("statusId", newValue)}
															model={this.state.client.statusId}
															errorLabel={t('form:statusIdError')}
															optionList={this.state.helperData.statusListArr}
														></CustomInputSelect>
													</div>
												</div>
												<div className="row">
													<div className="col-md-4 offset-4">
														<CustomInputText
															name="vatNumber"
															label={t('form:vatNumber')}
															onChangeFn={(newValue) => this.setClient("vatNumber", newValue)}
															model={this.state.client.vatNumber}
														></CustomInputText>
													</div>
													<div className="col-md-4">
														<CustomInputText
															name="stdPhone"
															label={t('form:stdPhone')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("stdPhone", newValue)}
															model={this.state.client.stdPhone}
															errorLabel={t('form:phoneError')}
														></CustomInputText>
													</div>
												</div>
											</div>
											:
											<div>
												<div className="row">
													<div className="col-md-4">
														<CustomInputSelect
															name="title"
															label={t('form:genre')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("title", newValue)}
															model={this.state.client.title}
															errorLabel={t('form:titleError')}
															optionList={[
																	{
																		value: '1',
																		label: 'Madame'
																	},{
																		value: '2',
																		label: 'Monsieur'
																	},{
																		value: '3',
																		label: 'Monsieur et Madame'
																	},{
																		value: '4',
																		label: 'Consorts'
																	},{
																		value: '5',
																		label: 'Indivision'
																	}
																]}
														></CustomInputSelect>
													</div>
													<div className="col-md-4">
														<CustomInputText
															name="lastName"
															label={t('form:lastName')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("lastName", newValue)}
															model={this.state.client.lastName}
															errorLabel={t('form:lastNameError')}
														></CustomInputText>
													</div>
													<div className="col-md-4">
														<CustomInputText
															name="firstName"
															label={t('form:firstName')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("firstName", newValue)}
															model={this.state.client.firstName}
															errorLabel={t('form:firstNameError')}
														></CustomInputText>
													</div>
												</div>
												<div className="row">
													<div className="col-md-4 offset-4">
														<CustomInputText
															name="email"
															label={t('form:email')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("email", newValue)}
															model={this.state.client.email}
															errorLabel={t('form:emailError')}
														></CustomInputText>
													</div>
													<div className="col-md-4">
														<CustomInputText
															name="phone"
															label={t('form:phone')}
															validationFn={this.state.client.validationFn.bind(this.state.client)}
															onChangeFn={(newValue) => this.setClient("phone", newValue)}
															model={this.state.client.phone}
															errorLabel={t('form:phoneError')}
														></CustomInputText>
													</div>
												</div>
											</div>
											}
											<div className="row">
												<div className="col-md-4">
													<CustomInputSelect
														name="relationShipManagerId"
														label={t('form:relationShipManagerId')}
														validationFn={this.state.client.validationFn.bind(this.state.client)}
														onChangeFn={(newValue) => this.setClient("relationShipManagerId", newValue)}
														model={this.state.client.relationShipManagerId}
														errorLabel={t('form:relationShipManagerIdError')}
														optionList={this.state.helperData.relationShipManagerListArr}
													></CustomInputSelect>
												</div>
												<div className="col-md-4">
													<CustomInputSelect
														name="stateId"
														label={t('form:stateId')}
														validationFn={this.state.client.validationFn.bind(this.state.client)}
														onChangeFn={(newValue) => this.setClient("stateId", newValue)}
														model={this.state.client.stateId}
														errorLabel={t('form:stateIdError')}
														optionList={this.state.helperData.stateListArr}
													></CustomInputSelect>
												</div>
											</div>
											<div className="row">
												<div className="col-md-6">
													<CustomInputText
														name="address"
														label={t('form:address')}
														onChangeFn={(newValue) => this.setClient("address", newValue)}
														model={this.state.client.address}
													></CustomInputText>
												</div>
												<div className="col-md-6">
													<CustomInputText
														name="addressCompl"
														label={t('form:address2')}
														onChangeFn={(newValue) => this.setClient("addressCompl", newValue)}
														model={this.state.client.addressCompl}
													></CustomInputText>
												</div>
											</div>
											<div className="row">
												<div className="col-md-4">
													<CustomInputZipCode
														name="postalCode"
														label={t('form:zipCode')}
														onChangeFn={(newValue) => this.setClient("postalCode", newValue)}
														model={this.state.client.postalCode}
														errorLabel={t('form:zipCodeError')}
													></CustomInputZipCode>
												</div>
												<div className="col-md-4">
													<CustomInputText
														name="city"
														label={t('form:city')}
														onChangeFn={(newValue) => this.setClient("city", newValue)}
														model={this.state.client.city}
													></CustomInputText>
												</div>
												<div className="col-md-4">
													<CustomCountrySelect
														name="countryId"
														label={t('form:country')}
														onChangeFn={(newValue) => this.setClient("countryId", newValue)}
														model={this.state.client.countryId}
													></CustomCountrySelect>
												</div>
											</div>
											<div className="row">
												<div className="col-md-4">
													<CustomInputSelect
														name="businessProviderId"
														label={t('form:businessProviderId')}
														validationFn={this.state.client.validationFn.bind(this.state.client)}
														onChangeFn={(newValue) => this.setClient("businessProviderId", newValue)}
														model={this.state.client.businessProviderId}
														errorLabel={t('form:businessProviderIdError')}
														optionList={this.state.helperData.businessProviderListArr}
													></CustomInputSelect>
												</div>
											</div>
											<div className="row">
												<div className="col-md-12">
													<AddUser
														users={this.state.client.contacts}
													></AddUser>
												</div>
											</div>
											<div className="form-buttons-w">
												<button className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')} type="button" onClick={this.saveClient}>{t('common:save')}</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
