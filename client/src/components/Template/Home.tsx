import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {Link, browserHistory} from 'react-router';
import {AssureurService} from "../../services/assureur";
import {VersionService} from "../../services/version";
import {ParagraphService} from "../../services/paragraph";
import {defaultConfirm} from './../Global/confirm/DefaultConfirm';
import {UserService} from "../../services/user";
import {TemplateSelection} from "../../classes/templateSelection";
import {Languages} from "../../enums/languages";
declare var $: any;

export interface TemplateHomeProps {
	params: any;
}

class ConfirmObject {
	text: string;
	callback: any;
}

@translate(['template', 'modal', 'common'], {wait: true})
export class TemplateHome extends React.Component<TemplateHomeProps, any> {

	constructor(props: TemplateHomeProps) {
		super(props);
		this.copyProjectToContract = this.copyProjectToContract.bind(this);
		this.isActiveVersionLocked = this.isActiveVersionLocked.bind(this);
		this.state = {
			insurances: [],
			versions: [],
			paragraphs: [],
			activeInsurance: 0,
			activeVersion: null,
			defaultInsurerOptionVal: 0,
			versionsForOption: [],
			newVersionFrom: 0,
			newVersionLabel: '',
			newVersionActivation: new Date(),
			editVersionLabel: '',
			editVersionActivation: '',
            confirmObject: null,
            canEditActive: false,
            selectedLanguage: TemplateSelection.lang
		};
		UserService.getMe().then((res) => {
			let canEditActive = UserService.hasRight(res.data, 'EDIT_ACTIVE_VERSION');
			this.setState({canEditActive: canEditActive})
		})
		AssureurService.find({showDisabled: false}).then(
			insurances => {
				let insurance_id = insurances.data[this.state.activeInsurance]['id'];
				this.setState({ 
					insurances: insurances.data,
					defaultInsurerOptionVal: insurance_id
				});
				this._retriveVersion(insurance_id);
				VersionService.find({'insurance_id': insurance_id}).then(
					versions => {
						this.setState({	versionsForOption: versions.data	});
					}
				);
			}
		);
	}
	_retriveVersion(insurance_id){
		VersionService.find({'insurance_id': insurance_id}).then(
			versions => {
				this.setState({
					versions: versions.data,
					activeVersion: versions.data.length ? versions.data[0] : null
				});
				if(versions.data.length > 0){
					this._retriveParagraph(this.state.activeVersion.id);
				} else{
					this.setState({
						paragraphs: []
					});
				}
			}
		);
	}
	_retriveParagraph(version_id){
		ParagraphService.find({'version_id': version_id}).then(
			paragraphs => {
				this.setState({
					paragraphs: paragraphs.data
				});
			}
		);
	}
	insertNewVersion(){
		let newData = {
			insurance_id: this.state.insurances[this.state.activeInsurance]['id'],
			label: this.state.newVersionLabel,
		}
		let from_version_id = parseInt(this.state.newVersionFrom);
		VersionService.insert(newData).then(
			newVersions => {
				if(newVersions.data.id){
					ParagraphService.find({'version_id': from_version_id}).then(
						paragraphs => {
							paragraphs.data.map((paragraph, index)=>{
								let new_paragraphData={
									group: paragraph.group,
									version_id: newVersions.data.id,
									level: paragraph.level,
									subLevel: paragraph.subLevel,
									title: paragraph.title,
									condition: paragraph.condition,
									editor: paragraph.editor
								}
								ParagraphService.insert(new_paragraphData).then(
									paragraph => {}
								);
							});
						}
					);
					let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
					this._retriveVersion(insurance_id);
				}
			}
		);
		this.setState({newVersionLabel: ''});
		$("button[data-dismiss='modal']").trigger('click');
	}
	changeCompany(index){
		this.setState({activeInsurance: index});
		let insurance_id = this.state.insurances[index]['id'];
		this._retriveVersion(insurance_id);
		this.changeInsuranceForOptions(insurance_id);
	}
	changeVersion(version){
		this.setState({activeVersion: version});
		this._retriveParagraph(version.id);
	}
	openEditModal(){
		$('#editVersionModal').modal('show');
		let tempLabel = this.state.activeVersion.label;
		let tempActivation = this.state.activeVersion.activation;
		this.setState({editVersionLabel: tempLabel, editVersionActivation: tempActivation});
	}
	askConfirm(confirmObject: ConfirmObject) {
		this.setState({confirmObject: confirmObject});
	}
    resetConfirm() {
		this.setState({confirmObject: null})
	}
	updateVersion(){
		let label = this.state.editVersionLabel;
		let id = this.state.activeVersion.id;
		let updatedData = {
			label: label,
		}
		let updateCallBack = function() {
            VersionService.update(id, updatedData).then(
                result => {
                    if(result.data[0] == 1){
                        let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
                        this._retriveVersion(insurance_id);
                    }
                }
            );
            this.resetConfirm();
            $('#editVersionModal').modal('hide');
		}.bind(this);
		this.askConfirm({text: 'updateMsg', callback: updateCallBack})

	}
    activateVersion() {
        let id = this.state.activeVersion.id;
		let confirmCallback = function() {
            VersionService.activate(id).then(result => {
                let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
                this._retriveVersion(insurance_id);
            });
            this.resetConfirm();
            $('#editVersionModal').modal('hide');
		}.bind(this);
        this.askConfirm({text: 'activateMsg', callback: confirmCallback})
	}
	deleteVersion(){
        let id = this.state.activeVersion.id;
		let deleteCallback = function() {
            this.setState({activeVersion: 0});
            VersionService.remove(id).then(
                () => {
                    let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
                    this._retriveVersion(insurance_id);
                }
            );
            this.resetConfirm();
            $('#editVersionModal').modal('hide');
		}.bind(this);
        this.askConfirm({text: 'deleteMsg', callback: deleteCallback})

	}
	addParagraph(e, group){
		e.preventDefault();
		let addUrl = `/template/add/${group}/${this.state.activeVersion.id}`;
		browserHistory.push(addUrl);
	}
	deleteParagraph(id){
		ParagraphService.delete(id).then(
			() => {
				let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
				this._retriveVersion(insurance_id);
			}
		);
	}
    selectLanguage(language:Languages) {
		TemplateSelection.lang = language;
		this.setState({selectedLanguage: language})
	}
    getVersionClass(version) {
		if(version.isActive) return 'last-selected';
		if(version.activation) return 'locked';
		return 'draft';
	}
    isActiveVersionLocked():boolean {
		if(!this.state.activeVersion) return false;
        return (!this.state.activeVersion.isActive && this.state.activeVersion.activation) || (this.state.activeVersion.isActive && !this.state.canEditActive)
    }
	changeInsuranceForOptions(insurance_id){
		VersionService.find({'insurance_id': insurance_id}).then(
			versions => {
				this.setState({versionsForOption: versions.data});
			}
		);
		this.setState({
			newVersionFrom: 0,
			defaultInsurerOptionVal: insurance_id
		});
	}
	datetimeParse(datetime){
		let dateObj = new Date(datetime);
		let month = dateObj.getUTCMonth() + 1;
		let day = dateObj.getUTCDate();
		let year = dateObj.getUTCFullYear();
		return [year,
          (month>9 ? '' : '0') + month,
          (day>9 ? '' : '0') + day
         ].join('-');
	}

	copyProjectToContract=(e, confirmModalOption)=>{
		e.preventDefault();
		defaultConfirm(confirmModalOption).then(() => {
			ParagraphService.copy(this.state.activeVersion.id).then(
				resp=>{
					let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
					this._retriveVersion(insurance_id);
				}
			)
		}, () => {
			console.log('cancel!');
		});
	}

    copyFrToEn(e, confirmModalOption, group:number) {
        e.preventDefault();
        defaultConfirm(confirmModalOption).then(() => {
            ParagraphService.copyFrToEn(this.state.activeVersion.id, group).then(resp=>{
                let insurance_id = this.state.insurances[this.state.activeInsurance]['id'];
                this._retriveVersion(insurance_id);
            })
		})
	}

	render() {
		const {t} : any = this.props;
		const confirmModalOption={
			okLabel: t('modal:ok'),
			cancelLabel: t('modal:cancel'),
			title: t('modal:areYouSure'),
			confirmation: t('modal:copyProjectToContract')
		}
		const insurance_options = ()=>{
			let items = [];
			items.push(<option key="0" value="0"> -- {t('template:selectCompany')} -- </option>)
			this.state.insurances.map((company, index) => {
				items.push(<option key={index+1} value={company.id}>{company.businessName}</option>);
			});
			return items;
		}
		const version_options = ()=>{
			let items = [];
			items.push(<option key="0" value="0"> -- {t('template:selectVersion')} -- </option>);
			this.state.versionsForOption.map((version, index) => {
				items.push(<option key={index+1} value={version.id}>{version.label}</option>);
			});
			return items;
		}
		var companiesDOM = this.state.insurances.map((company, index) => {
			return (
				<li key={index} className={"company_lists " + (this.state.activeInsurance == index ? 'active' : '')}  onClick={ ()=>{this.changeCompany(index)} }>
					<div><i className="os-icon os-icon-phone-21"></i><span>{company.businessName}</span></div>
				</li>
			)
		});
		var versionsDOM = this.state.versions.map((version, index)=>{
			return (
				<li key={index} className={"version_lists " + (this.state.activeVersion == version ? 'active ' : ' ') + this.getVersionClass(version)}
					onClick={ ()=>{ this.changeVersion(version);} }>
					<div>
						<span>{version.label}</span>
					</div>
					{
						version.activation ?
							<div className="version_timestamp">Active since  {this.datetimeParse(version.activation)}</div> :
							<div className="version_timestamp">{t('draft')}</div>
					}

				</li>
			)
		});
		const table = (group)=>{
			return(
				<div className="message-content">
					<div className="table-responsive">
						<table className="table table-bordered table-v2 table-striped">
							<thead>
								<tr>
									<th>{t('template:level')}</th>
									<th>{t('template:subLevel')}</th>
									<th>{t('template:title')}</th>
									<th>{t('common:actions')}</th>
								</tr>
							</thead>
							<tbody>
							{
								this.state.paragraphs.filter((para:any) => para.lang == this.state.selectedLanguage).map((paragraph, index)=>{
									if(group == paragraph.group){
										return(
											<tr key={index}>
												<td>{paragraph.level}</td>
												<td>{paragraph.subLevel}</td>
												<td>{paragraph.title}</td>
                                                {
                                                    !this.isActiveVersionLocked() ?
                                                        <td className="row-actions">
                                                            <Link to={"/template/edit/" + paragraph.id} ><i className="os-icon os-icon-pencil-2"></i></Link>
                                                            <a href="#" onClick={()=>{this.deleteParagraph(paragraph.id);}}><i className="os-icon os-icon-ui-15"></i></a>
                                                        </td>
                                                        : <td className="row-actions">
															<Link to={"/template/edit/" + paragraph.id + '?locked=1'} ><i className="os-icon os-icon-pencil-2"></i></Link>
														</td>
                                                }

											</tr>
										);
									}
								})
							}
								
							</tbody>
						</table>
					</div>
				</div>
			)
		}
		const pdfListDOM = ()=>{
				return(
					<div className="pdf_list_dom">
						<ul className="nav nav-tabs nav-fill custom_tab_ctrl">
							<li className="nav-item">
								<a aria-expanded="true" className={"nav-link"+ (this.state.selectedLanguage == Languages.fr?' active':'')} data-toggle="tab"
								   onClick={() => this.selectLanguage(Languages.fr)} href="#paraTab">
                                    {t('common:fr')}
								</a>
							</li>
							<li className="nav-item">
								<a aria-expanded="false" className={"nav-link"+ (this.state.selectedLanguage == Languages.en?' active':'')} data-toggle="tab"
								   onClick={() => this.selectLanguage(Languages.en)} href="#paraTab">
                                    {t('common:en')}
								</a>
							</li>
						</ul>
						<div className="tab-content">
							<div className="tab-pane active" id="paraTab" role="tabpanel">


								<div className="aec-full-message-w">
									<div className="aec-full-message">
										<div className="message-head">
											<div className="user-w with-status status-green">
												<div className="user-name">
													<h6 className="user-title">
                                                        {t('template:project')}
													</h6>
												</div>
											</div>
										</div>
                                        {
                                            this.state.versions.length > 0 ?
												<div className="add_lock">
													<div className="btn_div">
														<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()}
																onClick={(e)=>{this.addParagraph(e, 0)}}>
															<i className="os-icon os-icon-ui-22"></i><span>{t('template:addVersionButtonLabel')}</span>
														</button>
														<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()}
																onClick={(e)=>{this.copyProjectToContract(e, confirmModalOption)}}>
															<i className="os-icon os-icon-ui-22"></i><span>{t('template:copyProjectToContract')}</span>
														</button>
														{
															this.state.selectedLanguage == Languages.fr ?
                                                            <button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()}
                                                            		onClick={(e)=>{this.copyFrToEn(e, confirmModalOption, 0)}}>
                                                            	<i className="os-icon os-icon-ui-22"></i><span>{t('template:copyFrToEn')}</span>
                                                            </button> : null
														}
													</div>
													<a href="#">{!this.isActiveVersionLocked() ? <i className="fa fa-unlock"></i> : <i className="fa fa-lock"></i> }</a>
												</div> : null
                                        }
                                        {this.state.versions.length > 0 ? table(0) : null}


									</div>
								</div>
								<div className="aec-full-message-w">
									<div className="aec-full-message">
										<div className="message-head">
											<div className="user-w with-status status-green">
												<div className="user-name">
													<h6 className="user-title">
                                                        {t('template:contract')}
													</h6>
												</div>
											</div>
										</div>
                                        {
                                            this.state.versions.length > 0 ?
												<div className="add_lock">
													<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()} onClick={(e)=>{this.addParagraph(e, 1)}}><i className="os-icon os-icon-ui-22"></i><span>{t('template:addVersionButtonLabel')}</span></button>
                                                    {   this.state.selectedLanguage == Languages.fr ?
														<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()}
																onClick={(e)=>{this.copyFrToEn(e, confirmModalOption, 1)}}>
															<i className="os-icon os-icon-ui-22"></i><span>{t('template:copyFrToEn')}</span>
														</button> : ''
                                                    }
													<a href="#">{!this.isActiveVersionLocked() ? <i className="fa fa-unlock"></i> : <i className="fa fa-lock"></i> }</a>
												</div> : null
                                        }

                                        {this.state.versions.length > 0 ? table(1) : null}
									</div>
								</div>
								<div className="aec-full-message-w">
									<div className="aec-full-message">
										<div className="message-head">
											<div className="user-w with-status status-green">
												<div className="user-name">
													<h6 className="user-title">
                                                        {t('template:attestation')}
													</h6>
												</div>
											</div>
										</div>
                                        {
                                            this.state.versions.length > 0 ?
												<div className="add_lock">
													<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()} onClick={(e)=>{this.addParagraph(e, 2)}}><i className="os-icon os-icon-ui-22"></i><span>{t('template:addVersionButtonLabel')}</span></button>
                                                    {   this.state.selectedLanguage == Languages.fr ?
														<button className="btn btn-sm btn-primary" disabled={this.isActiveVersionLocked()}
																onClick={(e)=>{this.copyFrToEn(e, confirmModalOption, 2)}}>
															<i className="os-icon os-icon-ui-22"></i><span>{t('template:copyFrToEn')}</span>
														</button> : ''
                                                    }
													<a href="#">{!this.isActiveVersionLocked() ? <i className="fa fa-unlock"></i> : <i className="fa fa-lock"></i> }</a>
												</div> : null
                                        }
                                        {this.state.versions.length > 0 ? table(2) : null}
									</div>
								</div>

							</div>
						</div>
					</div>
				)
		}
		const Manifestation = ()=>{
			return(
				<div className="app-email-w forse-show-content">
					<div className="app-email-i">
						<div className="ae-side-menu">
							<ul className="ae-main-menu">
								{companiesDOM}
							</ul>
						</div>
						<div className="ae-side-menu">
							<div className="list_create_btn">
								{/*<a href="" data-target="#newVersionModal" data-toggle="modal"><i className="os-icon os-icon-common-03"></i><span>Create new version</span></a>*/}
								<a href="#" className="btn btn-sm btn-outline-primary" data-target="#newVersionModal" data-toggle="modal"><i className="os-icon os-icon-ui-22"></i><span>{t('template:addVersionButtonLabel')}</span></a>
								{
									!this.isActiveVersionLocked() ?
										<a href="#" className="btn btn-sm btn-outline-info" onClick={this.openEditModal.bind(this)}>
											<i className="os-icon os-icon-edit-1"></i><span>{t('template:editVersionButtonLabel')}</span>
										</a> :''
                                }
							</div>
							<ul className="ae-main-menu">
								{versionsDOM}
							</ul>
							
						</div>
						<div className="ae-content-w">
							<div className="ae-content">
								{pdfListDOM()}
							</div>
						</div>
					</div>
				</div>
		)}
		return (
			<div className="content-w template">
				<div className="content-i">
					<div className="content-box">
						<div className="os-tabs-w">
							
							<ul className="nav nav-tabs nav-fill custom_tab_ctrl">
								<li className="nav-item">
									<a aria-expanded="true" className="nav-link active" data-toggle="tab" href="#manifestation">Manifestation</a>
								</li>
								<li className="nav-item">
									<a aria-expanded="false" className="nav-link" data-toggle="tab" href="#media">Media</a>
								</li>
								<li className="nav-item">
									<a aria-expanded="false" className="nav-link" data-toggle="tab" href="#rc_event">RC event</a>
								</li>
								<li className="nav-item">
									<a aria-expanded="false" className="nav-link" data-toggle="tab" href="#salon">Salon</a>
								</li>
								<li className="nav-item">
									<a aria-expanded="false" className="nav-link" data-toggle="tab" href="#police">Police cadre Salon</a>
								</li>
								<li className="nav-item">
									<a aria-expanded="false" className="nav-link" data-toggle="tab" href="#tous">Tous risques Materiels</a>
								</li>
							</ul>
							<div className="tab-content">
								<div className="tab-pane active" id="manifestation" role="tabpanel"><Manifestation/></div>
								<div className="tab-pane" id="media" role="tabpanel">Media</div>
								<div className="tab-pane" id="rc_event" role="tabpanel">RC event</div>
								<div className="tab-pane" id="salon" role="tabpanel">Salon</div>
								<div className="tab-pane" id="police" role="tabpanel">Police cadre Salon</div>
								<div className="tab-pane" id="tous" role="tabpanel">Tous risques Materiels</div>
							</div>
						</div>
					</div>
				</div>
						<div aria-hidden="true" aria-labelledby="versionModalLabel" className="modal fade" id="newVersionModal" role="dialog">
							<div className="modal-dialog" role="document">
								<div className="modal-content">
									<div className="modal-header">
										<h5 className="modal-title" id="versionModalLabel">
											{t('template:createVersion')}
										</h5>
										<button aria-label="Close" className="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
									</div>
									<div className="modal-body">
										<form>
											<div className="row">
												<div className="col-sm-12">
													<div className="form-group">
														<label>{t('template:versionLabel')}</label>
														<input className="form-control" placeholder={t('template:versionName')} type="text" value={this.state.newVersionLabel} onChange={ (e)=>{this.setState({newVersionLabel: e.target.value});} }/>
													</div>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-12">
													<div className="form-group">
														<label>{t('template:for')}</label>
														<select className="form-control"  onChange={(e)=>{this.changeInsuranceForOptions(e.target.value);}} value={this.state.defaultInsurerOptionVal} >
															{insurance_options()}
														</select>
													</div>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-12">
													<div className="form-group">
														<label> {t('template:from')}</label>
														<select className="form-control" onChange={(e)=>{this.setState({newVersionFrom: e.target.value});}}>
															{version_options()}
														</select>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div className="modal-footer">
										<button className="btn btn-secondary" data-dismiss="modal" type="button"> {t('common:cancel')}</button>
										<button className="btn btn-primary" type="button" onClick={this.insertNewVersion.bind(this)} > {t('common:save')}</button>
									</div>
								</div>
							</div>
						</div>
						<div aria-hidden="true" aria-labelledby="editversionModalLabel" className="modal fade" id="editVersionModal" role="dialog">
							<div className="modal-dialog" role="document">
								<div className="modal-content">
									<div className="modal-header">
										<h5 className="modal-title" id="editversionModalLabel">
											{t('template:editVersion')}
										</h5>
										<button aria-label="Close" className="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
									</div>
									<div className="modal-body">
										<form>
											<div className="row">
												<div className="col-sm-12">
													<div className="form-group">
														<label>{t('template:versionLabel')}</label>
														<input className="form-control" value={this.state.editVersionLabel} placeholder={t('template:versionName')} type="text" onChange={ (e)=>{this.setState({editVersionLabel: e.target.value});} }/>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div className="modal-footer">
										<button className="btn btn-secondary" data-dismiss="modal" type="button"> {t('common:cancel')}</button>
										<button className="btn btn-primary" type="button" onClick={this.updateVersion.bind(this)} > {t('common:save')}</button>
										<button className="btn btn-secondary" type="button" onClick={this.activateVersion.bind(this)} > {t('common:activate')}</button>
										<button className="btn btn-danger" type="button" onClick={this.deleteVersion.bind(this)} > {t('common:delete')}</button>
									</div>
                                    {this.state.confirmObject ?
										<div className="modal-footer">
											<span style={{float: 'left', display: 'inline-block', color: 'red'}}>{t(this.state.confirmObject.text)}</span>
											<button className="btn btn-primary" onClick={this.state.confirmObject.callback}>{t('common:confirm')}</button>
											<button className="btn btn-secondary" onClick={this.resetConfirm.bind(this)}>{t('common:cancel')}</button>
										</div> : ''
                                    }
								</div>
							</div>
						</div>
			</div>
		);
	}
}
