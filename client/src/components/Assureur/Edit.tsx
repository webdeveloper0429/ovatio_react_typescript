import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {browserHistory} from 'react-router';
import {Assureur} from './../../models/AssureurModel'
import AddUser from "../Global/addUser/AddUser";
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomCountrySelect from './../Global/input/CustomCountrySelect';
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import CustomInputRate from "../Global/input/CustomInputRate";
import CustomTitle from "../Global/title/CustomTitle";
import {UserService} from '../../services/user';
import {AssureurService} from '../../services/assureur';
import {toast} from 'react-toastify';
import CustomInputCity from "../Global/input/CustomInputCity";
import {AvatarView} from "../Global/avatarView/AvatarView";

@translate(['assureur', 'form', 'countries'], {wait: true})
export class AssureurEdit extends React.Component<any, any> {

    public state: any;
    public countries: any;

    constructor(props: any) {
        super(props);
        this.state = {
            canSubmit: false,
            assureur: new Assureur(),
        };
        this.setAssureur = this.setAssureur.bind(this);
        this.checkForm = this.checkForm.bind(this);
        this.getEditDataIfEditMode = this.getEditDataIfEditMode.bind(this);
        this.save = this.save.bind(this);
        this.getEditDataIfEditMode();

        UserService.getMe().then(
            res => {
                this.setState({
                    isAdminRole: UserService.isAdmin(res.data)
                })
            }
        );

    }

    getEditDataIfEditMode() {
        if (this.props.params.id) {
            AssureurService.getAssurreurById(this.props.params.id).then(resp => {
                let row = resp.data;
                this.state.assureur.hydrate(row);
                this.setState({
                    assureur: this.state.assureur
                })
            })
        }
    }

    save(event: any) {
        event.preventDefault();
        if (this.state.canSubmit) {
            let promise: any;
            if (this.props.params.id) {
                promise = AssureurService.update(this.props.params.id, this.state.assureur).then(
                    (resp)=> {
                        browserHistory.push('/assureur')
                    }
                );
            }
            else {
                promise = AssureurService.insert(this.state.assureur).then(
                    (resp)=> {
                        browserHistory.push('/assureur')
                    }
                );
            }
            promise.then(() => toast.success(this.props.t('saveSuccess'))).catch(() => toast.error(this.props.t('error')))
        }
    }

    setAssureur(attribut, value) {
        this.state.assureur[attribut] = value;
        this.setState({assureur: this.state.assureur});
    }

    maxLength(value, limit, callback) {
        if (callback)
            callback(value.substring(0, limit))

    }

    checkForm() {
        this.state.canSubmit = this.state.assureur.validateObject();
    }

    handleChangeAvatarData = (avatarData)=>{
        this.state.assureur['avatarId'] = avatarData.filename;
        this.setState({ assureur: this.state.assureur });
    }

    render() {
        const {t} : any = this.props;
        this.checkForm();
        return (
            <div className="content-w">
                <div className="content-i">
                    <div className="content-box">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="element-wrapper">
                                    <h6 className="element-header">
                                        {
                                            (this.props.params.id ? t('assureur:editAssureurPageTitle')
                                                : t('assureur:createAssureurPageTitle'))
                                        }
                                    </h6>
                                    <div className="element-box">
                                        <form id="formValidate">
                                            <AvatarView
                                                handleUpdate={this.handleChangeAvatarData}
                                                avatarData={{
                                                    filename: this.state.assureur.avatarId,
                                                    displayName: this.state.assureur.businessName
                                                }}
                                            ></AvatarView>
                                            <CustomTitle label={t('form:generalInformation')}>
                                            </CustomTitle>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <CustomInputText
                                                        name="businessName"
                                                        label={t('form:businessName')}
                                                        validationFn={this.state.assureur.validationFn}
                                                        onChangeFn={(newValue) => this.setAssureur("businessName", newValue)}
                                                        model={this.state.assureur.businessName}
                                                        errorLabel={t('form:businessNameError')}
                                                    ></CustomInputText>
                                                </div>
                                                <div className="col-sm-4">
                                                    <CustomInputSelect
                                                        name="status"
                                                        label={t('form:status')}
                                                        disabled={!this.state.isAdminRole}
                                                        onChangeFn={(newValue) => this.setAssureur("status", newValue)}
                                                        model={this.state.assureur.status}
                                                        optionList={[
																{
																	value: true,
																	label: t('form:enabled')
																},
																{
																	value: false,
																	label: t('form:disable')
																}
															]}
                                                    ></CustomInputSelect>
                                                </div>
                                                <div className="col-sm-4">
                                                    <CustomInputRate
                                                        name="rate"
                                                        label={t('form:rate')}
                                                        onChangeFn={(newValue) => this.setAssureur("rate", newValue)}
                                                        model={this.state.assureur.rate}
                                                    ></CustomInputRate>
                                                </div>
                                                <div className="col-sm-4">
                                                    <CustomInputText
                                                        name="siret"
                                                        label={t('form:siret')}
                                                        validationFn={this.state.assureur.validationFn}
                                                        onChangeFn={(newValue) => this.setAssureur("siret", newValue)}
                                                        model={this.state.assureur.siret}
                                                        errorLabel={t('form:siretError')}
                                                    ></CustomInputText>
                                                </div>
                                                <div className="col-sm-12">
                                                    <fieldset className="form-group">
                                                        <div className="form-group">
                                                            <label>{t('form:tos')}</label>
                                                            <textarea className="form-control"
                                                                      value={this.state.assureur.legalNotice}
                                                                      onChange={(newValue) => this.maxLength(newValue.target.value, 400, this.setAssureur.bind(this, "legalNotice"))}></textarea>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <CustomTitle label={t('form:address')}></CustomTitle>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <CustomInputText
                                                        name="adress"
                                                        label={t('form:address')}
                                                        onChangeFn={(newValue) => this.setAssureur("address", newValue)}
                                                        model={this.state.assureur.address}
                                                    ></CustomInputText>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomInputZipCode
                                                        name="zipCode"
                                                        label={t('form:zipCode')}
                                                        onChangeFn={(newValue) => {this.setAssureur("zipCode", newValue);}}
                                                        model={this.state.assureur.zipCode}
                                                        country={this.state.assureur.countryId}
                                                        populateCallback={zipcodeObject => this.setAssureur("city", zipcodeObject.ville)}
                                                        errorLabel={t('assureur:zipCodeError')}
                                                    ></CustomInputZipCode>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomInputCity
                                                        name="city"
                                                        label={t('form:city')}
                                                        onChangeFn={(newValue) => {this.setAssureur("city", newValue);}}
                                                        country={this.state.assureur.countryId}
                                                        populateCallback={zipcodeObject => this.setAssureur("zipCode", zipcodeObject.cp)}
                                                        model={this.state.assureur.city}
                                                        errorLabel={t('assureur:cityError')}
                                                    ></CustomInputCity>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomCountrySelect
                                                        name="country"
                                                        label={t('form:country')}
                                                        onChangeFn={(newValue) => this.setAssureur("countryId", newValue)}
                                                        model={this.state.assureur.countryId}
                                                    ></CustomCountrySelect>
                                                </div>
                                            </div>
                                            <CustomTitle label={t('form:addressAccounting')}></CustomTitle>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <CustomInputText
                                                        name="addressAccounting"
                                                        label={t('form:addressAccounting')}
                                                        onChangeFn={(newValue) => this.setAssureur("addressAccounting", newValue)}
                                                        model={this.state.assureur.addressAccounting}
                                                    ></CustomInputText>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomInputZipCode
                                                        name="zipCodeAccounting"
                                                        label={t('form:zipCodeAccounting')}
                                                        onChangeFn={(newValue) => {this.setAssureur("zipCodeAccounting", newValue);}}
                                                        model={this.state.assureur.zipCodeAccounting}
                                                        populateCallback={zipcodeObject => this.setAssureur("cityAccounting", zipcodeObject.ville)}
                                                        country={this.state.assureur.countryAccountingId}
                                                        errorLabel={t('assureur:zipCodeError')}
                                                    ></CustomInputZipCode>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomInputCity
                                                        name="cityAccounting"
                                                        label={t('form:cityAccounting')}
                                                        onChangeFn={(newValue) => {this.setAssureur("cityAccounting", newValue);}}
                                                        model={this.state.assureur.cityAccounting}
                                                        populateCallback={zipcodeObject => this.setAssureur("zipCodeAccounting", zipcodeObject.cp)}
                                                        errorLabel={t('assureur:cityError')}
                                                        country={this.state.assureur.countryAccountingId}
                                                    ></CustomInputCity>
                                                </div>
                                                <div className="col-sm-6">
                                                    <CustomCountrySelect
                                                        name="countryAccounting"
                                                        label={t('form:countryAccounting')}
                                                        onChangeFn={(newValue) => this.setAssureur("countryAccountingId", newValue)}
                                                        model={this.state.assureur.countryAccountingId}
                                                    ></CustomCountrySelect>
                                                </div>
                                            </div>
                                            <CustomTitle label={t('form:contacts')}></CustomTitle>
                                            <AddUser
                                                users={this.state.assureur.contacts}
                                            ></AddUser>
                                            <div className="form-buttons-w">
                                                <button
                                                    className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')}
                                                    type="button" onClick={this.save}>{t('common : save')}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
