import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import i18n from '../../i18n';
import {Link, browserHistory} from 'react-router';
import {RolesService} from '../../services/role';
import {UserService, SkipRole} from '../../services/user';
import {User} from "../../models/UserModel";
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomInputNumber from './../Global/input/CustomInputNumber';
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import { toast } from 'react-toastify';

export interface UserEditProps {
	t:any;
	params: any;
}

@translate(['user', 'form', 'toaster'], {wait: true})
export class UserEdit extends React.Component<UserEditProps, any> {
	public state: any;

	constructor(props: UserEditProps) {
		super(props);
		this.setUser = this.setUser.bind(this);
		this.checkForm = this.checkForm.bind(this);
		this.save = this.save.bind(this);
		this.state = {
			isAdmin : false,
			canSubmit: false,
			user: new User(),
			helperData: {
				roleIdListArr: []
			}
		};

		UserService.getMe().then(
			res => {
				this.setState({
					isAdmin: UserService.isAdmin(res.data)
				})
			}
		);

		RolesService.find().then(
			resp => {
				resp.data.map((row)=>{
					if(!SkipRole.includes(row.label)){
						this.state.helperData.roleIdListArr.push({
							'value': row.id,
							'label': row.nameFr
						})
						this.setState({ helperData: this.state.helperData });
					}
				});
			}
		);
	}
	componentDidMount(){
		this.getEditDataIfEditMode();		
	}

	checkForm() {
		this.state.canSubmit = this.state.user.validateObject();
	}

	setUser(attr, val) {
		this.state.user[attr] = val;
		this.setState({ user: this.state.user });
	}

	save(event: any){
		event.preventDefault();
		if(this.state.canSubmit){
			let promise:any;
			if(this.props.params.id){
				promise = UserService.update(this.props.params.id, this.state.user).then(
					(resp)=>{
						browserHistory.push('/user')
					}
				);
			}
			else{
				promise = UserService.insert(this.state.user).then(
					(resp)=>{
						browserHistory.push('/user')
					}
				);
			}
			promise.then(() => toast.success(this.props.t('toaster:saveSuccess'))).catch(() => toast.props.error(this.props.t('toaster:error')));
		}			
	}

	getEditDataIfEditMode() {
		if (this.props.params.id) {
			UserService.getOne(this.props.params.id).then(resp => {
				let row = resp.data;
				this.state.user.hydrate(row);
				this.setState({ user: this.state.user });
			})
		}
	}

	render() {
		const {t} : any = this.props;

		this.checkForm()
		return (
			<div className="content-w">
				<div className="content-i">
					<div className="content-box">
						<div className="row">
							<div className="col-sm-12">
								<div className="element-wrapper">
									<h6 className="element-header">
										{
											(this.props.params.id ? t('user:editUserPageTitle')
												: t('user:createUserPageTitle'))
										}
									</h6>
									<div className="element-box">
										<form id="formValidate">
											<div className="row">
												<div className="col-sm-10">
													<div className="row">
														<div className="col-sm-6">
															<CustomInputSelect
																name="genre"
																label={t('form:genre')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("genre", newValue)}
																model={this.state.user.genre}
																errorLabel={t('genreError')}
																optionList={[
																		{
																			value: 'mr',
																			label: t('mr')
																		},{
																			value: 'mrs',
																			label: t('mrs')
																		}
																	]}
															></CustomInputSelect>
														</div>
														<div className="col-sm-6">
															<CustomInputSelect
																name="roleId"
																label={t('form:roleId')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("roleId", newValue)}
																model={this.state.user.roleId}
																errorLabel={t('form:roleIdError')}
																disabled={!this.state.isAdmin}
																optionList={this.state.helperData.roleIdListArr}
															></CustomInputSelect>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="lastName"
																label={t('form:lastName')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("lastName", newValue)}
																model={this.state.user.lastName}
																errorLabel={t('form:lastNameError')}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="firstName"
																label={t('form:firstName')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("firstName", newValue)}
																model={this.state.user.firstName}
																errorLabel={t('form:firstNameError')}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="address"
																label={t('form:address')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("address", newValue)}
																model={this.state.user.address}
																errorLabel={t('form:addressError')}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="address2"
																label={t('form:address2')}
																onChangeFn={(newValue) => this.setUser("address2", newValue)}
																model={this.state.user.address2}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputZipCode
																name="zipCode"
																label={t('form:zipCode')}
																onChangeFn={(newValue) => this.setUser("zipCode", newValue)}
																model={this.state.user.zipCode}
																errorLabel={t('form:zipCodeError')}
															></CustomInputZipCode>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="city"
																label={t('form:city')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("city", newValue)}
																model={this.state.user.city}
																errorLabel={t('form:cityError')}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="email"
																label={t('form:email')}
																validationFn={this.state.user.validationFn}
																onChangeFn={(newValue) => this.setUser("email", newValue)}
																model={this.state.user.email}
																errorLabel={t('form:emailError')}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="phone"
																label={t('form:phone')}
																validationFn={this.state.user.validationFn.bind(this.state.user)}
																onChangeFn={(newValue) => this.setUser("phone", newValue)}
																model={this.state.user.phone}
																errorLabel={t('form:phoneError')}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="mobilePhone"
																label={t('form:mobilePhone')}
																validationFn={this.state.user.validationFn.bind(this.state.user)}
																onChangeFn={(newValue) => this.setUser("mobilePhone", newValue)}
																model={this.state.user.mobilePhone}
																errorLabel={t('form:mobilePhoneError')}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputSelect
																name="isActive"
																label={t('form:isActive')}
																onChangeFn={(newValue) => this.setUser("isActive", newValue)}
																model={this.state.user.isActive}
																optionList={[
																		{
																			value: 0,
																			label: t('form:inactive')
																		},{
																			value: 1,
																			label: t('form:active')
																		}
																	]}
															></CustomInputSelect>
														</div>
														<div className="col-sm-6">
															{
																this.props.params.id?
																<div></div>
																:
																<CustomInputText
																	name="password"
																	label={t('form:password')}
																	onChangeFn={(newValue) => this.setUser("password", newValue)}
																	model={this.state.user.password}
																></CustomInputText>
															}
														</div>
													</div>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-6">
													<button
														className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')}
														type="button" onClick={this.save}>{t('common:save')}
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
