import * as React from "react";
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import { translate, Interpolate, Trans } from 'react-i18next';
import {ManifestationService} from "../../services/manifestation";
import {VersionService} from "../../services/version";
import {ParagraphService} from "../../services/paragraph";
import {ContractParagraphService} from "../../services/contractParagraph";
import i18n from '../../i18n';
import { SHOW_ALL, TYPE_PROJECT, TYPE_CONTRAT, ONGOING, CANCELED, TRANSFORMED } from '../../consts'
import { Link } from 'react-router'
import CustomInputText from '../Global/input/CustomInputText';
import DateComponent from '../Schedule/components/DateComponent'
import CustomInputSelect from './../Global/input/CustomInputSelect';
import DeleteConfirmModal from '../Global/modal/DeleteConfirmModal';
import $ from 'jquery';
declare var $: any;
import { toast } from 'react-toastify';
import {Languages} from "../../enums/languages";

export interface ManifestationListState {
    searchValue: string,
    rowCount: number,
    offset: number,
    sortParams: any,
    pageCount: number,
    manifestationList: any,
    selectedManifestationIndex: number,
    yearFilter: string,
    status: number,
    versionLabel: any,
    inceptionDate: any,
    canSubmit: boolean,
    currentTab: number,
    deleteId: number,
    helperData: any,
    contractNumber: string,
    errorText: string,
}

@translate(['manifestation','common', 'form', 'toaster', 'modal'], { wait: true })
export class ManifestationList extends React.Component<any, ManifestationListState> {
    public columns: Array<string>;
    public state: any;
	public DeleteConfirmModal: any;

    constructor(props: any) {
        super(props);
        axios.defaults.baseURL = '/api/manifestations';
        this.columns = ['type',
            'reference',
            'eventName',
            'premiumIncludingTax',
            'BNS',
            'accountManagerId',
            ];
        this.state = {
            searchValue: '',
            rowCount: 10,
            offset: 0,
            sortParams: {
                column: 'eventName',
                orderASC: true,
            },
            pageCount: 0,
            yearFilter: '',
            status: 0,
            manifestationList: [],
            selectedManifestationIndex: 0,
            versionLabel: '',
            inceptionDate: this.makeFormObject('', false),
            canSubmit: false,
            currentTab: TYPE_PROJECT,
            deleteId: 0,
            contractNumber: '',
            errorText: null
        };
        this.search = this.search.bind(this);
        this.handleRowCount = this.handleRowCount.bind(this);
        this.setSortParams = this.setSortParams.bind(this);
        this.deleteManifestation = this.deleteManifestation.bind(this);
        this.upgradeToContract = this.upgradeToContract.bind(this);
        this.loadDataFromServer = this.loadDataFromServer.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.upgradeConfirm = this.upgradeConfirm.bind(this);
        this.filterYear = this.filterYear.bind(this);
        this.handlestatus = this.handlestatus.bind(this);
        this.duplicateProject = this.duplicateProject.bind(this);
        this.handleInceptionDate = this.handleInceptionDate.bind(this);
        this.checkForm = this.checkForm.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.currentTab != nextProps.currentTab) {
            this.setState({
                currentTab: nextProps.currentTab,
                status: 0,
            }, () => {
                this.loadDataFromServer()
            })
        }
    }

    componentDidMount() {
        this.setState({
            currentTab: this.props.currentTab
        }, () => {
            this.loadDataFromServer();
        })
    }

    makeFormObject(value: any, valid: boolean) {
        return {
            value: value === null ? '' : value,
            valid: valid
        }
    }

    checkForm() {
        let valid = this.state.versionLabel && this.state.inceptionDate.valid
        this.state.canSubmit = valid
    }

    duplicateProject(id: number, event: any) {
        event.preventDefault()
        ManifestationService.duplicate(id, {status: ONGOING})
            .then(() => {
                this.loadDataFromServer()
                toast.success(this.props.t('toaster:saveSuccess'))
            }).catch(() => toast.error(this.props.t('toaster:error')))
    }

	deleteManifestation(confirmState: boolean) {
		if(confirmState){
			axios.delete('/' + this.state.deleteId).then(() => {
				this.loadDataFromServer();
				toast.success(this.props.t('toaster:deleteSuccess'))
			}).catch(() => toast.error(this.props.t('toaster:error')))
		}
	}

    upgradeToContract(){
        let contractId = null;
        if (!this.state.canSubmit)
            return
        let manifestation = this.state.manifestationList[this.state.selectedManifestationIndex];
        let lang:Languages
        //todo: in a static service
        switch(manifestation.policyLanguage) {
            case 'english':
                lang = Languages.en;
            case 'french':
            default:
                lang = Languages.fr;
        }
        let manifestation_id = manifestation.id;
        let mainInsurer = manifestation.mainInsurer;
        let mainVersionId = mainInsurer && mainInsurer.versions.length ? mainInsurer.versions[0].id : null;
        //todo: find mainVersionId in the server
        ManifestationService.clone(manifestation_id, {
            sortPDF: 1,
            status: ONGOING,
            contractVersionId: mainVersionId,
            contractNumber: this.state.contractNumber,
            lang: lang,
        }).then((insertedDao: any) => {
            contractId = insertedDao.data.insertedId
            return this.loadDataFromServer().then(() => $('#upgradeConfirmModal').modal('hide'));
        }).catch(err => {
            $('#upgradeConfirmModal').modal('hide');
            if(err.response.data.code === 1) {
                this.setState({
                    errorText: 'manifestation:noPara'
                });
            } else {
                this.setState({
                    errorText: 'common:errorText'
                });
            }
            $('#errorConfirmModal').modal('show');
        })

        ParagraphService.find({'version_id': mainVersionId}).then(
            paragraphs => {
                paragraphs.data.map((paragraph, index)=>{
                    if(paragraph.group == 1){        //only contract paragraphs
                        let new_contract_paragraphData={
                            manifestation_id: contractId,
                            level: paragraph.level,
                            subLevel: paragraph.subLevel,
                            title: paragraph.title,
                            condition: paragraph.condition,
                            editor: paragraph.editor
                        }
                        ContractParagraphService.insert(new_contract_paragraphData).then(
                            paragraph => {}
                        );
                    }
                });
            }
        )
    }

    filterYear(event: any) {
        this.setState({ yearFilter: event.target.value }, () => {
            this.loadDataFromServer()
        })
    }

    search(event: any) {
        event.preventDefault();
        this.setState({searchValue: event.target.value}, () => {
            this.loadDataFromServer();
        });
    }

    handlestatus(event: any) {
        event.preventDefault()
        this.setState({ status: event.target.value }, () => {
            this.loadDataFromServer();
        })
    }

    handleRowCount(event: any) {
        this.setState({rowCount: event.target.value}, () => {
            this.loadDataFromServer();
        });
    }

    handlePageChange(event: any) {
        let rowCount = this.state.rowCount
        this.setState({
            offset: Math.ceil(event.selected * rowCount),
        }, () => {
            this.loadDataFromServer();
        })
    }

    setSortParams(sortColumn: string, event: any) {
        if (['type', 'eventName', 'BNS'].indexOf(sortColumn) > -1) {
            let orderASC = sortColumn === this.state.sortParams.column ? !this.state.sortParams.orderASC : true
            this.setState({
                sortParams: {
                    column: sortColumn,
                    orderASC: orderASC
                }
            }, () => {
                this.loadDataFromServer();
            })
        }
    }

    loadDataFromServer() {
        let params: any = {
            sort: this.state.sortParams.column,
            order: this.state.sortParams.orderASC ? 'ASC' : 'DESC',
            search: this.state.searchValue,
            offset: this.state.offset,
            limit: this.state.rowCount,
            year: this.state.yearFilter,
            type: this.state.currentTab,
            status: this.state.status,
        };
        return axios.get('/count', {params: {}}).then((resp) => {
            let totalRowCount = resp.data.count
            return axios.get('', {params: params}).then((resp) => {
                let rowCount = this.state.rowCount
                this.setState({
                    pageCount: Math.ceil(totalRowCount / rowCount),
                    manifestationList: resp.data,
                })
            })
        });
    }

    handleInceptionDate(date) {
        console.log('handleInceptionDate', date)
        this.setState({
            inceptionDate: this.makeFormObject(date.value, true)
        })
    }

    upgradeConfirm(e, index) {
        e.preventDefault();
        if(!this.state.manifestationList[index].mainInsurer || !this.state.manifestationList[index].mainInsurer.versions.length) {
            this.setState({
                errorText: 'manifestation:noVersion',
            });
            $('#errorConfirmModal').modal('show');
        } else {
            this.setState({
                selectedManifestationIndex: index,
                inceptionDate: this.makeFormObject(this.state.manifestationList[index]['inceptionDate'], true),
                versionLabel: this.state.manifestationList[index].mainInsurer.versions[0].label,
                errorText: null
            });

            $('#upgradeConfirmModal').modal('show');
        }
	}
	
	confirmDelete(id: number, e: any){
		e.preventDefault();
		this.setState({ deleteId: id });
		this.DeleteConfirmModal.openModal()
    }
    
    handleContractNumber(newVal:string){
        this.setState({ contractNumber: newVal });
    }

    render() {
        let self = this;
        const { t } : any = this.props;
        this.checkForm();

        let insuranceCompanyList = this.state.manifestationList.map((manifestation, index) => {
            let duplicateProject = self.duplicateProject.bind(this, manifestation.id);
            return (
                <tr key={manifestation.id} className={manifestation.status == CANCELED ? 'canceled-row' : ''}>
                    <td>{manifestation.type}</td>
                    <td>{manifestation.sortPDF != TYPE_CONTRAT? manifestation.projectNumber : manifestation.contractNumber}</td>
                    <td>{manifestation.eventName}</td>
                    <td>{manifestation.totalDeclaredBudget}</td>
                    <td>{manifestation.BNS}</td>
                    <td>{manifestation.accountManager.firstName+' '+manifestation.accountManager.lastName}</td>
                    <td className="row-actions">
                        <Link title={t('manifestation:editManifestationHover')} to={"/manifestation/edit/" + manifestation.id}><i className="os-icon os-icon-pencil-2"></i></Link>
                        {
                            manifestation.deletedAt ?
                                <a title={t('manifestation:deleteManifestationHover')}onClick={ (e)=>this.confirmDelete(manifestation.id, e) } href=""><i className="os-icon os-icon-others-43"></i></a>
                                :
                                <a title={t('manifestation:enableManifestationHover')} onClick={ (e)=>this.confirmDelete(manifestation.id, e) } href=""><i className="os-icon os-icon-ui-15"></i></a>
                        }
                        {
                            manifestation.sortPDF == TYPE_PROJECT ?
                            <a  title={t('manifestation:duplicateManifestationHover')} onClick={duplicateProject} href=""><i className="os-icon os-icon-hierarchy-structure-2"></i></a>
                            :
                            <span></span>
                        }
                        {
                            manifestation.sortPDF == TYPE_PROJECT ?
                                <Link  title={t('manifestation:previewManifestationHover')} to={"/preview/" + manifestation.id}><i className="os-icon os-icon-newspaper"></i></Link>
                                :
                                <Link to={"/preview_contract/" + manifestation.id}><i className="os-icon os-icon-newspaper"></i></Link>
                        }
                        {
                            manifestation.sortPDF == TYPE_PROJECT ?
                                <a   title={t('manifestation:updateManifestationHover')} onClick={(e)=>{this.upgradeConfirm(e, index)}} href=""><i className="os-icon os-icon-mail-19"></i></a>
                                :
                                <span></span>
                        }
                    </td>
                </tr>
            )
        })

        // the first elem is the database name
        // the second elem is the title to display
        let insuranceCompanyTableHeader = this.columns.map((item, i) => {
            let boundItemClick = self.setSortParams.bind(self, item);
            return (
                <th key={i} onClick={boundItemClick}>{t('manifestation:' + item)} {item === self.state.sortParams.column ? (self.state.sortParams.orderASC ? <i className="os-icon os-icon-arrow-up2"></i> : <i className="os-icon os-icon-arrow-down"></i>) : ''}</th>
            )
        })
        function numberRange(start, end) {
            return new Array(end - start).fill(0).map((d, i) => i + start + 1);
        }
        let currentYear = new Date().getFullYear();
        let last10YearsArray = numberRange(currentYear - 10, currentYear);

        let last10Years = last10YearsArray.map((year, i)=> {
            return (
                <option key={i} value={year}>{year}</option>
            )
        })
        return (
            <div style={{marginTop: '20px'}}>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="element-wrapper">
                                    <h6 className="element-header">
                                        {t('manifestation:manifestationListTitle')}
                                    </h6>
                                    <div className="element-box-tp">
                                        <div className="controls-above-table">
                                            <div className="row">
                                                <div className="col-md-12 col-lg-2">
                                                    <Link className="btn btn-sm btn-secondary" to="/manifestation/create">{t('addManifestation')}</Link>
                                                </div>
                                                <div className="col-md-12 col-lg-10">
                                                    <form className="form-inline justify-content-sm-end">
                                                        <input className="form-control form-control-sm rounded bright" placeholder={t('manifestation:searchAManifestation')} type="text" value={this.state.searchValue} onChange={this.search} />
                                                        <select className="form-control form-control-sm rounded bright" onChange={this.filterYear}>
                                                            <option value="">{t('manifestation:allYears')}</option>
                                                            {last10Years}
                                                        </select>
                                                        <select className="form-control form-control-sm rounded bright" onChange={this.handlestatus}>
                                                            <option value={SHOW_ALL}>{t('manifestation:allProject')}</option>
                                                            <option value={ONGOING}>{t('manifestation:ongoingProjects')}</option>
                                                            <option value={CANCELED}>{t('manifestation:canceledProjects')}</option>
                                                            {
                                                                this.props.currentTab === TYPE_PROJECT ?
                                                                    <option value={TRANSFORMED}>{t('manifestation:transformedProjects')}</option>
                                                                    : ''
                                                            }
                                                        </select>
                                                        <select className="form-control form-control-sm rounded bright" onChange={this.handleRowCount}>
                                                            <option value="10">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="table-responsive">
                                            <table className="table table-bordered table-lg table-v2 table-striped">
                                                <thead>
                                                <tr>
                                                    {insuranceCompanyTableHeader}
                                                    <th>{t('common:action')}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    {insuranceCompanyList}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="controls-below-table">
                                            <div className="table-records-info">
                                                {t('common:showingRecordLabel')}{this.state.offset} - {this.state.offset + this.state.rowCount}
                                            </div>
                                            <div className="table-records-pages" id="react-paginate">
                                                <ReactPaginate
                                                    previousLabel={t('common:previousLabel')}
                                                    nextLabel={t('common:nextLabel')}
                                                    breakLabel={"..."}
                                                    activeClassName={"current"}
                                                    breakClassName={"break-me"}
                                                    pageCount={this.state.pageCount}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={5}
                                                    onPageChange={this.handlePageChange}
                                                    containerClassName={"table-records-pages"}
                                                    subContainerClassName={"pages pagination"} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div aria-hidden="true" aria-labelledby="errorModalLabel" className="modal fade" id="errorConfirmModal" role="dialog">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="errorModalLabel">
                                            {t("common:error")}
                                        </h5>
                                        <button aria-label="Close" className="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                    </div>
                                    <div className="modal-body">
                                        <p>{t(this.state.errorText)}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div aria-hidden="true" aria-labelledby="upgradeModalLabel" className="modal fade" id="upgradeConfirmModal" role="dialog">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="upgradeModalLabel">
                                            {t("modal:areYouSure")}
                                        </h5>
                                        <button aria-label="Close" className="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                    </div>
                                    <div className="modal-body date-picker-bt3">
                                        <p>{t("manifestation:updateProject")}</p>
                                        <CustomInputSelect
                                            model={this.state.versionLabel}

                                            optionList={[{value: this.state.versionLabel, label: this.state.versionLabel}]}
                                            name="version"
                                            label={t('form:version')}
                                            required={true}
                                            disabled={true}
                                        ></CustomInputSelect>
                                        <DateComponent
                                            label={t("form:inceptionDate")}
                                            fieldKey='inceptionDate'
                                            handleChange={this.handleInceptionDate}
                                            value={this.state.inceptionDate.value}
                                            minDate={this.state.inceptionDate.value}
                                            required={true}
                                        />
                                        <CustomInputText
                                            name="contractNumber"
                                            label={t('manifestation:contractNumber')}
                                            onChangeFn={(newValue) => this.handleContractNumber(newValue)}
                                            model={this.state.contractNumber}
                                        ></CustomInputText>
                                    </div>
                                    <div className="modal-footer">
                                        <button className="btn btn-secondary" data-dismiss="modal" type="button"> {t("common:cancel")}</button>
                                        <button
                                            className={"btn btn-danger " + (this.state.canSubmit ? '' : 'disabled')}
                                            type="button"
                                            onClick={() => { this.upgradeToContract() }}
                                        > {t("manifestation:upgrade")}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
				<DeleteConfirmModal
					onRef={ref => (this.DeleteConfirmModal = ref)}
					onConfirm={(confirmState)=>{ this.deleteManifestation(confirmState) }}
				/>
            </div>
        );
    }
}
