import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {Link, browserHistory} from 'react-router';
import {BusinessProviderService} from "../../services/businessProvider";
import {AssureurService} from "../../services/assureur";
import {ManifestationService} from "../../services/manifestation";
import {ExpertService} from "../../services/expert";
import _cloneDeep from "lodash/cloneDeep";
import {UserService} from "../../services/user";
import ReactAutocomplete from "react-autocomplete";
import ScheduleContainer from "../Schedule/ScheduleContainer";
import {ClientService} from "../../services/client";
import {DevisesService} from "../../services/devise";
import {Manifestation} from "../../models/ManifestationModel";
import {Client} from "../../models/ClientModel";
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomCountrySelect from './../Global/input/CustomCountrySelect';
import CustomInputTextarea from './../Global/input/CustomInputTextarea';
import CustomInputNumber from './../Global/input/CustomInputNumber';
import CustomInputDate from './../Global/input/CustomInputDate';
import CustomInputCheckbox from './../Global/input/CustomInputCheckbox';
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import CustomInputRate from './../Global/input/CustomInputRate';
import CustomNumberFormat from './../Global/input/CustomNumberFormat';
import { SHOW_ALL, TYPE_PROJECT, TYPE_CONTRAT, ONGOING, CANCELED, TRANSFORMED } from '../../consts';
import { toast } from 'react-toastify';

export interface ManifestationEditProps {
	params: any;
	t:any;
}

@translate(['manifestation','form', 'countries', 'toaster'], {wait: true})
export class ManifestationEdit extends React.Component<ManifestationEditProps, any> {
	public state: any;
	public countries: any;

	constructor(props: ManifestationEditProps) {
		super(props);
		this.handleNumberPersonInsured = this.handleNumberPersonInsured.bind(this);
		this.handleUnavailabilityField = this.handleUnavailabilityField.bind(this);
		this.handleTechnicianUnavailabilityField = this.handleTechnicianUnavailabilityField.bind(this);
		this.handleNumberTechnicianInsured = this.handleNumberTechnicianInsured.bind(this);

		this.checkForm = this.checkForm.bind(this);
		this.getEditDataIfEditMode = this.getEditDataIfEditMode.bind(this);
		this.save = this.save.bind(this);

		this.onAutocompleteChange = this.onAutocompleteChange.bind(this);
		this.onAutocompleteSelect = this.onAutocompleteSelect.bind(this);
		this.addCoInsurer = this.addCoInsurer.bind(this);
		this.state = {
			canSubmit: false,
			manifestation: new Manifestation(),
			helperData: {
				businessProviderListArr: [],
				accountManagerListArr: [],
				assureurListArr: [],
				expertListArr: [],
				deviseList: [],
			},

			scheduleData: this.makeScheduleObject({}, true),
			personInsuredList: [],
			technicianList: [],
			coInsurerList: [],

			clientsAutoCompl: [],
			clientId: null,
			client: new Client()
		};
		Promise.all(
			[
			BusinessProviderService.find({}),
			UserService.findAccountManger(),
			AssureurService.find({}),
			ExpertService.find(),
			DevisesService.find()
			]
		).then(data => {
			data[0].data.map((row) => {
				this.state.helperData.businessProviderListArr.push({
					'value': row.id,
					'label': row.businessName
				})
			});
			data[1].data.map((row) => {
				this.state.helperData.accountManagerListArr.push({
					'value': row.id,
					'label': `${row.firstName} ${row.lastName}`
				})
			});
			data[2].data.map((row) => {
				this.state.helperData.assureurListArr.push({
					'value': row.id,
					'label': row.businessName
				})
			});
			data[3].data.map((row) => {
				this.state.helperData.expertListArr.push({
					'value': row.id,
					'label': row.businessName
				})
			});
			data[4].map((row) => {
				this.state.helperData.deviseList.push({
					'value': row.id,
					'label': row.name
				})
			});
			this.setState({ helperData: this.state.helperData });
		})
	}

	componentDidMount() {
		this.getEditDataIfEditMode();
	}

	budgetRules(_state) {
		if (_state.scheduleData && _state.scheduleData.value) {
			return ManifestationService.computeBudget(_state.manifestation, _state.scheduleData.value.budget);
		} else {
			return {}
		}

	}

	getEditDataIfEditMode() {
		if (this.props.params.id) {
			ManifestationService.getOne(this.props.params.id).then(resp => {
				let row = resp.data;
				this.state.manifestation.hydrate(row);
				this.setState({
					// manifestation: this.state.manifestation,
					personInsuredList: row.personUnavailabilityList,
					technicianList: row.technicianUnavailabilityList,
					coInsurerList: row.coInsurerList
				})
				this.setState({
					scheduleData: {
						value: {
							category: row.category,
							customCategory: row.customCategory,
							eventDate: {value: row.eventDate, formattedValue: null},
							eventEndDate: {value: row.eventEndDate, formattedValue: null},
							artist: row.artist,
							dateType: row.dateType,
							numberInsured: row.numberInsured,
							budget: row.budget,
							// dates: JSON.parse(row.manifestationDates),
							dates: row.manifestationDates.map(e => {
								return {
									...e,
									date: {
										value: e.date,
										formattedValue: (new Date(Date.parse(e.date))).toLocaleDateString()
									},
								};
							}),
						},
						valid: true,
					}
				});
				ClientService.getOneClient(row.clientId).then(
					resp=> {
						let row = resp.data[0];
						this.state.client.hydrate(row);
						this.setState({clientId: row.id});
					}
				);
			})
		}
	}

	makeScheduleObject(value: object, valid: boolean) {
		return {
			value: value === null ? {} : value,
			valid: valid
		}
	}

	checkForm() {
		this.state.canSubmit = this.state.manifestation.validateObject() &&
			this.state.client.validateObjectSimple() &&
			this.state.scheduleData.valid;
	}

	setManifestation(attr, val) {
		this.state.manifestation[attr] = val;
		this.setState({manifestation: this.state.manifestation});
	}

	setClient(attr, val) {
		this.state.client[attr] = val;
		this.setState({client: this.state.client});
	}

	handleScheduleUpdate = (data, valid) => {
		this.setState({scheduleData: {value: _cloneDeep(data), valid}});
	}

	handleNumberPersonInsured(newValue) {
		while (newValue != this.state.personInsuredList.length) {
			if (newValue > this.state.personInsuredList.length) {
				this.state.personInsuredList.push({
					band: '',
					insuredPeople: '',
					function: '',
					replaceable: '',
					age: '',
					medicalExamination: '',
				})
			} else if (newValue < this.state.personInsuredList.length) {
				this.state.personInsuredList.pop();
			}
		}
		this.setManifestation("numberPersonInsured", newValue);
	}

	handleNumberTechnicianInsured(newValue) {
		while (newValue != this.state.technicianList.length) {
			if (newValue > this.state.technicianList.length) {
				this.state.technicianList.push({
					firstName: '',
					lastName: '',
					age: '',
					medicalExamination: '',
				})
			} else if (newValue < this.state.technicianList.length) {
				this.state.technicianList.pop();
			}
		}
		this.setManifestation("numberTechnicianInsured", newValue);
	}

	handleUnavailabilityField(newValue: any, field: string, index: number) {

		this.state.personInsuredList[index][field] = newValue;
		this.setState({
			personInsuredList: this.state.personInsuredList
		})
	}

	handleTechnicianUnavailabilityField(newValue: any, field: string, index: number) {

		this.state.technicianList[index][field] = newValue;
		this.setState({
			technicianList: this.state.technicianList
		})
	}

	onAutocompleteChange(e) {
		this.setState({clientId: null});
		this.state.manifestation.clientId = null;
		this.state.client.businessName = e.target.value;
		this.setState({client: this.state.client});
		ClientService.getAutocompleteClients({search: e.target.value}).then(
			resp => {
				this.setState({clientsAutoCompl: resp.data});
			}
		);
	}

	onAutocompleteSelect(value, item) {
		this.state.client.hydrate(item);
		this.state.manifestation.clientId = item.id;
		this.setState({clientId: item.id});
	}

	save(event: any) {
		event.preventDefault();
		if (this.state.canSubmit) {
			if (!this.state.clientId) {
				ClientService.insert(this.state.client).then(
					resp=> {
						this.state.manifestation.clientId = resp.data.id;
						this.setState({clientId: resp.data.id}, function () {
							this.saveManifestation();
						});
					}
				)
			}
			else {
				this.saveManifestation();
			}
		}
	}

    handleAgeCheckboxChange(checked:boolean, index:number) {
		this.state.personInsuredList[index].overAgeLimit = checked;
		if(checked) {
            this.state.personInsuredList[index]["age"] = 0;
		}
        this.setState({
            personInsuredList: this.state.personInsuredList
        })
	}

	saveManifestation() {
		this.state.manifestation.setScheduleDate(this.state.scheduleData.value);
		this.state.manifestation.personUnavailabilityList = this.state.personInsuredList;
		this.state.manifestation.technicianUnavailabilityList = this.state.technicianList;
		this.state.manifestation.coInsurerList = this.state.coInsurerList;
        let promise:any;
		if (this.props.params.id) {
			promise = ManifestationService.update(this.props.params.id, this.state.manifestation).then(resp => {
				browserHistory.push('/manifestation')
			});
		}
		else {
			promise = ManifestationService.create(this.state.manifestation).then(resp => {
				browserHistory.push('/manifestation')
			});
		}
        promise.then(() => toast.success(this.props.t('toaster:saveSuccess'))).catch(() => toast.error(this.props.t('toaster:error')))
	}

	displayNumberCommission(number){
		return  isNaN(number) ? '' :Math.round(number * 100) / 100;
	}

	setCoInsurer(field,index, val){
		this.state.coInsurerList[index][field]=val;
		this.setState({coInsurerList: this.state.coInsurerList});
	}

	addCoInsurer(){
		this.state.coInsurerList.push({
			companyId: 0,
			rate: "0"
		})
		this.setState({ coInsurerList: this.state.coInsurerList });
	}

	removeInsurer(index){
		this.state.coInsurerList.splice(index, 1);
		this.setState({ coInsurerList: this.state.coInsurerList });
	}

    setInsuredBudgetFromRate(newRate:string) {
        this.state.manifestation["unavailabilityRate"] = newRate;
        if(this.state.scheduleData.value.budget) {
            this.state.manifestation.insuredBudget = parseInt(newRate)*this.state.scheduleData.value.budget/100;
        }
        this.setState({manifestation: this.state.manifestation});
	}

	setRateFromInsuredBudget(newBudget:string) {
        this.state.manifestation["insuredBudget"] = newBudget;
        if(this.state.scheduleData.value.budget) {
            this.state.manifestation.unavailabilityRate = parseInt(newBudget)/this.state.scheduleData.value.budget*100;
        }
        this.setState({manifestation: this.state.manifestation});
    }

    setWeatherBudget(newRate:string, newPercent:string) {
        if(newRate) {
        	this.state.manifestation.badWeatherRate = parseInt(newRate);
        }
        if(newPercent) {
        	this.state.manifestation.percentBudgetToBadWeather = parseInt(newPercent);
        }
        if(this.state.scheduleData.value.budget && this.state.manifestation.badWeatherRate && this.state.manifestation.percentBudgetToBadWeather) {
            this.state.manifestation.weatherBudget = this.state.scheduleData.value.budget*(this.state.manifestation.badWeatherRate/100)*(this.state.manifestation.percentBudgetToBadWeather/100);
        }
        this.setState({manifestation: this.state.manifestation});
	}

	clearClient(e:any){
		e.preventDefault();		
		this.state.manifestation.clientId = null;
		this.state.client.hydrate({});
		this.setState({
			clientId: null,
			client: this.state.client
		});
	}

	render() {
		const {t} : any = this.props;

		var budgetObject = this.budgetRules(this.state);
		//budgetObject.C5 < 368 ? this.state.BNSType.value = '' : false;


		var personInsuredListDOM = this.state.personInsuredList.map((item, index) => {
			return (
				<tr key={index}>
					<td>
						{index}
					</td>
					<td className="padding-input-array">
						<CustomInputText
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "band", index)}
							model={item.band}
						></CustomInputText>
					</td>

					<td className="padding-input-array">
						<CustomInputText
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "insuredPeople", index)}
							model={item.insuredPeople}
						></CustomInputText>
					</td>
					<td className="padding-input-array">
						<CustomInputText
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "function", index)}
							model={item.function}
						></CustomInputText>
					</td>
					<td className="padding-input-array">
						<CustomInputSelect
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "replaceable", index)}
							model={item.replaceable}
							optionList={[
									{
										value: 'NO',
										label: t('common:no')
									},{
										value: 'YES',
										label: t('common:yes')
									}
								]}
						></CustomInputSelect>
					</td>
					<td className="padding-input-array">
						<CustomInputCheckbox
							name="OverAgeLimit"
							value={item.overAgeLimit}
							label={''}
                            style={{display: 'inline-block', left:'10 px', 'text-align': 'center'}}
							isChecked={item.overAgeLimit}
							onChangeFn={(newValue) => this.handleAgeCheckboxChange(newValue, index)}
						></CustomInputCheckbox>
					</td>
					<td>
						<CustomInputNumber
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "age", index)}
							model={item.age}
							disabled={this.state.personInsuredList[index].overAgeLimit}
							min="0"
							max="200"
						></CustomInputNumber>
					</td>
					<td className="padding-input-array" >
						<CustomInputSelect
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleUnavailabilityField(newValue, "medicalExamination", index)}
							model={item.medicalExamination}
							optionList={[
									{
										value: 'NO',
										label: t('common:no')
									},{
										value: 'YES',
										label: t('common:yes')
									}
								]}
						></CustomInputSelect>
					</td>
				</tr>
			)

		})

		var technicianInsuredListDOM = this.state.technicianList.map((item, index) => {
			return (
				<tr key={index}>
					<td >
						{index}
					</td>
					<td className="padding-input-array">
						<CustomInputText
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleTechnicianUnavailabilityField(newValue, "firstName", index)}
							model={item.firstName}
						></CustomInputText>
					</td>
					<td className="padding-input-array">
						<CustomInputText
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleTechnicianUnavailabilityField(newValue, "lastName", index)}
							model={item.lastName}
						></CustomInputText>
					</td>
					<td className="padding-input-array">
						<CustomInputNumber
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleTechnicianUnavailabilityField(newValue, "age", index)}
							model={item.age}
							min="0"
							max="200"
						></CustomInputNumber>
					</td>
					<td className="padding-input-array">
						<CustomInputSelect
							hasFormGroup = {false}
							hasNoLabel ={true}
							onChangeFn={(newValue) => this.handleTechnicianUnavailabilityField(newValue, "medicalExamination", index)}
							model={item.medicalExamination}
							optionList={[
									{
										value: 'NO',
										label: t('common:no')
									},{
										value: 'YES',
										label: t('common:yes')
									}
								]}
						></CustomInputSelect>
					</td>
				</tr>
			)

		})

		var sumInsurerRate = parseFloat(this.state.manifestation.rateInsurer);
		var coInsurerListDom = [];
		this.state.coInsurerList.map((item, index) => {
			let row = 	<div className="row coInsurerList" key={index}>
							<div className="col-sm-4">
								<div className="coInsurerBtn">
									{
										this.state.manifestation.sortPDF != TYPE_CONTRAT ?
										<i className="fa fa-minus-circle" onClick={()=>this.removeInsurer(index)}></i>
										:<div></div>
									}
								</div>
							</div>
							<div className="col-sm-4">
								<CustomInputSelect
									name="coInsurerId"
									onChangeFn={(newValue) => this.setCoInsurer('companyId',index, newValue)}
									model={item.companyId}
									optionList={this.state.helperData.assureurListArr}
								></CustomInputSelect>
							</div>
							<div className="col-sm-4">
								<CustomInputRate
									name="rate"
									onChangeFn={(newValue) => this.setCoInsurer("rate", index, newValue)}
									model={item.rate}
								></CustomInputRate>
							</div>
						</div>
			coInsurerListDom.push(row);
			sumInsurerRate += parseFloat(item.rate);
		})
		coInsurerListDom.push(
			<div className="row coInsurerList" key={-1}>
				<div className="col-sm-4">
					<div className="coInsurerBtn">
						{
							this.state.manifestation.sortPDF != TYPE_CONTRAT ?
							<i className="fa fa-plus-circle" onClick={this.addCoInsurer}></i>
							:<div></div>
						}						
					</div>
				</div>
			</div>
		);

		this.checkForm()
		return (
			<div className="content-w">
				<div className="content-i">
					<div className="content-box">
						<div className="row">
							<div className="col-sm-12">
								<div className="element-wrapper">
									<h6 className="element-header">
										{
											(this.props.params.id ? t('manifestation:editManifestationTitle')
												: t('manifestation:createManifestationTitle'))
										}
									</h6>
									<div className="element-box">
										<form id="formValidate">
											{
												this.state.manifestation.sortPDF != TYPE_CONTRAT ?
												<div className="row">
													<div className="col-sm-4 offset-8">
														<CustomInputText
															name="projectNumber"
															label={t('manifestation:projectNumber')}
															onChangeFn={(newValue) => this.setManifestation("projectNumber", newValue)}
															model={this.state.manifestation.projectNumber}
														></CustomInputText>
													</div>
												</div> 
												:
												<div>
													<div className="row">
														<div className="col-sm-4 offset-8">
															<CustomInputText
																name="contractNumber"
																label={t('manifestation:contractNumber')}
																onChangeFn={(newValue) => this.setManifestation("contractNumber", newValue)}
																model={this.state.manifestation.contractNumber}
																disabled={true}
															></CustomInputText>
														</div>
													</div> 
													<div className="row">
														<div className="col-sm-4 offset-8">
															{t('manifestation:inceptionDate')}: {(new Date(this.state.manifestation.inceptionDate).toLocaleDateString('fr'))}
														</div>
													</div> 
												</div>
											}
											<fieldset className="form-group">
												<legend><span>{t('manifestation:generalInformation')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="eventName"
																label={t('manifestation:eventName')}
																validationFn={this.state.manifestation.validationFn}
																onChangeFn={(newValue) => this.setManifestation("eventName", newValue)}
																model={this.state.manifestation.eventName}
																errorLabel={t('manifestation:eventNameError')}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputText>
														</div>
														{/*														<div className="col-sm-3">
														 <CustomInputText
														 name="totalDeclaredBudget"
														 label={t('manifestation:totalDeclaredBudget')}
														 onChangeFn={(newValue) => this.setManifestation("totalDeclaredBudget", newValue)}
														 model={this.state.manifestation.totalDeclaredBudget}
														 errorLabel={t('manifestation:totalDeclaredBudgetError')}
														 ></CustomInputText>
														 </div>*/}
														<div className="col-sm-3">
															<CustomInputSelect
																name="status"
																label={t('manifestation:status')}
																validationFn={this.state.manifestation.validationFn}
																onChangeFn={(newValue) => this.setManifestation("status", newValue)}
																model={this.state.manifestation.status}
																errorLabel={t('manifestation:statusError')}
																disabled={true}
																optionList={[
																	{
																		value: ONGOING,
																		label: t('manifestation:ongoingProject')
																	}, {
																		value: CANCELED,
																		label: t('manifestation:canceledProject')
																	}, {
																		value: TRANSFORMED,
																		label: t('manifestation:transformedProject')
																	}
																]}
															></CustomInputSelect>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-4">
															<CustomInputSelect
																name="accountManagerId"
																label={t('manifestation:accountManagerId')}
																validationFn={this.state.manifestation.validationFn}
																onChangeFn={(newValue) => this.setManifestation("accountManagerId", newValue)}
																model={this.state.manifestation.accountManagerId}
																errorLabel={t('manifestation:accountManagerIdError')}
																optionList={this.state.helperData.accountManagerListArr}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
														<div className="col-sm-4">
															<CustomInputSelect
																name="policyLanguage"
																label={t('manifestation:policyLanguage')}
																onChangeFn={(newValue) => this.setManifestation("policyLanguage", newValue)}
																model={this.state.manifestation.policyLanguage}
																optionList={[
																		{
																			value: 'french',
																			label: t('manifestation:francais')
																		},
																		{
																			value: 'english',
																			label: t('manifestation:anglais')
																		}
																	]}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
														<div className="col-sm-4">
															<CustomInputSelect
																name="currency"
																label={t('manifestation:currency')}
																onChangeFn={(newValue) => this.setManifestation("currency", newValue)}
																model={this.state.manifestation.currency}
																optionList={this.state.helperData.deviseList}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
													</div>
													<div className={this.state.manifestation.isBusinessProvider ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="isBusinessProvider"
																	label={t('manifestation:businessProvider')}
																	model={this.state.manifestation.isBusinessProvider}
																	isChecked={this.state.manifestation.isBusinessProvider}
																	onChangeFn={(newValue) => this.setManifestation("isBusinessProvider", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.isBusinessProvider ?
																	<div className="col-sm-4">
																		<CustomInputSelect
																			name="businessProviderId"
																			label={t('manifestation:businessProviderId')}
																			onChangeFn={(newValue) => this.setManifestation("businessProviderId", newValue)}
																			model={this.state.manifestation.businessProviderId}
																			optionList={this.state.helperData.businessProviderListArr}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputSelect>
																	</div> : <div></div>
															}
															{
																this.state.manifestation.isBusinessProvider ?
																	<div className="col-sm-4">
																		<CustomInputSelect
																			name="businessProviderFeesRate"
																			label={t('manifestation:businessProviderFeesRate')}
																			onChangeFn={(newValue) => this.setManifestation("businessProviderFeesRate", newValue)}
																			model={this.state.manifestation.businessProviderFeesRate}
																			optionList={[
																					{
																						value: "30",
																						label: "30%"
																					},{
																						value: "50",
																						label: "50%"
																					}
																				]}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputSelect>
																	</div>
																	:
																	<div></div>
															}
														</div>
													</div>
													<div className={this.state.manifestation.hasAdditionalInsuredParty ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasAdditionalInsuredParty"
																	label={t('manifestation:hasAdditionalInsuredParty')}
																	model={this.state.manifestation.hasAdditionalInsuredParty}
																	isChecked={this.state.manifestation.hasAdditionalInsuredParty}
																	onChangeFn={(newValue) => this.setManifestation("hasAdditionalInsuredParty", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.hasAdditionalInsuredParty ?
																	<div className="col-sm-6">
																		<CustomInputText
																			name="additionalInsuredPartyName"
																			label={t('manifestation:additionalInsuredPartyName')}
																			onChangeFn={(newValue) => this.setManifestation("additionalInsuredPartyName", newValue)}
																			model={this.state.manifestation.additionalInsuredPartyName}
																			errorLabel={t('manifestation:additionalInsuredPartyNameError')}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputText>
																	</div>
																	: <div></div>
															}
														</div>
													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>Client</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-6">
															<div
																className={"form-group "+ (this.state.client.validationFn("businessName", this.state.client.businessName) ? "" : "has-error has-danger")}>
																<label htmlFor="businessName"
																	className="form-control-label">{t('form:businessName')}</label>
																<ReactAutocomplete
																	items={this.state.clientsAutoCompl}
																	shouldItemRender={(item, value) => item.businessName.toLowerCase().indexOf(value.toLowerCase()) > -1}
																	getItemValue={item => item.businessName}
																	renderInput={(props)=><input {...props} className="form-control"/>}
																	wrapperStyle={{display: 'block'}}
																	menuStyle={{
																		borderRadius: '3px',
																		boxShadow: '0 2px 12px rgba(0, 0, 0, 0.2)',
																		background: 'rgba(255, 255, 255, 0.9)',
																		padding: '2px 0',
																		fontSize: '90%',
																		position: 'fixed',
																		overflow: 'auto',
																		maxHeight: '50%',
																		zIndex: '10'
																	}}
																	renderItem={(item, highlighted) =>
																		<div key={item.id} style={{ backgroundColor: highlighted ? '#ddd' : 'transparent', padding: 2}}	>
																			{item.businessName}
																		</div>
																	}
																	value={this.state.client.businessName}
																	onChange={this.onAutocompleteChange}
																	onSelect={this.onAutocompleteSelect}
																	inputProps={{ disabled:this.state.clientId }}
																/>
															</div>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="siretNumber"
																label={t('form:siretNumber')}
																validationFn={this.state.client.validationFn}
																onChangeFn={(newValue) => this.setClient("siretNumber", newValue)}
																model={this.state.client.siretNumber}
																disabled={this.state.clientId}
																errorLabel={t('form:siretNumberError')}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="vatNumber"
																label={t('form:vatNumber')}
																onChangeFn={(newValue) => this.setClient("vatNumber", newValue)}
																model={this.state.client.vatNumber}
																disabled={this.state.clientId}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="address"
																label={t('form:address')}
																onChangeFn={(newValue) => this.setClient("address", newValue)}
																model={this.state.client.address}
																disabled={this.state.clientId}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="addressCompl"
																label={t('form:address2')}
																onChangeFn={(newValue) => this.setClient("addressCompl", newValue)}
																model={this.state.client.addressCompl}
																disabled={this.state.clientId}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="city"
																label={t('form:city')}
																onChangeFn={(newValue) => this.setClient("city", newValue)}
																model={this.state.client.city}
																disabled={this.state.clientId}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputZipCode
																name="postalCode"
																label={t('form:zipCode')}
																onChangeFn={(newValue) => this.setClient("postalCode", newValue)}
																model={this.state.client.postalCode}
																disabled={this.state.clientId}
															></CustomInputZipCode>
														</div>
														<div className="col-sm-6">
															<CustomCountrySelect
																name="countryId"
																label={t('form:countryId')}
																onChangeFn={(newValue) => this.setManifestation("countryId", newValue)}
																model={this.state.manifestation.countryId}
																disabled={this.state.clientId}
															></CustomCountrySelect>
														</div>
													</div>
													{ this.state.clientId?
														<div className="row">
															<div className="col-sm-6">
																<button 
																	className="btn btn-sm btn-warning" 
																	onClick={(e)=>{ this.clearClient(e) }}
																	disabled={this.state.manifestation.sortPDF}
																>{t("manifestation:clear")}</button>
															</div>
														</div>:<div></div>
													}
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:planning')}</span>
												</legend>
												<ScheduleContainer
													handleUpdate={this.handleScheduleUpdate}
													data={this.state.scheduleData}
													disabled={this.state.manifestation.sortPDF}			   
												/>
											</fieldset>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:budget')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-6">
															<CustomInputSelect
																name="BNSType"
																label={t('manifestation:BNSType')}
																onChangeFn={(newValue) => this.setManifestation("BNSType", newValue)}
																model={this.state.manifestation.BNSType}
																errorLabel={t('manifestation:BNSTypeError')}
																optionList={[
																		{
																			value: '',
																			label: t('manifestation:noBNS')
																		},{
																			value: 'DEDUCTED_BSN',
																			label: t('manifestation:deductedBNS')
																		},{
																			value: 'NOT_DECUTED_BNS',
																			label: t('manifestation:notDeductedBNS')
																		}
																	]}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
														<div className="col-sm-6">
															<CustomInputSelect
																name="applicableFranchise"
																label={t('manifestation:applicableFranchise')}
																onChangeFn={(newValue) => this.setManifestation("applicableFranchise", newValue)}
																model={this.state.manifestation.applicableFranchise}
																errorLabel={t('manifestation:applicableFranchiseError')}
																optionList={[
																		{
																			value: '',
																			label: t('manifestation:noFranchise')
																		},{
																			value: 'CAPITAL_FRANCHISE',
																			label: t('manifestation:capitalFranchise')
																		},{
																			value: 'NUMBER_REPRESENTATION_FRANCHISE',
																			label: t('manifestation:nomberRepresentationFranchise')
																		},{
																			value: 'OTHER_FRANCHISE',
																			label: t('manifestation:otherFranchise')
																		}
																	]}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
													</div>
													<div className="row">

														<div className="col-sm-4">
															<CustomInputRate
																name="comprehensiveInsuranceRate"
																label={t('manifestation:comprehensiveInsuranceRate')}
																onChangeFn={(newValue) => this.setManifestation("comprehensiveInsuranceRate", newValue)}
																model={this.state.manifestation.comprehensiveInsuranceRate}
																errorLabel={t('manifestation:comprehensiveInsuranceRateError')}
																min="0"
																max="5"
																disabled={this.state.manifestation.sortPDF}
															></CustomInputRate>
														</div>
														{
															this.state.manifestation.BNSType ?
																<div className="col-sm-4">
																	<CustomInputNumber
																		name="BNS"
																		label={t('manifestation:BNS')}
																		onChangeFn={(newValue) => this.setManifestation("BNS", newValue)}
																		model={this.state.manifestation.BNS}
																		errorLabel={t('manifestation:BNSError')}
																		min="0"
																		max="50"
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputNumber>
																</div> : <div></div>
														}
														{
															this.state.manifestation.applicableFranchise ?
																<div className="col-sm-4">
																	<CustomInputNumber
																		name="franchiseAmount"
																		label={t('manifestation:franchiseAmount')}
																		onChangeFn={(newValue) => this.setManifestation("franchiseAmount", newValue)}
																		model={this.state.manifestation.franchiseAmount}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputNumber>
																</div> : <div></div>
														}

													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:specialDisposition')}</span>
												</legend>
											</fieldset>
											<CustomInputTextarea
												name="specialConditions"
												label={t('manifestation:specialConditions')}
												onChangeFn={(newValue) => this.setManifestation("specialConditions", newValue)}
												model={this.state.manifestation.specialConditions}
												disabled={this.state.manifestation.sortPDF}
											></CustomInputTextarea>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:extention')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className={this.state.manifestation.hasUnavailabiliytExtension ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasUnavailabiliytExtension"
																	label={t('manifestation:hasUnavailabiliytExtension')}
																	model={this.state.manifestation.hasUnavailabiliytExtension}
																	isChecked={this.state.manifestation.hasUnavailabiliytExtension}
																	onChangeFn={(newValue) => this.setManifestation("hasUnavailabiliytExtension", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.hasUnavailabiliytExtension ?
																	<div className="col-sm-4">
																		<CustomInputRate
																			name="unavailabilityRate"
																			label={t('manifestation:unavailabilityRate')}
																			onChangeFn={(newValue) => {
																				this.setInsuredBudgetFromRate(newValue);
                                                                            }}
																			model={this.state.manifestation.unavailabilityRate}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</div> : <div></div>
															}
															{
																this.state.manifestation.hasUnavailabiliytExtension ?
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="insuredBudget"
																			label={t('manifestation:insuredBudget')}
																			onChangeFn={(newValue) => {
                                                                                this.setRateFromInsuredBudget(newValue);
																			}}
																			model={this.state.manifestation.insuredBudget}
																			min="0"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>
															}
															{
																this.state.manifestation.hasUnavailabiliytExtension ?
																	<div className="col-sm-6 offset-4">
																		<CustomInputNumber
																			name="numberPersonInsured"
																			label={t('manifestation:numberInsuredPerson')}
																			onChangeFn={(newValue) => this.handleNumberPersonInsured(newValue)}
																			model={this.state.manifestation.numberPersonInsured}
																			min="0"
																			max="50"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>
															}
														</div>
														{
															this.state.manifestation.hasUnavailabiliytExtension ?
																<div className="row">
																	<div className="col-sm-12">
																		<div className="table-responsive">
																			<table
																				className="table table-bordered table-lg table-v2 table-striped">
																				<thead>
																				<tr>
																					<th>#</th>
																					<th>{t('manifestation:band')}</th>
																					<th>{t('manifestation:insuredPeople')}</th>
																					<th>{t('manifestation:function')}</th>
																					<th>{t('manifestation:replaceable')}</th>
																					<th>{t('manifestation:overAgeLimit')}</th>
																					<th>{t('manifestation:age')}</th>
																					<th>{t('manifestation:medicalExamination')}</th>
																				</tr>
																				</thead>
																				<tbody>
																				{personInsuredListDOM}
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>   : <div></div>
														}
														{
															this.state.manifestation.hasUnavailabiliytExtension ?
																<div className="row">
																	<div className="col-sm-4">
																		<CustomInputCheckbox
																			name="isTechnicianIndispensable"
																			label={t('manifestation:isTechnicianIndispensable')}
																			model={this.state.manifestation.isTechnicianIndispensable}
																			isChecked={this.state.manifestation.isTechnicianIndispensable}
																			onChangeFn={(newValue) => this.setManifestation("isTechnicianIndispensable", newValue)}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputCheckbox>
																	</div>
																	{
																		this.state.manifestation.isTechnicianIndispensable ?
																			<div className="col-sm-4">
																				<CustomInputNumber
																					name="numberTechnicianInsured"
																					label={t('manifestation:numberTechnicianInsured')}
																					onChangeFn={(newValue) => this.handleNumberTechnicianInsured(newValue)}
																					model={this.state.manifestation.numberTechnicianInsured}
																					min="0"
																					max="50"
																					disabled={this.state.manifestation.sortPDF}
																				></CustomInputNumber>
																			</div> : <div></div>
																	}
																	{
																		this.state.manifestation.isTechnicianIndispensable ?
																			<div className="col-sm-4">
																				<CustomInputCheckbox
																					name="isTechnicianInsuredNamed"
																					label={t('manifestation:isTechnicianInsuredNamed')}
																					model={this.state.manifestation.isTechnicianInsuredNamed}
																					isChecked={this.state.manifestation.isTechnicianInsuredNamed}
																					onChangeFn={(newValue) => this.setManifestation("isTechnicianInsuredNamed", newValue)}
																					disabled={this.state.manifestation.sortPDF}
																				></CustomInputCheckbox>
																			</div> : <div></div>
																	}
																</div>
																:<div></div>
														}
														{
															this.state.manifestation.hasUnavailabiliytExtension && this.state.manifestation.isTechnicianIndispensable && this.state.manifestation.isTechnicianInsuredNamed ?
																<div className="row">
																	<div className="col-sm-12">
																		<div className="table-responsive">
																			<table
																				className="table table-bordered table-lg table-v2 table-striped">
																				<thead>
																				<tr>
																					<th>#</th>
																					<th>{t('form:lastName')}</th>
																					<th>{t('form:firstName')}</th>
																					<th>{t('manifestation:age')}</th>
																					<th>{t('manifestation:medicalExamination')}</th>
																				</tr>
																				</thead>
																				<tbody>
																				{technicianInsuredListDOM}
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
																: <div></div>
														}
														{
															this.state.manifestation.hasUnavailabiliytExtension ?
																<div className="row">
																	<div className="col-sm-6">
																		<CustomInputCheckbox
																			name="isFeeDeducted"
																			label={t('manifestation:isFeeDeducted')}
																			model={this.state.manifestation.isFeeDeducted}
																			isChecked={this.state.manifestation.isFeeDeducted}
																			onChangeFn={(newValue) => this.setManifestation("isFeeDeducted", newValue)}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputCheckbox>
																	</div>

																	{
																		this.state.manifestation.isFeeDeducted ?
																			<div className="col-sm-6">
																				<CustomInputNumber
																					name="artisteFee"
																					label={t('manifestation:artisteFee')}
																					onChangeFn={(newValue) => this.setManifestation("artisteFee", newValue)}
																					model={this.state.manifestation.artisteFee}
																					min="0"
																					max="100"
																					disabled={this.state.manifestation.sortPDF}
																				></CustomInputNumber>
																			</div> : <div></div>
																	}
																</div> : <div></div>
														}
														{
															this.state.manifestation.hasUnavailabiliytExtension ?
																<div className="row">
																	<div className="col-sm-4">
																		<CustomInputCheckbox
																			name="hasSpecialDispositionExtension"
																			label={t('manifestation:hasSpecialDispositionExtension')}
																			model={this.state.manifestation.hasSpecialDispositionExtension}
																			isChecked={this.state.manifestation.hasSpecialDispositionExtension}
																			onChangeFn={(newValue) => this.setManifestation("hasSpecialDispositionExtension", newValue)}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputCheckbox>
																	</div>

																	{
																		this.state.manifestation.hasSpecialDispositionExtension ?
																			<div className="col-sm-8">
																				<CustomInputTextarea
																					name="specialDisposition"
																					label={t('manifestation:specialDisposition')}
																					onChangeFn={(newValue) => this.setManifestation("specialDisposition", newValue)}
																					model={this.state.manifestation.specialDisposition}
																					disabled={this.state.manifestation.sortPDF}
																				></CustomInputTextarea>
																			</div> : <div></div>
																	}
																</div> : <div></div>
														}
													</div>
													<div className={this.state.manifestation.hasBadWeatherExtension ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasBadWeatherExtension"
																	label={t('manifestation:hasBadWeatherExtension')}
																	model={this.state.manifestation.hasBadWeatherExtension}
																	isChecked={this.state.manifestation.hasBadWeatherExtension}
																	onChangeFn={(newValue) => this.setManifestation("hasBadWeatherExtension", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
														</div>
														{
															this.state.manifestation.hasBadWeatherExtension ?
																<div className="row">
																	<div className="col-sm-4 offset-4">
																		<CustomInputRate
																			name="badWeatherRate"
																			label={t('manifestation:badWeatherRate')}
																			onChangeFn={(newValue) => this.setWeatherBudget(newValue, null)}
																			model={this.state.manifestation.badWeatherRate}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</div>
																	<div className="col-sm-4">
																		<CustomInputRate
																			name="percentBudgetToBadWeather"
																			label={t('manifestation:percentBudgetToBadWeather')}
																			onChangeFn={(newValue) => this.setWeatherBudget(null, newValue)}
																			model={this.state.manifestation.percentBudgetToBadWeather}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</div>
																	<div className="col-sm-4 offset-4">
																		<CustomInputNumber
																			name="weatherBudget"
																			label={t('manifestation:weatherBudget')}
																			onChangeFn={(newValue) => this.setManifestation("weatherBudget", newValue)}
																			model={this.state.manifestation.weatherBudget}
																			min="0"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div>
																</div> : <div></div>
														}
													</div>
													<div className={this.state.manifestation.hasCrowShortageExtension ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasCrowShortageExtension"
																	label={t('manifestation:hasCrowShortageExtension')}
																	model={this.state.manifestation.hasCrowShortageExtension}
																	isChecked={this.state.manifestation.hasCrowShortageExtension}
																	onChangeFn={(newValue) => this.setManifestation("hasCrowShortageExtension", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>

															{
																this.state.manifestation.hasCrowShortageExtension ?
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="franchise"
																			label={t('manifestation:franchise')}
																			onChangeFn={(newValue) => this.setManifestation("franchise", newValue)}
																			model={this.state.manifestation.franchise}
																			min="0"
																			max="100"
																			defaultValue="20"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>
															}
														</div>
													</div>
													<div className={this.state.manifestation.hasBombingExtension ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasBombingExtension"
																	label={t('manifestation:hasBombingExtension')}
																	model={this.state.manifestation.hasBombingExtension}
																	isChecked={this.state.manifestation.hasBombingExtension}
																	onChangeFn={(newValue) => this.setManifestation("hasBombingExtension", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.hasBombingExtension ?
																	<div className="col-sm-4">
																		<CustomInputCheckbox
																			name="bombingHasOptionThreat"
																			label={t('manifestation:bombingHasOptionThreat')}
																			model={this.state.manifestation.bombingHasOptionThreat}
																			isChecked={this.state.manifestation.bombingHasOptionThreat}
																			onChangeFn={(newValue) => this.setManifestation("bombingHasOptionThreat", newValue)}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputCheckbox>
																	</div> : <div></div>
															}
															{
																this.state.manifestation.hasBombingExtension ?
																	<div className="col-sm-4">
																		<CustomInputCheckbox
																			name="bombingHasOptionRecommendation"
																			label={t('manifestation:bombingHasOptionRecommendation')}
																			model={this.state.manifestation.bombingHasOptionRecommendation}
																			isChecked={this.state.manifestation.bombingHasOptionRecommendation}
																			onChangeFn={(newValue) => this.setManifestation("bombingHasOptionRecommendation", newValue)}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputCheckbox>
																	</div> : <div></div>
															}

														</div>
														{
															this.state.manifestation.hasBombingExtension ?
																<div className="row">
																	<div className="col-sm-4 offset-4">
																		<CustomInputRate
																			name="bombingRate"
																			label={t('manifestation:bombingRate')}
																			onChangeFn={(newValue) => this.setManifestation("bombingRate", newValue)}
																			model={this.state.manifestation.bombingRate}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</div>
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="bombingNumberOfDay"
																			label={t('manifestation:bombingNumberOfDay')}
																			onChangeFn={(newValue) => this.setManifestation("bombingNumberOfDay", newValue)}
																			model={this.state.manifestation.bombingNumberOfDay}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div>
																	<div className="col-sm-4 offset-4">
																		<CustomInputNumber
																			name="bombingNumberOfKilometre"
																			label={t('manifestation:bombingNumberOfKilometre')}
																			onChangeFn={(newValue) => this.setManifestation("bombingNumberOfKilometre", newValue)}
																			model={this.state.manifestation.bombingNumberOfKilometre}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div>
																</div> : <div></div>
														}
													</div>
													<div className="row">
														<div className="col-sm-4">
															<CustomInputCheckbox
																name="hasMoralReasonExtension"
																label={t('manifestation:hasMoralReasonExtension')}
																model={this.state.manifestation.hasMoralReasonExtension}
																isChecked={this.state.manifestation.hasMoralReasonExtension}
																onChangeFn={(newValue) => this.setManifestation("hasMoralReasonExtension", newValue)}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputCheckbox>
														</div>
													</div>
													<div className={this.state.manifestation.hasExpertFeeExtension ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasExpertFeeExtension"
																	label={t('manifestation:hasExpertFeeExtension')}
																	model={this.state.manifestation.hasExpertFeeExtension}
																	isChecked={this.state.manifestation.hasExpertFeeExtension}
																	onChangeFn={(newValue) => this.setManifestation("hasExpertFeeExtension", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.hasExpertFeeExtension ?
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="expertWarrantyAmount"
																			label={t('manifestation:expertWarrantyAmount')}
																			onChangeFn={(newValue) => this.setManifestation("expertWarrantyAmount", newValue)}
																			model={this.state.manifestation.expertWarrantyAmount}
																			min="0"
																			defaultValue="1500"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>

															}
															{
																this.state.manifestation.hasExpertFeeExtension ?
																	<div className="col-sm-4">
																		<CustomInputRate
																			name="expertFranchise"
																			label={t('manifestation:expertFranchise')}
																			onChangeFn={(newValue) => this.setManifestation("expertFranchise", newValue)}
																			model={this.state.manifestation.expertFranchise}
																			min="0"
																			max="100"
																			defaultValue="20"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</div> : <div></div>
															}
														</div>
													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:endOfGuarantee')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-4">
															<CustomInputSelect
																name="endOfGuaranteeRule"
																label={t('manifestation:endOfGuaranteeRule')}
																onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRule", newValue)}
																model={this.state.manifestation.endOfGuaranteeRule}
																optionList={[
																		{
																			value: "endOfGuaranteeProgramme",
																			label: t('manifestation:endOfGuaranteeProgramme')
																		},{
																			value: "endOfGuaranteActe",
																			label: t('manifestation:endOfGuaranteActe')
																		},{
																			value: "endOfGuaranteeDate",
																			label: t('manifestation:endOfGuaranteeDate')
																		},{
																			value: "endOfGuaranteeOther",
																			label: t('manifestation:endOfGuaranteeOther')
																		}
																	]}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
													</div>
													{
														this.state.manifestation.endOfGuaranteeRule == 'endOfGuaranteeProgramme' ?
															<div className="row">
																<div className="col-sm-4">
																	<CustomInputSelect
																		name="endOfGuaranteeRuleOnePercentDurationOfTheShow"
																		label={t('manifestation:endOfGuaranteeRuleOnePercentDurationOfTheShow')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleOnePercentDurationOfTheShow", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleOnePercentDurationOfTheShow}
																		optionList={[
																				{
																					value: "50%",
																					label: "50%"
																				},{
																					value: "60%",
																					label: "60%"
																				},{
																					value: "75%",
																					label: "75%"
																				},{
																					value: "100%",
																					label: "100%"
																				}
																			]}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputSelect>
																</div>
																<div className="col-sm-4">
																	<CustomInputSelect
																		name="endOfGuaranteeRuleOneHasPlayed"
																		label={t('manifestation:isOver')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleOneHasPlayed", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleOneHasPlayed}
																		optionList={[
																				{
																					value: "global",
																					label: t('manifestation:global')
																				},{
																					value: "headlining",
																					label: t('manifestation:headlining')
																				},{
																					value: "false",
																					label: t('manifestation:lastHeadlining')
																				}
																			]}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputSelect>
																</div>
																<div className="col-sm-4">
																	<CustomInputSelect
																		name="endOfGuaranteeRuleOneHasOption"
																		label={t('manifestation:option')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleOneHasOption", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleOneHasOption}
																		optionList={[
																				{
																					value: "na",
																					label: t('manifestation:na')
																				},{
																					value: "andHeadlining",
																					label: t('manifestation:andHeadlining')
																				},{
																					value: "orHeadlining",
																					label: t('manifestation:orHeadlining')
																				}
																			]}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputSelect>
																</div>
															</div> : <div></div>

													}
													{
														this.state.manifestation.endOfGuaranteeRule == 'endOfGuaranteActe' ?
															<div className="row">
																<div className="col-sm-4">
																	<CustomInputSelect
																		name="endOfGuaranteeRuleTwoActNumberIsOver"
																		label={t('manifestation:endOfGuaranteeRuleTwoActNumberIsOver')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleTwoActNumberIsOver", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleTwoActNumberIsOver}
																		optionList={[
																				{
																					value: "first",
																					label: t('manifestation:first')
																				},{
																					value: "second",
																					label: t('manifestation:second')
																				},{
																					value: "third",
																					label: t('manifestation:third')
																				},{
																					value: "beforeLast",
																					label: t('manifestation:beforeLast')
																				},{
																					value: "last",
																					label: t('manifestation:last')
																				}
																			]}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputSelect>
																</div>
															</div> : <div></div>
													}
													{
														this.state.manifestation.endOfGuaranteeRule == 'endOfGuaranteeDate' ?
															<div className="row">
																<div className="col-sm-6">
																	<CustomInputText
																		name="endOfGuaranteeRuleThreeTimeIsOver"
																		label={t('manifestation:endOfGuaranteeRuleThreeTimeIsOver')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleThreeTimeIsOver", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleThreeTimeIsOver}
																		errorLabel={t('manifestation:endOfGuaranteeRuleThreeTimeIsOver')}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputText>
																</div>
															</div> : <div></div>
													}
													{
														this.state.manifestation.endOfGuaranteeRule == 'endOfGuaranteeOther' ?
															<div className="row">
																<div className="col-sm-6">
																	<CustomInputText
																		name="endOfGuaranteeRuleFourCondition"
																		label={t('manifestation:endOfGuaranteeRuleFourCondition')}
																		onChangeFn={(newValue) => this.setManifestation("endOfGuaranteeRuleFourCondition", newValue)}
																		model={this.state.manifestation.endOfGuaranteeRuleFourCondition}
																		errorLabel={t('manifestation:endOfGuaranteeRuleThreeTimeIsOver')}
																		disabled={this.state.manifestation.sortPDF}
																	></CustomInputText>
																</div>
															</div> : <div></div>
													}
													<div className="row">
														<div className="col-sm-6">
															<CustomInputCheckbox
																name="averageClause"
																label={t('manifestation:averageClause')}
																model={this.state.manifestation.averageClause}
																isChecked={this.state.manifestation.averageClause}
																onChangeFn={(newValue) => this.setManifestation("averageClause", newValue)}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputCheckbox>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputCheckbox
																name="averagePremiumClause"
																label={t('manifestation:averagePremiumClause')}
																model={this.state.manifestation.averagePremiumClause}
																isChecked={this.state.manifestation.averagePremiumClause}
																onChangeFn={(newValue) => this.setManifestation("averagePremiumClause", newValue)}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputCheckbox>
														</div>
													</div>
													<div className={this.state.manifestation.contractualIndemnityLimit ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="contractualIndemnityLimit"
																	label={t('manifestation:contractualIndemnityLimit')}
																	model={this.state.manifestation.contractualIndemnityLimit}
																	isChecked={this.state.manifestation.contractualIndemnityLimit}
																	onChangeFn={(newValue) => this.setManifestation("contractualIndemnityLimit", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.contractualIndemnityLimit ?
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="contractualIndemnityLimitRate"
																			label={t('manifestation:contractualIndemnityLimitRate')}
																			onChangeFn={(newValue) => this.setManifestation("contractualIndemnityLimitRate", newValue)}
																			model={this.state.manifestation.contractualIndemnityLimitRate}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>
															}
															{
																this.state.manifestation.contractualIndemnityLimit ?
																	<div className="col-sm-4">
																		<CustomInputNumber
																			name="averageFinancialCommitment"
																			label={t('manifestation:averageFinancialCommitment')}
																			onChangeFn={(newValue) => this.setManifestation("averageFinancialCommitment", newValue)}
																			model={this.state.manifestation.averageFinancialCommitment}
																			min="0"
																			max="100"
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputNumber>
																	</div> : <div></div>
															}
														</div>
													</div>
													<div className="row">
														<div className="col-sm-4">
															<CustomInputDate
																name="subscriptionDeadLine"
																label={t('manifestation:subscriptionDeadLine')}
																onChangeFn={(newValue) => this.setManifestation("subscriptionDeadLine", newValue)}
																model={this.state.manifestation.subscriptionDeadLine}
																errorLabel={t('manifestation:subscriptionDeadLineError')}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputDate>
														</div>

													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:placement')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-4 offset-4">
															<CustomInputSelect
																name="mainInsurerId"
																label={t('manifestation:mainInsurerId')}
																onChangeFn={(newValue) => this.setManifestation("mainInsurerId", newValue)}
																model={this.state.manifestation.mainInsurerId}
																optionList={this.state.helperData.assureurListArr}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputSelect>
														</div>
														<div className="col-sm-4">
															<CustomInputRate
																name="rateInsurer"
																label={t('manifestation:rateInsurer')}
																onChangeFn={(newValue) => this.setManifestation("rateInsurer", newValue)}
																model={this.state.manifestation.rateInsurer}
																disabled={this.state.manifestation.sortPDF}
															></CustomInputRate>
														</div>
													</div>
													<div className={this.state.manifestation.isCoInsurance ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="isCoInsurance"
																	label={t('manifestation:isCoInsurance')}
																	model={this.state.manifestation.isCoInsurance}
																	isChecked={this.state.manifestation.isCoInsurance}
																	onChangeFn={(newValue) => this.setManifestation("isCoInsurance", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																sumInsurerRate > 100?
																<div className="col-sm-8">
																	<div className="has-error has-danger">
																		<div className="form-control-feedback">
																			{t('manifestation:rateSumError')}
																		</div>
																	</div>
																</div>
																:<div></div>
															}
														</div>
														{
															this.state.manifestation.isCoInsurance ?
															coInsurerListDom
															:
															<div></div>
														}
													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:expert')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className={this.state.manifestation.hasExpert ? 'form-part-selected' : ''}>
														<div className="row">
															<div className="col-sm-4">
																<CustomInputCheckbox
																	name="hasExpert"
																	label={t('manifestation:hasExpert')}
																	model={this.state.manifestation.hasExpert}
																	isChecked={this.state.manifestation.hasExpert}
																	onChangeFn={(newValue) => this.setManifestation("hasExpert", newValue)}
																	disabled={this.state.manifestation.sortPDF}
																></CustomInputCheckbox>
															</div>
															{
																this.state.manifestation.hasExpert ?
																	<div className="col-sm-4">
																		<CustomInputSelect
																			name="expertId"
																			label={t('manifestation:expertId')}
																			onChangeFn={(newValue) => this.setManifestation("expertId", newValue)}
																			model={this.state.manifestation.expertId}
																			optionList={this.state.helperData.expertListArr}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputSelect>
																	</div> : <div></div>
															}
														</div>
													</div>
												</div>
											</div>
											<fieldset className="form-group">
												<legend><span>{t('manifestation:cotisation')}</span>
												</legend>
											</fieldset>
											<div className="row">
												<div className="col-sm-12">
													<div className="row">
														<div className="col-sm-4">
															<CustomInputNumber
																name="notBNSContribution"
																label={t('manifestation:notBNSContribution')}
																model={budgetObject.notBNSContribution}
																disabled={true}
															></CustomInputNumber>
														</div>
														<div className="col-sm-4">
															<CustomInputNumber
																name="BNS"
																label={t('manifestation:BNS')}
																model={budgetObject.BNS}
																disabled={true}
															></CustomInputNumber>
														</div>
														<div className="col-sm-4">
															<CustomInputNumber
																name="wholeContribution"
																label={t('manifestation:wholeContribution')}
																model={budgetObject.wholeContribution}
																disabled={true}
															></CustomInputNumber>
														</div>
													</div>
													<div className="row">
														<div className="table-responsive">
															<table
																className="table table-bordered table-lg table-v2 table-striped">
																<thead>
																<tr>
																	<th className="manifestation-array-title-row"></th>
																	<th>{t('manifestation:allRisk')}</th>
																	<th>{t('manifestation:unavailability')}</th>
																	<th>{t('manifestation:badWeather')}</th>
																	<th>{t('manifestation:bombing')}</th>
																	<th>{t('manifestation:total')}</th>
																</tr>
																</thead>
																<tbody>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:exposedBudget')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.A1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.A2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.A3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.A4)} /></td>
																	<td className="manifestation-amount-title-row"></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:HTRate')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.B1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.B2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.B3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.B4)} /></td>
																	<td className="manifestation-amount-title-row"></td>
																</tr>
																<tr>
																	<td className="manifestation-array-title-row">{t('manifestation:HTAmount')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.C1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.C2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.C3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.C4)} /></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.C5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:HTBNS')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.D1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.D2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.D3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.D4)} /></td>
																	<td className="manifestation-amount-title-row" ><CustomNumberFormat value={this.displayNumberCommission(budgetObject.D5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:ovationCommission')}</td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td>
																		<CustomInputRate
																			name="ovationCommission"
																			onChangeFn={(newValue) => this.setManifestation("ovationCommission", newValue)}
																			model={this.state.manifestation.ovationCommission}
																			min="0"
																			max="100"
																			hasNoLabel ={true}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.E5)} /></td>
																</tr>
																<tr >
																	<td  className="manifestation-array-title-row">{t('manifestation:ovationCommissionBNS')}</td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td className="manifestation-array-title-row">
																		<CustomInputRate
																			name="ovationBSNCommission"
																			onChangeFn={(newValue) => this.setManifestation("ovationBSNCommission", newValue)}
																			model={this.state.manifestation.ovationBSNCommission}
																			min="0"
																			max="100"
																			hasNoLabel ={true}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.F5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">
																		<span>{t('manifestation:TaxPercent')}</span>
																		<CustomInputRate
																			name="taxRate"
																			onChangeFn={(newValue) => this.setManifestation("taxRate", newValue)}
																			model={this.state.manifestation.taxRate}
																			min="0"
																			max="30"
																			hasNoLabel ={true}
																			disabled={this.state.manifestation.sortPDF}
																		></CustomInputRate>
																	</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.G1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.G2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.G3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.G4)} /></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.G5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:BNSTaxe')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.H1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.H2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.H3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.H4)} /></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.H5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:TTCAmount')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.I1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.I2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.I3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.I4)} /></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.I5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:TTCBNS')}</td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.J1)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.J2)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.J3)} /></td>
																	<td><CustomNumberFormat value={this.displayNumberCommission(budgetObject.J4)} /></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.J5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:chargedAmount')}</td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.K5)} /></td>
																</tr>
																<tr >
																	<td className="manifestation-array-title-row">{t('manifestation:toBeReversedAmount')}</td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td></td>
																	<td className="manifestation-amount-title-row"><CustomNumberFormat value={this.displayNumberCommission(budgetObject.L5)} /></td>
																</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div className="row">
												<div className="form-buttons-w">
													<button
														className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')}
														type="button" onClick={this.save}>Enregistrer
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
