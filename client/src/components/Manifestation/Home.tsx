import * as React from "react";
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import { translate, Interpolate, Trans } from 'react-i18next';
import { ManifestationService } from "../../services/manifestation";
import { VersionService } from "../../services/version";
import { ParagraphService } from "../../services/paragraph";
import { ContractParagraphService } from "../../services/contractParagraph";
import i18n from '../../i18n';
import { Link } from 'react-router'
import $ from 'jquery';
import AddUser from "../Global/addUser/AddUser";
import { ManifestationList } from "./List";
import { SHOW_ALL, TYPE_PROJECT, TYPE_CONTRAT, ONGOING, CANCELED, TRANSFORMED } from '../../consts'
declare var $: any;

export interface ManifestationHomeProps {
    t: any
}
export interface ManifestationHomeState {
    searchValue: string,
    rowCount: number,
    offset: number,
    sortParams: any,
    pageCount: number,
    manifestationList: any,
    selectedManifestationIndex: number,
    tab: number
}

@translate(['manifestation'], { wait: true })
export class ManifestationHome extends React.Component<ManifestationHomeProps, ManifestationHomeState> {
    public state: any;

    constructor(props: ManifestationHomeProps) {
        super(props);
        // axios.defaults.baseURL = '/api/manifestations';
        this.state = {
            contacts: [],
            tab: TYPE_PROJECT
        };
    }

    componentDidMount() {
        //
    }

    setTab(value) {
        this.setState({
            tab: value,
        })
    }

    render() {
        const { t }: any = this.props;
        return (
            <div className="content-w template">
                <div className="content-i">
                    <div className="content-box">
                        <div className="os-tabs-w">

                            <ul className="nav nav-tabs nav-fill custom_tab_ctrl">
                                <li className="nav-item">
                                    <a aria-expanded="true" className="nav-link active" data-toggle="tab" onClick={() => {this.setTab(TYPE_PROJECT)}}>Projets</a>
                                </li>
                                <li className="nav-item">
                                    <a aria-expanded="false" className="nav-link" data-toggle="tab" onClick={() => {this.setTab(TYPE_CONTRAT)}}>Contrats</a>
                                </li>
                            </ul>
                            <div className="tab-content">
                                <div className="tab-pane active" role="tabpanel">
                                    <ManifestationList
                                    currentTab={this.state.tab}
                                    ></ManifestationList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
