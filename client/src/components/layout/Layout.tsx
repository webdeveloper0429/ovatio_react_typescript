import * as React from "react";
import { Sidebar } from './Sidebar'
import { MobileSidebar } from './MobileSidebar'

export interface LayoutProps { }

export class Layout extends React.Component<LayoutProps, any> {
	constructor(props: any) {
        super(props);
        this.handleShowMobile = this.handleShowMobile.bind(this);
        this.state = {
        	showMobile: false
        };
    }
    handleShowMobile(e){
    	e.preventDefault();
    	this.setState({ showMobile: !this.state.showMobile });
    }
	render() {
		return (
			<div className="all-wrapper menu-side with-side-panel">
				<div className="hamburger" onClick={this.handleShowMobile}>
					<span>Menu</span>
				</div>
				<MobileSidebar
					showMobile = {this.state.showMobile}
					handleShowMobile = {this.handleShowMobile}
				/>
				<div className="layout-w">
					<Sidebar />
					{this.props.children}
				</div>
				<div className="display-type"></div>
			</div>
		);
	}
}

export default Layout;
