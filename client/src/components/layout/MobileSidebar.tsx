import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import i18n from '../../i18n';
import {Link } from 'react-router'
import axios from 'axios';


@translate([], {wait: true})
export class MobileSidebar extends React.Component<any, any> {
	constructor(props: any) {
		super(props);
		this.state = {
			user: {
				email: 'Utilisateur'
			}
		};
		axios.get('/api/users/me').then(resp => {
			this.setState({
				user: resp.data
			})
		});
	}

	hasRight(rightLabel) {
		if (this.state.user && this.state.user.role) {
			let rightList = this.state.user.role.rightList;
			let found = false;
			rightList.forEach(function (_right) {
				if (rightLabel == _right.label) {
					found = true;
				}
			});
			return found;
		} else {
			return false;
		}
	}

	logOut(){
		axios({
			method: 'post',
			url: 'api/users/logout',
			baseURL: '/'
		})
		.then(resp => {
			window.location.pathname = '/login'
		});
	}

	render() {
		const {t} : any = this.props;
		return (
			<div className={"sidebar-wrapper "+(this.props.showMobile ? "show-sidebar" : "")}>
				<div className="content-panel-close" onClick={this.props.handleShowMobile}>
					<i className="os-icon os-icon-close"></i>
				</div>
				<div className="element-wrapper">
					<div className="logo-w sidebar-logo-container" onClick={this.props.handleShowMobile}>
						<Link to="/"><img className="sidebar-logo-img" alt=""  src="/img/logo_ovatio.jpg"/></Link>
					</div>
					<div className="menu-and-user">
						<div className="logged-user-w">
							<div className="logged-user-i">
								<div className="logged-user-info-w">
									<div className="logged-user-name">
										{ this.state.user ? this.state.user!.email : ''} <span onClick={() => this.logOut()} style={{fontSize : "small", color: "grey", fontStyle: "italic"}}>({t('common:logout')})</span>
									</div>

								</div>
							</div>
						</div>

						<ul className="main-menu">
							{
								this.hasRight('GET_USER_LIST') ?
									<li className="" onClick={this.props.handleShowMobile}>
										<Link to="/user">
											<div className="icon-w">
												<div className="os-icon os-icon-window-content"></div>
											</div>
											<span>{t('common:userTab')}</span>
										</Link>
									</li> :
									<li className="" onClick={this.props.handleShowMobile}>
										<Link to={`/user/edit/` + this.state.user.id }>
											<div className="icon-w">
												<div className="os-icon os-icon-window-content"></div>
											</div>
											<span>{t('common:myProfile')}</span>
										</Link>
									</li>
							}
							{
								this.hasRight('GET_COMPANY_LIST')?
									<li className="" onClick={this.props.handleShowMobile}>
										<Link to="/assureur">
											<div className="icon-w">
												<div className="os-icon os-icon-window-content"></div>
											</div>
											<span>{t('common:assureurTab')}</span>
										</Link>
									</li> : null
							}
							{
								<li className="" onClick={this.props.handleShowMobile}>
									<Link to="/businessProvider">
										<div className="icon-w">
											<div className="os-icon os-icon-window-content"></div>
										</div>
										<span>{t('common:businessProviderTab')}</span>
									</Link>
								</li>
							}
							{
								this.hasRight('GET_EXPERT_LIST')?
									<li className="" onClick={this.props.handleShowMobile}>
										<Link to="/expert">
											<div className="icon-w">
												<div className="os-icon os-icon-window-content"></div>
											</div>
											<span>{t('common:expertTab')}</span>
										</Link>
									</li> : null
							}
							{
								this.hasRight('GET_CLIENT_LIST')?
									<li className="" onClick={this.props.handleShowMobile}>
									<Link to="/client">
										<div className="icon-w">
											<div className="os-icon os-icon-window-content"></div>
										</div>
										<span>{t('common:clientTab')}</span>
									</Link>
								</li> : null
							}
							{
								<li className="" onClick={this.props.handleShowMobile}>
									<Link to="/manifestation">
										<div className="icon-w">
											<div className="os-icon os-icon-window-content"></div>
										</div>
										<span>{t('common:manifestationTab')}</span>
									</Link>
								</li>
							}
							{
								<li className="" onClick={this.props.handleShowMobile}>
									<Link to="/template">
										<div className="icon-w">
											<div className="os-icon os-icon-window-content"></div>
										</div>
										<span>{t('common:template')}</span>
									</Link>
								</li>
							}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

export default MobileSidebar;
