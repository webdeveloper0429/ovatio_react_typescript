import * as React from 'react';
import axios from 'axios';
import { Line, Circle } from 'rc-progress';
import {translate, Interpolate, Trans} from 'react-i18next';

@translate(['common'], {wait: true})
export class AvatarView extends React.Component<any, any> {
	public output: any;
	public file: any;
	constructor(props: any){
		super();
		axios.defaults.baseURL = '/api/avatar';
		this.state = {
			uploadProgress: 0,
			outputText: '',
			avatarData: {},
			canUpload: false
		}
	}
	componentDidMount(){
		this.setState({ avatarData: this.props.avatarData })
	}
	componentWillReceiveProps(nextProps){
		this.setState({ avatarData: nextProps.avatarData })
	}
	passData2Parent = ()=>{
		this.props.handleUpdate(this.state.avatarData);
	}
	handleFileChange = ()=>{
		if(this.file.files.length == 0) this.setState({ canUpload: false });
		else this.setState({ canUpload: true });
	}
	uploadHandle = ()=>{
		const {t} : any = this.props;
		var data = new FormData();
		data.append('foo', 'bar');
		data.append('avatar', this.file.files[0]);
		var config = {
			onUploadProgress: (progressEvent) => {
				var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
				this.setState({uploadProgress: percentCompleted});
			}
		};
		axios.post('/', data, config)
		.then( (res) => {
			this.output.className = 'alert alert-success';
			this.setState({ outputText: t('common:uploadSuccess') })
			this.state.avatarData.filename = res.data.filename;
			this.passData2Parent();
		})
		.catch( (err) => {
			this.output.className= 'alert alert-danger';
			this.setState({ outputText: t('common:uploadFailed') })
		});
	}
	render() {
		return (
			<div>
				<div className="user-profile">
					<div className="up-head-w" style={{backgroundImage: 'url(/api/avatar/'+this.state.avatarData.filename+')'}}>
						<div className="up-main-info">
							<div className="user-avatar-w">
								<div className="user-avatar">
									{
										this.state.avatarData.filename?
										<img className="" alt=""  src={"/api/avatar/"+this.state.avatarData.filename} /> : <div></div>
									}									
								</div>
							</div>
							<h1 className="up-header">
								{this.state.avatarData.displayName}
							</h1>
						</div>
						<svg className="decor" width='842' height='219' viewBox='0 0 842 219' preserveAspectRatio='xMaxYMax meet' xmlns='http://www.w3.org/2000/svg'>
							<path d='M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z' transform='translate(-381 -362)' fill='#FFF' />
						</svg>
					</div>
					<div className="up-controls">
					</div>
				</div>
				<div className="row">
					<div className="col-sm-4">
						<div className="form-group">
							<input ref={file => this.file = file} type="file" className="form-control" onChange={this.handleFileChange} accept="image/*"/>
						</div>
					</div>
					<div className="col-sm-2">
						<div className="form-group">
							<button type="button" className="btn btn-primary" onClick={this.uploadHandle} disabled={!this.state.canUpload}>Upload</button>
						</div>
					</div>
					<div className="col-sm-6">
						<div ref={output => this.output = output} className="" style={{padding: '0.4rem 1.45rem'}}>{this.state.outputText}</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-12">
						<Line percent={this.state.uploadProgress} strokeWidth="1" strokeColor="#3DC1F6" />
					</div>
				</div>
			</div>
		)
	}
}
