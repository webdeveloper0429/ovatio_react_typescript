import * as React from "react";

export default class TableAvatar extends React.Component<any, any> {
    constructor(props: any){
        super(props);
    }

    render(){
        return(
            <div className="table_avatar">
                <img className="" alt=""  src={this.props.src} />
            </div>
        )
    }
}