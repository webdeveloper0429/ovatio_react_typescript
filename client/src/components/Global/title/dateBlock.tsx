import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['common'], {wait: true})
export default class DateBlock extends React.Component<any, any> {

    constructor(props: any) {
        super();
        this.state = {
            deletedAt: null,
        };
    }
    componentWillReceiveProps(nextProps) {
        this.getDeletedAt(nextProps.deletedAt);
    }

    getDeletedAt(deletedAt:string) {
        if(!deletedAt) return;
        let deletedDate = new Date(deletedAt);
        let deletedString = deletedDate.getDate()+'/'+deletedDate.getMonth()+'/'+deletedDate.getFullYear();
        this.setState({deletedAt: deletedString});
    }

    render() {
        const {t} : any = this.props;
        return (
            <span className={"deletedDateBlock"}>
                {this.state.deletedAt ? t('common:deletedAt')+' '+this.state.deletedAt : ''}
            </span>
        );
    }
}