import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['form'], {wait: true})
export default class CustomInputSelect extends React.Component<any, any> {
	validationFn;
	onChangeFn;

	constructor(props: any) {
		super();
		this.state = {
			isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;

		this.handleFn = this.handleFn.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.validationFn){
			this.setState({
				isValid: this.validationFn(nextProps.name, nextProps.model)
			});
		}
		this.onChangeFn = nextProps.onChangeFn;
	}

	handleFn(event: any) {
		event.preventDefault();
		this.setState({
			isValid: this.validationFn ? this.validationFn(this.props.name, event.target.value) : true
		});
		if(this.onChangeFn){
			this.onChangeFn(event.target.value);
		}
	}

	render() {
		let optionListDOM = this.props.optionList.map(_option => {
			return (
				<option key={_option.value} value={_option.value}>
					{_option.label}
				</option>
			)
		});
		return (
			<div className={(this.props.hasFormGroup === false ? "" : "form-group") + (this.state.isValid ? "" : "has-error has-danger")}>
				{
					this.props.hasNoLabel === true ? null : <label>{this.props.label}</label>
				}
				<select className="form-control"
						disabled={this.props.disabled}
						value={this.props.model}
						onChange={this.handleFn}>

					<option value=""></option>
					{optionListDOM}
				</select>
				{
					this.state.isValid !== false ? <div></div>
						:<div
						className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
						<ul className="list-unstyled">
							<li>{this.props.errorLabel}</li>
						</ul>
					</div>
				}
			</div>
		);
	}
}