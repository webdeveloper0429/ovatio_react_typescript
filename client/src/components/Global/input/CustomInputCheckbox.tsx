import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['form'], {wait: true})
export default class CustomInputCheckbox extends React.Component<any, any> {
	validationFn;
	onChangeFn;

	constructor(props: any) {
		super();
		this.state = {
			isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;

		this.handleFn = this.handleFn.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.validationFn){
			this.setState({
				isValid: this.validationFn(nextProps.name, nextProps.model)
			});
		}
		this.onChangeFn = nextProps.onChangeFn;
	}

	handleFn(event: any) {
		if(this.validationFn){
			this.setState({
				isValid: this.validationFn(this.props.name, event.target.checked)
			});
		}
		if(this.onChangeFn){
			this.onChangeFn(event.target.checked);
		}
	}

	render() {
		return (
			<div className={"form-check " + (this.state.isValid ? "" : "has-error has-danger")}>
				<label className="form-check-label container-checkbox" style={this.props.style}>
					<input 
						value={this.props.model}
						checked={this.props.isChecked}
						onChange={this.handleFn}
						className="form-check-input"
						type="checkbox"
						disabled={this.props.disabled}
					/>
						<span className="checkmark" style={this.props.disabled ? {cursor:'not-allowed'} : {}}></span>
					{ this.props.label ? <span>{this.props.label}</span> : ''}
				</label>
				{
					this.state.isValid !== false ? <div></div>
						:<div
						className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
						<ul className="list-unstyled">
							<li>{this.props.errorLabel}</li>
						</ul>
					</div>
				}
			</div>
		);
	}
}