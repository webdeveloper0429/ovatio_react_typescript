import * as React from "react";
import DatePicker from 'react-datepicker';
import * as moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['form'], {wait: true})
export default class CustomInputDate extends React.Component<any, any> {
	validationFn;
	onChangeFn;

	constructor(props: any) {
		super();
		this.state = {
			isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;

		this.handleFn = this.handleFn.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.validationFn){
			this.setState({
				isValid: this.validationFn(nextProps.name, nextProps.model)
			});
		}
		this.onChangeFn = nextProps.onChangeFn;
	}

	handleFn(date: any) {
		if(this.validationFn){
			this.setState({
				isValid: this.validationFn(this.props.name, date)
			});
		}
		if( this.onChangeFn){
			this.onChangeFn(date);
		}
	}

	render() {
		return (
			<div className={"form-group " + (this.state.isValid ? "" : "has-error has-danger")}>
				<label>{this.props.label}</label>
				<DatePicker
					dateFormat="DD/MM/YYYY"
					className="form-control"
					selected={this.props.model? moment(this.props.model) : undefined }
					minDate={this.props.minDate? moment(this.props.minDate) : undefined }
					maxDate={this.props.maxDate? moment(this.props.maxDate) : undefined }
					onChange={this.handleFn}
					disabled={this.props.disabled ? true : false}
				/>
				{
					this.state.isValid !== false ? <div></div>
						:<div
						className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
						<ul className="list-unstyled">
							<li>{this.props.errorLabel}</li>
						</ul>
					</div>
				}
			</div>
		);
	}
}