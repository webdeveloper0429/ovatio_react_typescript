import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import NumberFormat from 'react-number-format';


@translate(['form'], {wait: true})
export default class CustomNumberFormat extends React.Component<any, any> {


	constructor(props: any) {
		super();
		this.state = {

		};

	}

	render() {
		return (
			<NumberFormat value={this.props.value} displayType={'text'} thousandSeparator={' '} />
		);
	}
}