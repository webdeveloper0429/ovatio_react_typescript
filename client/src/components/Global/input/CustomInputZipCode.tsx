import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import { ZipcodeService } from "./../../../services/zipcode";


@translate(['form'], {wait: true})
export default class CustomInputZipCode extends React.Component<any, any> {

	validationFn;
	onChangeFn;
	populateCallback;
    country;

	constructor(props: any) {

		super();
		this.state = {
			isValid:  true,
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;
		this.populateCallback = props.populateCallback;
        this.country = props.country;

		this.handleFn = this.handleFn.bind(this);
		this.checkValid = this.checkValid.bind(this);

	}

	componentWillReceiveProps(nextProps) {
		this.onChangeFn = nextProps.onChangeFn;
        this.populateCallback = nextProps.populateCallback;
        this.country = nextProps.country;
        this.setState({
            isValid: true
        });
	}

	handleFn(event: any) {

		event.preventDefault();
        if(this.onChangeFn){
            this.onChangeFn(event.target.value);
        }


	}

	checkValid(event) {
		this.validate(event.target.value);
	}

	validate(zipcode) {
		if(!zipcode) {
            this.setState({
                isValid: true
            })
			return;
		}
        if(zipcode.length < 5) {
            this.setState({
                isValid: false
            })
            return;
        }
        if(this.country && this.country != 1) return; //todo: not use a hardcoded 1?

        ZipcodeService.get({zipcode:zipcode}).then((zipcodeObject) => {
            this.setState({
                isValid: !!zipcodeObject
            })
            if(zipcodeObject) {
                this.populateCallback(zipcodeObject);
            }
        })

	}
	render() {
		return (
			<div className={"form-group " + (this.state.isValid ? "" : "has-error has-danger")}>
				<label>{this.props.label}</label>
				<input className="form-control" type="number" onChange={this.handleFn} value={this.props.model} onBlur={this.checkValid} disabled={this.props.disabled}/>
				{
					this.state.isValid !== false ? <div></div>
						:<div
                            className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
                            <ul className="list-unstyled">
                                <li>{this.props.errorLabel}</li>
                            </ul>
                        </div>
				}
			</div>
		);
	}
}