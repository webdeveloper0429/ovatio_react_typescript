import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['form'], {wait: true})
export default class CustomInputRate extends React.Component<any, any> {
	validationFn;
	onChangeFn;

	constructor(props: any) {
		super();
		this.state = {
			model: (props.model || props.defaultValue || '0') +" %",
			isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
			min: props.min ? props.min : 0,
			max: props.max ? props.max : 100,
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;
        if(props.defaultValue) this.onChangeFn(props.defaultValue);

		this.handleFn = this.handleFn.bind(this);
		this.handleChangeFn = this.handleChangeFn.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			model: nextProps.model ? +nextProps.model + " %" : "0 %",
			isValid: this.validationFn ? this.validationFn(nextProps.name, nextProps.model) : true,
            min: nextProps.min ? nextProps.min : 0,
            max: nextProps.max ? nextProps.max : 100,
		});
		this.onChangeFn = nextProps.onChangeFn;
	}

	handleFn(event: any) {
		let value = event.target.value;
		let newValue = parseFloat(value.replace(/,/g, '.'));
		newValue = newValue < this.state.min ? this.state.min : newValue;
		newValue = newValue > this.state.max ? this.state.max : newValue;
		if (!newValue) {
			newValue = 0;
		}
		let displayValue = newValue + " %";
		event.preventDefault();
		this.setState({
			model: displayValue,
			isValid: this.validationFn ? this.validationFn(this.props.name, event.target.value) : true
		});
		this.onChangeFn(newValue);
	}

	handleChangeFn(event: any) {
		this.setState({
			model: event.target.value
		})
	}

	render() {
		return (
			<div>
				{
					this.props.hasNoLabel === true ? null : <label>{this.props.label}</label>
				}
				<input className="form-control" type="text"
					   onBlur={this.handleFn}
					   onChange={this.handleChangeFn}
					   value={this.state.model}
					   disabled={this.props.disabled}
				/>
			</div>
		);
	}
}