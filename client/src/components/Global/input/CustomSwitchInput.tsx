import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';

@translate(['form'], {wait: true})
export default class CustomSwitchInput extends React.Component<any, any> {

    onChangeFn;
    value1Label;
    value1;
    value2Label;
    value2;

    constructor(props: any) {

        super();
        this.state = {
            model: props.model,
            value1Class:null,
            value2Class:null,
        };
        this.onChangeFn = props.onChangeFn;
        this.value1Label = props.value1Label;
        this.value1 = props.value1;
        this.value2Label = props.value2Label;
        this.value2 = props.value2;

        this.handleFn = this.handleFn.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            model: nextProps.model == nextProps.value1 ? nextProps.value2 : nextProps.value1,
            value1Class: nextProps.model == nextProps.value1 ? 'selected' : 'unselected',
            value2Class: nextProps.model == nextProps.value2 ? 'selected' : 'unselected',
        });
        this.onChangeFn = nextProps.onChangeFn;
    }

    handleFn(event: any) {
        this.setState({
            model: this.state.model == this.state.value1 ? this.state.value2 : this.state.value1,
            value1Class: this.state.model == this.state.value1 ? 'selected' : 'unselected',
            value2Class: this.state.model == this.state.value2 ? 'selected' : 'unselected',
        });
        this.onChangeFn(this.state.model);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-12 text-center">
                    <div className="btn-group" role="group">
                        <label className="option1"><span className={this.state.value1Class}>{this.value1Label}</span></label>
                        <label className="switch">
                            <input type="checkbox" onChange={this.handleFn} checked={this.state.model == this.value1}/>
                                <span className="slider"></span>
                        </label>
                        <label className="option2"><span className={this.state.value2Class}>{this.value2Label}</span></label>
                    </div>
                </div>
            </div>
        );
    }
}