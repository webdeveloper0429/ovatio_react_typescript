import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


@translate(['form'], {wait: true})
export default class CustomInputNumber extends React.Component<any, any> {
	validationFn;
	onChangeFn;

	constructor(props: any) {
		super();
		this.state = {
			isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
			value: props.model || props.defaultValue || '',
		};
		this.validationFn = props.validationFn;
		this.onChangeFn = props.onChangeFn;

		if(props.defaultValue) this.onChangeFn(props.defaultValue);

		this.handleFn = this.handleFn.bind(this);
	}

	componentWillReceiveProps(nextProps) {
        this.setState({
            isValid: this.validationFn ? this.validationFn(nextProps.name, nextProps.model) : true,
            value: nextProps.model,
        });
		this.onChangeFn = nextProps.onChangeFn;
	}

	handleFn(event: any) {
		event.preventDefault();
		this.setState({
			isValid: this.validationFn ? this.validationFn(this.props.name, event.target.value) : true,
            value: event.target.value,
		});
		this.onChangeFn(event.target.value);
	}

	render() {
		return (
			<div className={(this.props.hasFormGroup === false ? "" : "form-group") + (this.state.isValid ? "" : "has-error has-danger")}>
				{
					this.props.hasNoLabel === true ? null : <label>{this.props.label}</label>
				}
				<input className="form-control" type="number"
					onChange={this.handleFn} 
					value={this.state.value}
					disabled={this.props.disabled}
					max={this.props.max || ''}
					min={this.props.min || ''}
				/>
				{
					this.state.isValid !== false ? <div></div>
						:<div
						className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
						<ul className="list-unstyled">
							<li>{this.props.errorLabel}</li>
						</ul>
					</div>
				}
			</div>
		);
	}
}