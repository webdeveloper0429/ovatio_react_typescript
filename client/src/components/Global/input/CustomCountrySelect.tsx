import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {CountryService} from "../../../services/country";


@translate(['form', 'countries'], {wait: true})
export default class CustomCountrySelect extends React.Component<any, any> {
    validationFn;
    onChangeFn;
    countryList;

    constructor(props: any) {
        super();
        this.state = {
            isValid:  props.validationFn ? props.validationFn(props.name, props.model) : true,
            countryList: props.countryList ? props.countryList : []
        };
        this.validationFn = props.validationFn;
        this.onChangeFn = props.onChangeFn;

        if(!this.state.countryList.length) {
            CountryService._countries.then((countries:any[]) => {
                countries.forEach((country) => this.state.countryList.push({
                    'value': country.id,
                    'label': country.label
                }))

                this.setState({
                    isValid: this.state.isValid,
                    countryList: this.state.countryList,
                })
            });
        }

        this.handleFn = this.handleFn.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(this.validationFn){
            this.setState({
                isValid: this.validationFn(nextProps.name, nextProps.model),
                countryList: nextProps.countryList ? nextProps.countryList : this.state.countryList
            });
        }
        this.onChangeFn = nextProps.onChangeFn;
    }

    handleFn(event: any) {
        event.preventDefault();
        this.setState({
            isValid: this.validationFn ? this.validationFn(this.props.name, event.target.value) : true
        });
        if(this.onChangeFn){
            this.onChangeFn(event.target.value);
        }
    }

    render() {
        const {t} : any = this.props;
        let optionListDOM = this.state.countryList.map(_option => {
            return (
                <option key={_option.value} value={_option.value}>
                    {t(_option.label)}
                </option>
            )
        });
        return (
            <div className={"form-group " + (this.state.isValid ? "" : "has-error has-danger")}>
                <label>{this.props.label}</label>
                <select className="form-control"
                        disabled={this.props.disabled}
                        value={this.props.model}
                        onChange={this.handleFn}>

                    <option value=""></option>
                    {optionListDOM}
                </select>
                {
                    this.state.isValid !== false ? <div></div>
                        :<div
                            className={"help-block form-text " + (this.state.isValid ? "" : "with-errors") + " form-control-feedback"}>
                            <ul className="list-unstyled">
                                <li>{this.props.errorLabel}</li>
                            </ul>
                        </div>
                }
            </div>
        );
    }
}