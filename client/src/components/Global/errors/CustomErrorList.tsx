import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';


//todo: add translations if needed
@translate(['businessProvider'], {wait: true})
export default class CustomErrorList extends React.Component<any, any> {

    constructor(props: any) {

        super();
        this.state = {
            errors:props.errors
        };

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            errors:nextProps.errors,
        });
    }

    render() {
        const {t} : any = this.props;
        return (
            <div className="row" style={{margin: '10px'}}>
                {
                    this.state.errors.map((errorLabel, i) =>
                        <div className="col-sm-12" key={i} style={{border: '1px solid red', padding: '10px', 'text-align': 'center', 'background-color': 'lightcoral'}}>
                            <p style={{'font-size':'large'}}><span className="glyphicons glyphicons-warning-sign"></span> {t(errorLabel)}</p>
                        </div>
                    )
                }
            </div>
        );
    }
}