import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import Modal from 'react-modal';
import FontAwesome from "react-fontawesome";
import CustomInputSelect from "../input/CustomInputSelect";
import CustomInputText from "../input/CustomInputText";
import CustomInputNumber from "../input/CustomInputNumber";
import CustomInputZipCode from "../input/CustomInputZipCode";
import {User} from "./../../../models/UserModel";


@translate(['user', 'form','error'], {wait: true})
export default class CustomInputRate extends React.Component<any, any> {

	constructor(props: any) {
		super();
		this.onCreateUser = this.onCreateUser.bind(this);
		this.onEditUser = this.onEditUser.bind(this);
		this.onDeleteUser = this.onDeleteUser.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.saveUser = this.saveUser.bind(this);
		this.state = {
			showModal: false,
			helperData:{
			},
			customStyles: {
				content: {
					margin: 'auto',
					maxWidth : '1000px',
					marginTop: '33vh',
					transform: 'translateY(-33%)'
				},
				overlay : {
					position          : 'fixed',
					top               : 0,
					left              : 0,
					right             : 0,
					bottom            : 0,
					backgroundColor   : 'rgba(0, 0, 0, 0.75)'
				}
			}
		};
	}

	onCreateUser(e) {
		e.preventDefault();
		var u = new User();
		this.setState({
			mode: 'create',
			showModal: true,
			currentUser: u
		});
	}

	onEditUser(user) {
		var u = new User();
		u.hydrate(user);
		this.setState({
			mode: 'edit',
			showModal: true,
			index: this.props.users.indexOf(user),
			currentUser: u
		});
	}

	onDeleteUser(user) {
		if(user.isDeleted === true){
			user.isDeleted = false;
		} else{
			user.isDeleted = true;
		}
		this.forceUpdate();
	}

	saveUser() {
		if (this.state.mode === "create") {
			if (this.props.users) {
				this.props.users.push(this.state.currentUser);
			}
			this.setState({
				mode: null,
				showModal: false,
				selected: null,
				currentUser: null,
			});
		} else if (this.state.mode === "edit") {
			if(this.props.users[this.state.index]){
				this.props.users[this.state.index] = this.state.currentUser;
			}
			this.setState({
				mode: null,
				showModal: false,
				selected: null,
				currentUser: null,
			});
		}
	}

	closeModal() {
		this.setState({
			mode: null,
			showModal: false,
			selected: null,
			currentUser: null,
		});
	}

	setUser(attribut, value) {
		this.state.currentUser[attribut] = value;
		this.setState({currentUser: this.state.currentUser});
	}

	render() {
		const {t} : any = this.props;
		return (
			<div>
				<div>
					<div className="controls-above-table">
						<div className="row">
							<div className="col-sm-6">
								<button className="btn btn-sm btn-primary" onClick={(e) => this.onCreateUser(e)}>
									<FontAwesome name="plus-square"/>&nbsp;&nbsp;{t('user:addUser')}</button>
							</div>
						</div>
					</div>
					<div className="table-responsive">
						<table className="table table-striped">
							<thead>
							<tr>
								<th>{t('form:firstName')}</th>
								<th>{t('form:lastName')}</th>
								<th>{t('form:email')}</th>
								<th>{t('form:phone')}</th>
								<th>{t('common:edit')}</th>
								<th>{t('common:delete')}</th>
							</tr>
							</thead>
							<tbody>
							{
								this.props.users ? this.props.users.map((user, index) =>
									<tr key={index} className={ (user.isDeleted == true ? "userIsDeleted" : "")}>
										<td>{user.firstName}</td>
										<td>{user.lastName}</td>
										<td>{user.email}</td>
										<td>{user.phone}</td>
										<td>
											<a title={t('common:edit')} onClick={(e) => this.onEditUser(user)}><i className="os-icon os-icon-pencil-2"></i></a>
										</td>
										<td>
											<a title={t('common:delete')} onClick={(e) => this.onDeleteUser(user)}><i className="os-icon os-icon-ui-15"></i></a>
										</td>
									</tr>
								) : null
							}
							</tbody>
						</table>
					</div>
				</div>
				<Modal isOpen={this.state.showModal} style={this.state.customStyles} contentLabel="Modal"
					   onRequestClose={this.closeModal}>
					<div>
						<h1>{ t("user:editUserPageTitle") }</h1>
						{this.state.currentUser ?

							<form>
								<div className="row">
									<div className="col-md-4">
										<CustomInputSelect
											name="genre"
											label={t('form:genre')}
											onChangeFn={(newValue) => this.setUser("genre", newValue)}
											model={this.state.currentUser.genre}
											optionList={[
													{
														value: "1",
														label: t('form:title-madam')
													},
													{
														value: "2",
														label: t('form:title-sir')
													}
												]}
										></CustomInputSelect>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="firstName"
											label={t('form:firstName')}
											validationFn={this.state.currentUser.validationFn}
											onChangeFn={(newValue) => this.setUser("firstName", newValue)}
											model={this.state.currentUser.firstName}
											errorLabel={t('form:firstNameError')}
										></CustomInputText>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="lastName"
											label={t('form:lastName')}
											validationFn={this.state.currentUser.validationFn}
											onChangeFn={(newValue) => this.setUser("lastName", newValue)}
											model={this.state.currentUser.lastName}
											errorLabel={t('form:lastNameError')}
										></CustomInputText>
									</div>

									<div className="col-md-4">
										<CustomInputText
											name="address"
											label={t('form:address')}
											onChangeFn={(newValue) => this.setUser("address", newValue)}
											model={this.state.currentUser.address}
										></CustomInputText>
									</div>
									<div className="col-md-4">
										<CustomInputZipCode
											name="zipCode"
											label={t('form:zipCode')}
											onChangeFn={(newValue) => this.setUser("zipCode", newValue)}
											model={this.state.currentUser.zipCode}
											errorLabel={t('form:zipCodeError')}
										></CustomInputZipCode>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="city"
											label={t('form:city')}
											validationFn={this.state.currentUser.validationFn}
											onChangeFn={(newValue) => this.setUser("city", newValue)}
											model={this.state.currentUser.city}
											errorLabel={t('form:cityError')}
										></CustomInputText>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="email"
											label={t('form:email')}
											validationFn={this.state.currentUser.validationFn}
											onChangeFn={(newValue) => this.setUser("email", newValue)}
											model={this.state.currentUser.email}
											errorLabel={t('form:emailError')}
										></CustomInputText>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="phone"
											label={t('form:phone')}
											validationFn={this.state.currentUser.validationFn.bind(this.state.currentUser)}
											onChangeFn={(newValue) => this.setUser("phone", newValue)}
											model={this.state.currentUser.phone}
											errorLabel={t('form:phoneError')}
										></CustomInputText>
									</div>
									<div className="col-md-4">
										<CustomInputText
											name="mobilePhone"
											label={t('form:mobilePhone')}
											validationFn={this.state.currentUser.validationFn.bind(this.state.currentUser)}
											onChangeFn={(newValue) => this.setUser("mobilePhone", newValue)}
											model={this.state.currentUser.mobilePhone}
											errorLabel={t('form:phoneError')}
										></CustomInputText>
									</div>
								</div>
							</form>
							: null }

						<div className="modal-footer">
							<button className="btn btn-secondary" type="button"
									onClick={this.closeModal}> {t('common:cancel')}</button>
							<button onClick={this.saveUser} className="btn btn-primary"
									type="button" disabled={(this.state.currentUser && this.state.currentUser.validateObjectModal()) ? false : true}>{t('common:save')}</button>
						</div>
					</div>
				</Modal>
			</div>

		);
	}
}
