import * as React from 'react';
import {translate, Interpolate, Trans} from 'react-i18next';
import { Modal, Button } from 'react-bootstrap';
import { confirmable, createConfirmation } from 'react-confirm';
import $ from 'jquery';
declare var $: any;

class Confirmation extends React.Component<any, any> {
	constructor(props: any){
		super();
	}
	componentDidMount(){
		if(this.props.show) $(this.refs.confirmModal).modal('show');
		else $(this.refs.confirmModal).modal('hide');
	}
	componentWillReceiveProps(nextProps){
		if(nextProps.show) $(this.refs.confirmModal).modal('show');
		else $(this.refs.confirmModal).modal('hide');
	}
	render() {
		const {
			okLabel,
			cancelLabel,
			title,
			confirmation,
			show,
			proceed,
			dismiss,
			cancel,
			enableEscape = true,
		} = this.props;
		return (
			<div>
				<div aria-hidden="true" ref="confirmModal" aria-labelledby="deleteModalLabel" className="modal fade" id="deleteConfirmModal" role="dialog">
					<div className="modal-dialog" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title" id="deleteConfirmModal">
									{title}
								</h5>
								<button aria-label="Close" className="close" type="button" onClick={cancel}><span aria-hidden="true"> &times;</span></button>
							</div>
							<div className="modal-body">
								<p>{confirmation}</p>
							</div>
							<div className="modal-footer">
								<button className="btn btn-secondary"  type="button" onClick={cancel}>{cancelLabel}</button>
								<button className="btn btn-danger" type="button" onClick={proceed} >{okLabel}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const DefaultConfirmation = createConfirmation(confirmable(Confirmation));

export function defaultConfirm( options = {}) {
  return DefaultConfirmation(options);
}
