import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import $ from 'jquery';
declare var $: any;


@translate(['modal'], {wait: true})
export default class DeleteConfirmModal extends React.Component<any, any> {

	constructor(props: any) {
		super();
		this.state = {
			show: false
		};
	}
	componentDidMount() {
		this.props.onRef(this)
	}
	componentWillUnmount() {
		this.props.onRef(undefined)
	}
	openModal(){
		$(this.refs.confirmModal).modal('show');
	}
	closeModal(){
		$(this.refs.confirmModal).modal('hide');
	}

	handleConfirm(val){
		this.props.onConfirm(val);
		if(val) $(this.refs.confirmModal).modal('hide');
	}

	render() {
		const {t} : any = this.props;
		return (
			<div>
				<div aria-hidden="true" ref="confirmModal" aria-labelledby="deleteModalLabel" className="modal fade" id="deleteConfirmModal" role="dialog">
					<div className="modal-dialog" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title" id="deleteConfirmModal">
									{t('modal:areYouSure')}
								</h5>
								<button aria-label="Close" className="close" data-dismiss="modal" type="button" onClick={()=>{ this.handleConfirm(false) }}><span aria-hidden="true"> &times;</span></button>
							</div>
							<div className="modal-body">
								<p>{t('modal:doYouWantToDelete')}</p>
							</div>
							<div className="modal-footer">
								<button className="btn btn-secondary" data-dismiss="modal" type="button" onClick={()=>{ this.handleConfirm(false) }}>{t('modal:cancel')}</button>
								<button className="btn btn-danger" type="button" onClick={()=>{ this.handleConfirm(true) }} >{t('modal:delete')}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}