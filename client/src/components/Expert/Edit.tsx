import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {Link, browserHistory} from 'react-router';
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomCountrySelect from './../Global/input/CustomCountrySelect';
import {Expert} from './../../models/ExpertModel'
import CustomTitle from "../Global/title/CustomTitle";
import AddUser from "../Global/addUser/AddUser";
import {ExpertService} from "./../../services/expert";
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import {UserService} from '../../services/user';
import notification from '../../lib/reactToastify';
import {ZipcodeService} from "./../../services/zipcode";
import { toast } from 'react-toastify';


@translate(['expert', 'form','countries', 'toast'], {wait: true})
export class ExpertEdit extends React.Component<any, any> {

	public state: any;
	public countries: any;

	constructor(props: any) {
		super(props);
		this.state = {
			canSubmit: false,
			expert: new Expert(),
		};

		this.setExpert = this.setExpert.bind(this);
		this.checkForm = this.checkForm.bind(this);
		this.getEditDataIfEditMode = this.getEditDataIfEditMode.bind(this);
		this.save = this.save.bind(this);
		this.getEditDataIfEditMode();

		UserService.getMe().then(
			res => {
				// console.log(res.data);
				this.setState({
					canEditState: UserService.isAdmin(res.data)
				})
			}
		);


	}

	getEditDataIfEditMode() {
		if (this.props.params.id) {
			ExpertService.getById(this.props.params.id).then(resp => {
				let row = resp.data;
				this.state.expert.hydrate(row || {});
				this.setState({
					expert: this.state.expert
				})
			})
		}
	}

	save(event: any) {
		event.preventDefault();
		if(this.state.canSubmit){
            let promise:any;
			if(this.props.params.id){
                promise = ExpertService.update(this.props.params.id, {expert:this.state.expert}).then(
					(resp)=>{
						browserHistory.push('/expert')
					}
				);
			}
			else{
                promise = ExpertService.insert({expert:this.state.expert}).then(
					(resp)=>{
						browserHistory.push('/expert')
					}
				);
			}
            promise.then(() => toast.success(this.props.t('toaster:saveSuccess'))).catch(() => toast.error(this.props.t('toaster:error')))
		}
	}

	setExpert(attribut, value) {

		this.state.expert[attribut] = value;
		this.setState({expert: this.state.expert});

	}

	checkForm() {

		this.state.canSubmit = this.state.expert.validateObject();

	}

	render() {
		const {t} : any = this.props;
		this.checkForm();
		return (
			<div className="content-w">
				<div className="content-i">
					<div className="content-box">
						<div className="row">
							<div className="col-sm-12">
								<div className="element-wrapper">
									<h6 className="element-header">
										{
											(this.props.params.id ? t('expert:editExpertPageTitle')
												: t('expert:createExpertPageTitle'))
										}
									</h6>
									<div className="element-box">
										<form id="formValidate">
											<div className="row">
												<div className="col-sm-4">
													<CustomInputText
														name="businessName"
														label={t('form:businessName')}
														validationFn={this.state.expert.validationFn}
														onChangeFn={(newValue) => this.setExpert("businessName", newValue)}
														model={this.state.expert.businessName}
														errorLabel={t('form:businessNameError')}
													></CustomInputText>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-4">
													<CustomInputText
														name="adress"
														label={t('form:address')}
														onChangeFn={(newValue) => this.setExpert("address1", newValue)}
														model={this.state.expert.address1}
													></CustomInputText>
												</div>
												<div className="col-sm-4">
													<CustomInputText
														name="adress"
														label={t('form:address2')}
														onChangeFn={(newValue) => this.setExpert("address2", newValue)}
														model={this.state.expert.address2}
													></CustomInputText>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-4">
													<CustomInputText
														name="city"
														label={t('form:city')}
														onChangeFn={(newValue) => this.setExpert("city", newValue)}
														model={this.state.expert.city}
													></CustomInputText>
												</div>
												<div className="col-sm-4">
													<CustomInputZipCode
														name="zipCode"
														label={t('form:zipCode')}
														onChangeFn={(newValue) => this.setExpert("zipCode", newValue)}
														model={this.state.expert.zipCode}
														errorLabel="Error Zipcode"
													></CustomInputZipCode>
												</div>
												<div className="col-sm-4">
													<CustomCountrySelect
														name="country"
														label={t('form:country')}
														onChangeFn={(newValue) => this.setExpert("country", newValue)}
														model={this.state.expert.country}
													></CustomCountrySelect>
												</div>
											</div>
											<div className="row">
												<div className="col-sm-4">
													<CustomInputText
														name="switchboardPhone"
														label={t('form:switchboardPhone')}
														validationFn={this.state.expert.validationFn}
														onChangeFn={(newValue) => this.setExpert("switchboardPhone", newValue)}
														errorLabel={t('expert:switchboardPhoneError')}
														model={this.state.expert.switchboardPhone}
													></CustomInputText>
												</div>
												<div className="col-sm-4">
													<CustomInputText
														name="email"
														label={t('form:email')}
														validationFn={this.state.expert.validationFn}
														onChangeFn={(newValue) => this.setExpert("email", newValue)}
														model={this.state.expert.email}
													></CustomInputText>
												</div>
											</div>
											<CustomTitle label={t('form:contacts')}></CustomTitle>
											<AddUser
												users={this.state.expert.contacts}
											></AddUser>
											<div className="form-buttons-w">
												<button
													className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')}
													type="button" onClick={this.save}>Enregistrer
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
