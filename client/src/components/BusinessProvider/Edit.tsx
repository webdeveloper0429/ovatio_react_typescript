import * as React from "react";
import axios, {AxiosError} from 'axios';
import {translate, Interpolate, Trans} from 'react-i18next';
import {browserHistory} from 'react-router';
import {BusinessProviderService} from "../../services/businessProvider";
import {BusinessProvider} from "../../models/BusinessProviderModel";
import CustomInputText from './../Global/input/CustomInputText';
import CustomInputSelect from './../Global/input/CustomInputSelect';
import CustomSwitchInput from './../Global/input/CustomSwitchInput';
import CustomErrorList from './../Global/errors/CustomErrorList';
import CustomInputZipCode from "../Global/input/CustomInputZipCode";
import CustomInputCity from "../Global/input/CustomInputCity";
import CustomCountrySelect from "../Global/input/CustomCountrySelect";
import CustomTitle from "../Global/title/CustomTitle";
import DateBlock from "../Global/title/dateBlock";
import AddUser from "../Global/addUser/AddUser";
import { toast } from 'react-toastify';

export interface BusinessProviderEditProps {
	t:any;
	params: any;
}

@translate(['businessProvider', 'common', 'form', 'countries', 'toaster'], {wait: true})
export class BusinessProviderEdit extends React.Component<BusinessProviderEditProps, any> {
	public state: any;
	public countries: any;
	public nameConstant: any;
	public serverErrors: any;

	constructor(props: BusinessProviderEditProps) {
		super(props);
		this.checkForm = this.checkForm.bind(this);
		this.save = this.save.bind(this);
		axios.defaults.baseURL = '/api/businessProviders';
		this.nameConstant = {
			professional: 'professional',
			particular: 'particular'
		}
		this.state = {
			canSubmit: false,
			redirect: false,
			businessProvider: new BusinessProvider(),
			helperData:{
				countryListArr: []
			},
			errors: []
		};
		this.serverErrors = {
			email:'duplicateEmailError',
			siret:'duplicateSiretError',
		};

	}

	componentDidMount(){
		this.getEditDataIfEditMode();
	}

	checkForm() {
		this.state.canSubmit = this.state.businessProvider.validateObject();
	}

	setBusinessProvider(attr, val){
		this.state.businessProvider[attr] = val;
		this.setState({ businessProvider: this.state.businessProvider });
	}

    resetErrorFields() {
		this.state.errors = [];
        this.setState({errors: this.state.errors});
	}

	save(event: any) {
		event.preventDefault();
		this.resetErrorFields();
		if(this.state.canSubmit){
			let submitPromise = null;
			if(this.props.params.id){
                submitPromise = BusinessProviderService.update(this.props.params.id, this.state.businessProvider).then(
					(resp)=>{
						browserHistory.push('/businessProvider')
					}
				);
			}
			else{
                submitPromise = BusinessProviderService.insert(this.state.businessProvider).then(
					(resp)=>{
						browserHistory.push('/businessProvider')
					}
				);
			}
            submitPromise.then(() => toast.success(this.props.t("toaster:saveSuccess"))
				.catch((error:AxiosError) => {
            	if(error.response.status === 403) {
            		this.state.errors.push(this.serverErrors[error.response.data.errorField]);
                    this.setState({errors: this.state.errors});

				} else {
                    toast.error(this.props.t("toaster:error"))
				}
			}))
		}	
	}

	getEditDataIfEditMode() {
		if (this.props.params.id) {
			BusinessProviderService.getById(this.props.params.id).then(
				resp => {
					let row = resp.data;
					this.state.businessProvider.hydrate(row);
					this.setState({businessProvider: this.state.businessProvider});
				}
			)
		}
	}

	render() {
		const {t} : any = this.props;
		this.checkForm()
		return (
			<div className="content-w">
				<div className="content-i">
					<div className="content-box">
						<div className="row">
							<div className="col-sm-12">
								<div className="element-wrapper">
									<h6 className="element-header">
										{
											(this.props.params.id ? t('businessProvider:editBusinessProviderPageTitle')
												: t('businessProvider:createBusinessProviderPageTitle'))
										}
									</h6>
									<div className="element-box">

										<form id="formValidate">


											<div className="row">
												<div className="col-sm-12">
													<CustomErrorList errors={this.state.errors}></CustomErrorList>
													<CustomSwitchInput name="type"
																		   model={this.state.businessProvider.type}
																		   value1={this.nameConstant.professional}
																		   value1Label={t('businessProvider:professional')}
																		   value2={this.nameConstant.particular}
																		   value2Label={t('businessProvider:particular')}
																		   onChangeFn={(newValue) => this.setBusinessProvider("type", newValue)}
													></CustomSwitchInput>
													<hr/>
													{
														this.state.businessProvider.type === 'professional' ?
															<div className="row">
																<div className="col-sm-6">
																	<CustomInputText
																		name="businessName"
																		label={t('businessProvider:company')}
																		validationFn={this.state.businessProvider.validationFn}
																		onChangeFn={(newValue) => this.setBusinessProvider("businessName", newValue)}
																		model={this.state.businessProvider.businessName}
																		errorLabel={t('businessProvider:businessNameError')}
																	></CustomInputText>
																</div>
																<div className="col-sm-6">
																	<CustomInputText
																		name="siret"
																		label={t('businessProvider:siret')}
																		validationFn={this.state.businessProvider.validationFn}
																		onChangeFn={(newValue) => this.setBusinessProvider("siret", newValue)}
																		model={this.state.businessProvider.siret}
																		errorLabel={t('businessProvider:siretError')}
																	></CustomInputText>	
																</div>
																<div className="col-sm-6">
																	<CustomInputText
																		name="orias"
																		label={t('businessProvider:orias')}
																		validationFn={this.state.businessProvider.validationFn}
																		onChangeFn={(newValue) => this.setBusinessProvider("orias", newValue)}
																		model={this.state.businessProvider.orias}
																		errorLabel={t('businessProvider:oriasError')}
																	></CustomInputText>
																</div>
															</div>
															:
															<div className="row">
																<div className="col-sm-6">
																	<CustomInputText
																		name="businessProviderLastName"
																		label={t('businessProvider:businessProviderLastName')}
																		validationFn={this.state.businessProvider.validationFn}
																		onChangeFn={(newValue) => this.setBusinessProvider("businessProviderLastName", newValue)}
																		model={this.state.businessProvider.businessProviderLastName}
																		errorLabel={t('businessProvider:businessProviderLastNameError')}
																	></CustomInputText>
																</div>
																<div className="col-sm-6">
																	<CustomInputText
																		name="businessProviderFirstName"
																		label={t('businessProvider:businessProviderFirstName')}
																		validationFn={this.state.businessProvider.validationFn}
																		onChangeFn={(newValue) => this.setBusinessProvider("businessProviderFirstName", newValue)}
																		model={this.state.businessProvider.businessProviderFirstName}
																		errorLabel={t('businessProvider:businessProviderFirstNameError')}
																	></CustomInputText>
																</div>
															</div>
													}
													<div className="row">
														<div className="col-sm-6">
															<CustomInputSelect
																name="status"
																label={t('businessProvider:status')}
																validationFn={this.state.businessProvider.validationFn}
																onChangeFn={(newValue) => this.setBusinessProvider("status", newValue)}
																model={this.state.businessProvider.status}
																errorLabel={t('businessProvider:statusError')}
																optionList={[
																		{
																			value: 'inProgress',
																			label: t('businessProvider:inProgress')
																		},{
																			value: 'prospect',
																			label: t('businessProvider:prospect')
																		},{
																			value: 'terminated',
																			label: t('businessProvider:terminated')
																		}
																	]}
															></CustomInputSelect>
														</div>
														<div className="col-sm-6">
															<CustomInputSelect
																name="commissionPercent"
																label={t('businessProvider:commissionPercent')}
																onChangeFn={(newValue) => this.setBusinessProvider("commissionPercent", newValue)}
																model={this.state.businessProvider.commissionPercent}
																optionList={[
																		{
																			value: '30',
																			label: '30 %'
																		},{
																			value: '50',
																			label: '50 %'
																		}
																	]}
															></CustomInputSelect>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="address"
																label={t('form:address')}
																onChangeFn={(newValue) => this.setBusinessProvider("address", newValue)}
																model={this.state.businessProvider.address}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="address2"
																label={t('form:address2')}
																onChangeFn={(newValue) => this.setBusinessProvider("address2", newValue)}
																model={this.state.businessProvider.address2}
															></CustomInputText>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputCity
																name="city"
																label={t('common:city')}
																onChangeFn={(newValue) => this.setBusinessProvider("city", newValue)}
																model={this.state.businessProvider.city}
																country={this.state.businessProvider.country}
																populateCallback={zipcodeObject => this.setBusinessProvider("zipCode", zipcodeObject.cp)}
																errorLabel={t('businessProvider:cityError')}
															></CustomInputCity>
														</div>
														<div className="col-sm-6">
															<CustomInputZipCode
																name="zipCode"
																label={t('common:postalCode')}
																onChangeFn={(newValue) => this.setBusinessProvider("zipCode", newValue)}
																populateCallback={zipcodeObject => this.setBusinessProvider("city", zipcodeObject.ville)}
																country={this.state.businessProvider.country}
																model={this.state.businessProvider.zipCode}
																errorLabel={t('common:postalCodeError')}
															></CustomInputZipCode>
														</div>
														<div className="col-sm-6">
															<CustomCountrySelect
																name="country"
																label={t('common:country')}
																onChangeFn={(newValue) => this.setBusinessProvider("countryId", newValue)}
																model={this.state.businessProvider.countryId}
															></CustomCountrySelect>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<CustomInputText
																name="email"
																label={t('form:email')}
																validationFn={this.state.businessProvider.validationFn}
																onChangeFn={(newValue) => {this.setBusinessProvider("email", newValue);}}
																model={this.state.businessProvider.email}
																errorLabel={t('form:emailError')}
															></CustomInputText>
														</div>
														<div className="col-sm-6">
															<CustomInputText
																name="phone"
																label={t('form:phone')}
																validationFn={this.state.businessProvider.validationFn}
																onChangeFn={(newValue) => this.setBusinessProvider("phone", newValue)}
																model={this.state.businessProvider.phone}
																errorLabel={t('form:phoneError')}
															></CustomInputText>
														</div>
													</div>
												</div>
											</div>

											<CustomTitle label={t('form:contacts')}></CustomTitle>
											<AddUser
												users={this.state.businessProvider.contacts}
											></AddUser>

											<div className="form-buttons-w">
												<button
													className={"btn btn-primary " + (this.state.canSubmit ? '' : 'disabled')}
													type="button" onClick={this.save}>{t('common:save')}
												</button>
												<DateBlock deletedAt={this.state.businessProvider.deletedAt}></DateBlock>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
