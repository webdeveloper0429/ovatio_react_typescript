import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {Link, browserHistory} from 'react-router';
import {AssureurService} from "../../services/assureur";
import {VersionService} from "../../services/version";
import {ParagraphService} from "../../services/paragraph";
import {ManifestationService} from "../../services/manifestation";
import {UserService} from "../../services/user";
import {ClientService} from "../../services/client";
import {PdfService} from "../../services/pdf";
import {makeTemp} from "./helper";
import notification from '../../lib/reactToastify';
import * as _ from 'lodash';
import {Languages} from "../../enums/languages";
let enPdf = require('../../../static/locales/en/pdf.json');
let frPdf = require('../../../static/locales/fr/pdf.json');


@translate(['template', 'form', 'pdf'], {wait: true})
export class Preview extends React.Component<any, any> {
	constructor(props: any) {
		super(props);
		this.state = {
			manifestation: {},
			domString: '',
			generatedPDFName: '',
			versionLabel: '',
			user: {},
			client: {},
			insurance: {},
			errors: []
		}
        this.props.i18n.addResourceBundle('en', 'pdf', enPdf);
        this.props.i18n.addResourceBundle('fr', 'pdf', frPdf);
		this.sendPdfToOwnerEmail = this.sendPdfToOwnerEmail.bind(this);
		ManifestationService.getOne(this.props.params.manifestation_id).then(res => {
			let manifestation = res.data;

			if(!manifestation.mainInsurerId) this.state.errors.push('insurerError');
			if(!manifestation.subscriptionDeadLine) this.state.errors.push('deadlineError');
			if(!manifestation.budget) this.state.errors.push('budgetError');
			if(!manifestation.eventName) this.state.errors.push('eventError');
			this.setState({	manifestation: manifestation, errors: this.state.errors });

			Promise.all([
				UserService.findUsers({id: manifestation.accountManagerId}).then(
					user =>{
						this.setState({user: user.data[0]});
					}
				),
				ClientService.getOneClient(manifestation.clientId).then(resp=>{
					if(!resp.data || !resp.data[0].businessName) this.state.errors.push('clientError');
					this.setState({client: resp.data[0], errors: this.state.errors});
				}),
				AssureurService.getAssurreurById(manifestation.mainInsurerId, true).then(resp=>{
					if((!resp.data.versions.length)) this.state.errors.push('versionError');
					if((resp.data.versions.length && !resp.data.versions[0].paragraphs.length)) this.state.errors.push('paragraphError');
					this.setState({insurance: resp.data, errors: this.state.errors})
				}),
			]).then(() => {
				this.generatePDF();
			})
		});
	}
	generatePDF(){
		if(this.state.errors.length) return;
		let manifestation = this.state.manifestation;
        let lang:Languages;
        switch(manifestation.policyLanguage) {
			case 'english':
				lang = Languages.en;
				break;
			case 'french':
			default:
				lang = Languages.fr;
		}

        let paragraphs = this.state.insurance.versions[0].paragraphs.filter((paragraph) => {
            return paragraph.group == manifestation.sortPDF && paragraph.lang == lang;
        });
		let user = this.state.user;
		let client = this.state.client;
		let insurance = this.state.insurance;

		if(paragraphs.length == 0){
			alert("empty template");
			return false;
		}

		let temp = makeTemp(paragraphs, manifestation, user, client, insurance, this.props.t, lang);
		this.setState({domString: temp});
        PdfService.generatePDF({html: temp, version: this.state.insurance.versions[0].label}).then(
			pdf=>{
				console.log("fileName:", pdf.data);
				this.setState({generatedPDFName: pdf.data});
			}
		);
	}
	sendPdfToOwnerEmail(event) {
		event.preventDefault()
		let manifestation = this.state.manifestation
		ManifestationService.sendByEmail(this.props.params.manifestation_id, this.state.generatedPDFName)
			.then(() => {
				notification({cb: () => {}}, 'Email sent');
			})
	}
	previewButton(){
		if(this.state.generatedPDFName == ''){
			return <button className="btn btn-danger" disabled>{this.props.t('template:generate')}</button>
		}
		else{
			return <Link to={"/api/pdfs?file="+this.state.generatedPDFName} target="_blank"><div className="btn btn-danger">{this.props.t('template:generate')}</div></Link>
		}
	}
	sendByMailButton() {
		if (!this.state.generatedPDFName) {
			return <button className="btn btn-primary" disabled>Email</button>
		}
		return <button className="btn btn-primary" onClick={this.sendPdfToOwnerEmail}>Email</button>
	}
	render(){
		var t = this.props.t;
		return(
			<div className="content-w templateAdd">
				<div className="content-i">
					<div className="content-box">
						<div className="element-wrapper">
							<h6 className="element-header">
								{t('template:templateTitle')}
							</h6>
							<div className="element-box">
								<form>
									<div className="row">
										<div className="col-sm-6 text-center">
                                            {this.previewButton()}
										</div>
										<div className="col-sm-6 text-center">
                                            {this.sendByMailButton()}
										</div>
									</div>
								</form>
							</div>
							<h6 className="element-header">
								{t('template:content')}
							</h6>
							<div className="element-box pdfPreviewContainer">
								{
									(this.state.errors.length) ?
									<div className="alert alert-warning">
										{t('template:toGenerateMandatoryField')}To generate a PDF, you need to define:
										<ul className="projectValidationForEmail">
											{this.state.errors.map((error) => <li>{t(error)}</li>)}
										</ul>
									</div>
									: <div id="pdfBoard" className="" dangerouslySetInnerHTML={{__html: this.state.domString}}></div>
								}
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
