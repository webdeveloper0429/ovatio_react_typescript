import * as React from "react";
import {translate, Interpolate, Trans} from 'react-i18next';
import {Link} from 'react-router';
import {ContractParagraphService} from "../../services/contractParagraph";
import {AssureurService} from "../../services/assureur";
import {ManifestationService} from "../../services/manifestation";
import {UserService} from "../../services/user";
import {VersionService} from "../../services/version";
import {ClientService} from "../../services/client";
import {PdfService} from "../../services/pdf";
import {makeTemp} from "./helper";
import notification from '../../lib/reactToastify';
import {Languages} from "../../enums/languages";
let enPdf = require('../../../static/locales/en/pdf.json');
let frPdf = require('../../../static/locales/fr/pdf.json');

export interface PreviewContractProps {
	params: any;
	t:any;
	i18n:any
}

@translate(['template', 'pdf'], {wait: true})
export class PreviewContract extends React.Component<PreviewContractProps, any> {
	constructor(props: PreviewContractProps) {
		super(props);
		this.state = {
			paragraphs: [],
			manifestation: {},
			domString: '',
			generatedPDFName: '',
			versionLabel: '',
			user: {},
			client: {},
			insurance: {},
			errors: [],
			lang: 'fr',
		}
        this.props.i18n.addResourceBundle('en', 'pdf', enPdf);
        this.props.i18n.addResourceBundle('fr', 'pdf', frPdf);

        this.sendPdfToOwnerEmail = this.sendPdfToOwnerEmail.bind(this)
		let promises = [];

		ManifestationService.getOne(this.props.params.manifestation_id).then(
			manifestation => {
				this.setState({	manifestation: manifestation.data, lang: this.getContractLanguage(manifestation.data) });

				promises.push(ContractParagraphService.find({manifestation_id: this.props.params.manifestation_id}).then(
                    paragraphs => {
                        let data = paragraphs.data.filter((paragraph) => {
                            return paragraph.lang == this.state.lang;
                        })
                        if(!data.length) this.state.errors.push('paragraphsError');
                        this.setState({	paragraphs: data	, errors: this.state.errors});
                    }
                ))

                promises.push(UserService.findUsers({id: manifestation.data.accountManagerId}).then(
					user =>{
						this.setState({user: user.data[0]});
					}
				));
                promises.push(ClientService.getOneClient(manifestation.data.clientId).then(
					resp=>{
						this.setState({
							client: resp.data[0]
						});
					}
				));
                promises.push(AssureurService.getAssurreurById(manifestation.data.mainInsurerId).then(
					resp=>{
						this.setState({
							insurance: resp.data
						})
					}
				));
                promises.push(VersionService.find({id: manifestation.data.contractVersionId}).then(
					version=>{
						this.setState({ versionLabel: version.data[0].label })
					}
				))
                Promise.all(promises).then(() => this.generatePDF())
			}
		);
	}
	generatePDF(){
		let manifestation = this.state.manifestation;
		let paragraphs = this.state.paragraphs;
		let user = this.state.user;
		let client = this.state.client;
		let insurance = this.state.insurance;
		let lang = this.state.lang;


		let temp = makeTemp(paragraphs, manifestation, user, client, insurance, this.props.t, lang);
		this.setState({domString: temp});
		PdfService.generatePDF({html: temp, version: this.state.versionLabel}).then(
			pdf=>{
				console.log("fileName:", pdf.data);
				this.setState({generatedPDFName: pdf.data});
			}
		);
	}
	getContractLanguage(manifestation) {
        let lang:Languages;
        switch(manifestation.policyLanguage) {
            case 'english':
                lang = Languages.en;
                break;
            case 'french':
            default:
                lang = Languages.fr;
        }
        return lang;
	}
	sendPdfToOwnerEmail(event) {
		event.preventDefault()
		ManifestationService.sendByEmail(this.props.params.manifestation_id, this.state.generatedPDFName)
			.then(() => {
				notification({cb: () => {}}, 'Email sent');
			})
	}
	previewButton(){
		if(this.state.generatedPDFName == ''){
			return <button className="btn btn-danger" disabled>Preview</button>
		}
		else{
			return <Link to={"/api/pdfs?file="+this.state.generatedPDFName} target="_blank"><div className="btn btn-danger">Preview</div></Link>
		}
	}
	sendByMailButton() {
		if (!this.state.generatedPDFName) {
			return <button className="btn btn-primary" disabled>Send by mail</button>
		}
		return <button className="btn btn-primary" onClick={this.sendPdfToOwnerEmail}>Send by mail</button>
	}
	render(){
		const { t } : any = this.props;
		return(
			<div className="content-w templateAdd">
				<div className="content-i">
					<div className="content-box">
						<div className="element-wrapper">
							<h6 className="element-header">
								Setting Tempalte
							</h6>
							<div className="element-box">
								<form>
									<div className="row">
										<div className="col-sm-6 text-center">
											{this.previewButton()}												
										</div>
										<div className="col-sm-6 text-center">
											{this.sendByMailButton()}												
										</div>
									</div>
								</form>
							</div>
							<h6 className="element-header">
                                {t('template:content')}
							</h6>
							<div className="element-box pdfPreviewContainer">
                                {
                                    (this.state.errors.length) ?
										<div className="alert alert-warning">
                                            {t('template:toGenerateMandatoryField')}:
											<ul className="projectValidationForEmail">
                                                {this.state.errors.map((error) => <li>{t(error)}</li>)}
											</ul>
										</div>
                                        : <div id="pdfBoard" className="" dangerouslySetInnerHTML={{__html: this.state.domString}}></div>
                                }
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
