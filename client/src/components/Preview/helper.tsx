import { pdfCover } from './pdfCover';
import {categories, types, damageBasises, natureDammages} from '../../listsData/scheduleListsData';
import {Languages} from "../../enums/languages";

export function makeTemp(paragraphs, manifestation, user, client, insurance, t:any, locale:Languages){
	//replace "select" value with local data
	manifestation.category = categories[manifestation.category];
	manifestation.dateType = types[manifestation.dateType];
	//determin pdf sort
	let sortPDF = manifestation.sortPDF;
	//make structure
	let localeT = (text:string) => {
		return t(text, {lng: locale})
	};
	let temp=pdfCover(localeT)[sortPDF];
	let mainNum=0,subNum=0;
	let breakPages = [3,5,6,7,8,13];
	paragraphs.map((paragraph, index)=>{
		//display condition
		switch (typeof(manifestation[paragraph.condition])) {
			case "boolean":
				if(manifestation[paragraph.condition] == false) return false;
				break;
			case "string":
				if(manifestation[paragraph.condition] == "") return false;
				break;
			case "number":
				if(manifestation[paragraph.condition] == 0) return false;
				break;
			case "object":
				if(manifestation[paragraph.condition] == null) return false;
				break;
			default:
				break;
		}
		//display condition end//
		if(paragraph.subLevel == 0){
			mainNum += 1;
			subNum = 0;
			if(breakPages.includes(paragraph.level)){
				temp += `<div style="page-break-after:always;"></div>`;
			}
			temp += `<div class="bigTitle">${mainNum}. ${paragraph.title}</div>`;
			temp += `<div class="content">${paragraph.editor}</div><br/>`;
		}
		else{
			if((paragraph.level == 6) && (paragraph.subLevel == 3)){    //hardcoded
				temp += `<div style="page-break-after:always;"></div>`;
			}
			temp += `<div class="subTitle">${mainNum}.${subNum} ${paragraph.title}</div>`;
			temp += `<div class="content">${paragraph.editor}</div><br/>`;
		}
		subNum += 1;
	});
	//to local string
	for(let key in manifestation){
		if(manifestation.hasOwnProperty(key)){
			if(typeof(manifestation[key]) === 'number'){
				manifestation[key] = manifestation[key].toLocaleString(locale, { minimumFractionDigits: 2 })
			}
		}
	}
	//inject data
	for(let key in manifestation){
		if(manifestation.hasOwnProperty(key)){
			let regex = new RegExp("{{" + key + "}}","g");
			temp = temp.replace(regex, manifestation[key]);
		}
	}
	for(let key in user){
		if(user.hasOwnProperty(key)){
			let regex = new RegExp("{{user." + key + "}}","g");
			temp = temp.replace(regex, user[key]);
		}
	}
	for(let key in client){
		if(client.hasOwnProperty(key)){
			let regex = new RegExp("{{client." + key + "}}","g");
			temp = temp.replace(regex, client[key]);
		}
	}
	
	temp = temp.replace(new RegExp("{{custom.issueDate}}","g"), getCurrentDateString(locale));

	temp = temp.replace(new RegExp("{{insurance.name}}", "g"), insurance.businessName ? insurance.businessName : "[DATA MISSING]");

	temp = temp.replace(/<tr.+\s.+{{custom.amountTable}}[\s\S]*?\/tr>/g, makeAmountTable(manifestation['manifestationDates'], locale));
	//inject data end//
	return temp;
}

function getCurrentDateString(locale){
	let dateObj = new Date();
	let month = dateObj.getUTCMonth() + 1;
	let day = dateObj.getUTCDate();
	let year = dateObj.getUTCFullYear();

	if(locale == 'en') return month + "/" + day + "/" + year;
	return day + "/" + month + "/" + year;

}

function makeAmountTable(dates, locale){
	let tableData = {};
	let trString = '';
	dates.map((date)=>{
		if(!(date.damageBasis in tableData)){
			tableData[date.damageBasis] = {amount: date.amount};//make object if there is no
		}
		else{
			tableData[date.damageBasis].amount += date.amount;//sum amount;
		}
	});

	for(let key in tableData){
		if(tableData.hasOwnProperty(key)){
			trString += `
							<tr class="amountTableTr">
								<td>${natureDammages[key]}</td>
								<td>${damageBasises[key]}</td>
								<td>EUR  ${tableData[key]['amount'].toLocaleString(locale, { minimumFractionDigits: 2 })}</td>
							</tr>
						`;

		}
	}
	// let tableString = `
	// 					<table class="amountTable">
	// 						<thead>
	// 							<th>NATURE DES DOMMAGES</th>
	// 							<th>BASE D'INDEMNISATION</th>
	// 							<th>MONTANT</th>
	// 						</thead>
	// 						<tbody>
	// 							${trString}
	// 						</tbody>
	// 					</table>
	// 				`
	return trString;
}

//<tr[\s\S]*?{{custom.amountTable}}[\s\S]*?<\/tr>
//<tr.+\n.+{{custom.amountTable}}.+\n.+\/tr>
//<tr.+\s.+{{custom.amountTable}}[\s\S]*?\/tr>