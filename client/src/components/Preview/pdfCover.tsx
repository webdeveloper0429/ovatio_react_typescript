export function pdfCover(t) {
    return [
            `<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
<div class="first_page">
	<div class="page_header_div">
		<div class="ticket">
			${t('pdf:assuranceProject')}
		</div>
		<div class="right">
			<div class="black_title">
				${t('pdf:risks')}
			</div>
			<div class="blue_title">
				${t('pdf:cancel')}
			</div>
		</div>
	</div>
	<div class="header_1">
		<div class="ticket">
			${t('pdf:souscriptor')}
		</div>
		<div class="box upper">
			<div class="title_bold">
				{{client.businessName}}
			</div>
			<div class="title_1">
				{{client.address}}
				<br/>
				{{client.postalCode}} {{client.city}}
			</div>
		</div>
	</div>
	<div class="issue_date">
		${t('pdf:creationDate')} : <b>{{custom.issueDate}}</b>
	</div>
	<div class="header_2">
		<div class="ticket">
			${t('pdf:yourProject')}
		</div>
		<div class="title">
			${t('pdf:cancel')}
		</div>
	</div>
	<div class="header_2">
		<div class="ticket">
			${t('pdf:ref')}
		</div>
		<div class="title">
			PROJET N° 2017-ADRC-A00015
		</div>
	</div>
	<div class="indent_1">
		<div class="description">
			<div class="title">
				${t('pdf:conditions')}
			</div>
			<div class="cover_content">
			</div>
		</div>
		<div class="description">
			<div class="title">
				${t('pdf:projectBetween')} :
			</div>
		</div>
		<div class="description">
			<div class="title">
				{{insurance.name}}
			</div>
			<div class="cover_content">
				${t('pdf:named')}
			</div>
		</div>
		<div class="description">
			<div class="title upper">
				{{client.businessName}}
			</div>
		</div>
	</div>
	<div class="header_1">
		<div class="ticket">
			${t('pdf:yourContact')}
		</div>
		<div class="box">
			<div class="title_bold">
				{{user.firstName}} {{user.lastName}}
			</div>
			<div class="title_2">
				{{user.city}}
				<br/>
				{{user.address}}
				<br/>
				${t('pdf:tel')} : {{user.phone}}
				<br/>
				${t('pdf:email')} : {{user.email}}
			</div>
		</div>
	</div>
	<div class="logo">
		<img src="logo.png" width="415" height="216">
	</div>
</div>
<div class="page_header_div">
	<div class="ticket">
		${t('pdf:assuranceProject')}
	</div>
	<div class="right">
		<div class="black_title">
			${t('pdf:risks')}
		</div>
		<div class="blue_title">
			${t('pdf:cancel')}
		</div>
	</div>
</div>
<div class="header_1">
	<div class="ticket">
		${t('pdf:souscriptor')}
	</div>
	<div class="box upper">
		<div class="title_bold">
			{{client.businessName}}
		</div>
		<div class="title_1">
			{{client.address}}
			<br/>
			{{client.postalCode}} {{client.city}}
		</div>
	</div>
</div>
<br/>
<br/>
<br/>`,
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
            `<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
<div class="first_page">
	<div class="page_header_div">
		<div class="ticket">
			${t('pdf:assuranceProject')}
		</div>
		<div class="right">
			<div class="black_title">
				${t('pdf:risks')}
			</div>
			<div class="blue_title">
				${t('pdf:cancel')}
			</div>
		</div>
	</div>
	<div class="header_1">
		<div class="ticket">
			${t('pdf:souscriptor')}
		</div>
		<div class="box upper">
			<div class="title_bold">
				{{client.businessName}}
			</div>
			<div class="title_1">
				{{client.address}}
				<br/>
				{{client.postalCode}} {{client.city}}
			</div>
		</div>
	</div>
	<div class="issue_date">
		${t('pdf:creationDate')} : <b>{{custom.issueDate}}</b>
	</div>
	<div class="header_2">
		<div class="ticket">
			${t('pdf:yourContract')}
		</div>
		<div class="title">
			${t('pdf:cancel')}
		</div>
	</div>
	<div class="header_2">
		<div class="ticket">
			${t('pdf:ref')}
		</div>
		<div class="title">
			CONTRAT N° NM1606583/17-181
		</div>
	</div>
	<div class="indent_1">
		<div class="description">
			<div class="title">
				${t('pdf:contractBetween')} :
			</div>
			<div class="cover_content">
				{{insurance.name}}
			</div>
		</div>
		<div class="description">
			<div class="title">
				${t('pdf:souscriptorOrAssured')} :
			</div>
			<div class="cover_content">
				<ul>
					<li>
						${t('pdf:coverContent1')}
					</li>
					<li>
						${t('pdf:coverContent2')}
					</li>
					<li>
						${t('pdf:coverContent3')}
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="header_1">
		<div class="ticket">
			${t('pdf:yourContact')}
		</div>
		<div class="box">
			<div class="title_bold">
				{{user.firstName}} {{user.lastName}}
			</div>
			<div class="title_2">
				{{user.city}}
				<br/>
				{{user.address}}
				<br/>
				${t('pdf:tel')} : {{user.phone}}
				<br/>
				${t('pdf:email')} : {{user.email}}
			</div>
		</div>
	</div>
	<div class="logo">
		<img src="logo.png" width="415" height="216">
	</div>
</div>
<div class="page_header_div">
	<div class="ticket">
		${t('pdf:assuranceProject')}
	</div>
	<div class="right">
		<div class="black_title">
			${t('pdf:risks')}
		</div>
		<div class="blue_title">
			${t('pdf:cancel')}
		</div>
	</div>
</div>
<div class="header_1">
	<div class="ticket">
		${t('pdf:souscriptor')}
	</div>
	<div class="box upper">
		<div class="title_bold">
			{{client.businessName}}
		</div>
		<div class="title_1">
			{{client.address}}
			<br/>
			{{client.postalCode}} {{client.city}}
		</div>
	</div>
</div>
<br/>
<br/>
<br/>`
        ]
}
