import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { I18nextProvider, translate } from 'react-i18next';
import {Provider} from 'react-redux';
import {Router, browserHistory} from 'react-router';

import createStore from './store';
import createRoutes from './routes';

const store = createStore()
const routes = createRoutes(store)
const MOUNT_NODE = document.getElementById('react-app')


/**
 * IMPORT
 */
import tether from 'tether';
import 'bootstrap/dist/js/bootstrap.js';
import 'bootstrap/dist/css/bootstrap.css';

/**
 * CSS
 */
import '../style/theme.css';
import '../style/pdf.css';
import '../style/themeOverwrite.less';
import '../style/custom.less';


import i18n from './i18n';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import 'font-awesome/css/font-awesome.min.css';

ReactDOM.render(
    <div>
        <I18nextProvider i18n={ i18n }>
            <Provider store={store}>
                <div>
                    <Router history={browserHistory}>
                        {routes}
                    </Router>
                </div>
            </Provider>
        </I18nextProvider>
        <ToastContainer
            position="bottom-right"
            type="default"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop
            closeOnClick
            pauseOnHover
        />
    </div>,
    document.getElementById('react-app')

// initialized i18next instance
);