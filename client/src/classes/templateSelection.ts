import {Languages} from "../enums/languages";

/**
 * A class used to store some parameters of the template/Home component
 */
export class TemplateSelection {
    static lang = Languages.fr;
    static versionIndex = null //todo
}