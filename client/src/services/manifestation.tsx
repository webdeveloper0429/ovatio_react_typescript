import axios from 'axios';

export class ManifestationService {

	static BASE_URL = '/api/manifestations/';

	static getOne(id){
		return axios({
			method: 'get',
			url: id,
			baseURL: this.BASE_URL
		});
	}
	static updateFields(id, data){
		return axios({
			method: 'put',
			url: '/update/'+id,
			data: data,
			baseURL: this.BASE_URL
		});
	}
	static create(data){
		return axios({
			method: 'post',
			url: '',
			data: data,
			baseURL: this.BASE_URL
		}); 
	}
	static update(id, data){
		return axios({
			method: 'put',
			url: '/'+id,
			data: data,
			baseURL: this.BASE_URL
		}); 
	}

	static clone(id, data) {
		return axios({
			method: 'POST',
			url: '/clone/'+id,
			data: data,
			baseURL: this.BASE_URL
		})
	}

	static duplicate(id, data) {
		return axios({
			method: 'POST',
			url: '/duplicate/'+id,
			data: data,
			baseURL: this.BASE_URL
		})
	}

	static sendByEmail(id, pdfName) {
		return axios({
			method: 'POST',
			url: '/sendByMail/'+id,
			data: {pdfName: pdfName},
			baseURL: this.BASE_URL
		})
	}

	static destroy(id) {
		return axios({
			method: 'DELETE',
			url: '/'+id,
			data: {},
			baseURL: this.BASE_URL
		})
	}

	static computeBudget(manifestation, budget){

		var res: any = {};

		res.A1 = parseFloat(budget);
		res.A2 = manifestation.hasUnavailabiliytExtension ? manifestation.insuredBudget -  (manifestation.isFeeDeducted ? manifestation.artisteFee : 0) : 0;
		res.A3 = manifestation.hasBadWeatherExtension ? res.A1 * manifestation.percentBudgetToBadWeather / 100 : 0;
		res.A4 = manifestation.hasBombingExtension ? res.A1 : 0;

		res.B1 = manifestation.comprehensiveInsuranceRate || 0;
		res.B2 = manifestation.unavailabilityRate || 0;
		res.B3 = manifestation.badWeatherRate || 0;
		res.B4 = manifestation.bombingRate || 0;

		res.D1 = res.A1 * res.B1 / 100 * (parseFloat(manifestation.BNS )/ 100 || 0);
		res.D2 = res.A2 * res.B2 / 100 * (parseFloat(manifestation.BNS )/ 100 || 0);
		res.D3 = res.A3 * res.B3 / 100 * (parseFloat(manifestation.BNS )/ 100 || 0);
		res.D4 = res.A4 * res.B4 / 100 * (parseFloat(manifestation.BNS )/ 100 || 0);
		res.D5 = res.D1 + res.D2 + res.D3 + res.D4;


		res.C1 = res.A1 * res.B1 / 100 - res.D1;
		res.C2 = res.A2 * res.B2 / 100 - res.D2;
		res.C3 = res.A3 * res.B3 / 100 - res.D3;
		res.C4 = res.A4 * res.B4 / 100 - res.D4;
		res.C5 = res.C1 + res.C2 + res.C3 + res.C4;

		res.E5 = manifestation.ovationCommission / 100 * res.C5 || 0;

		res.F5 = manifestation.ovationBSNCommission / 100 * res.D5  || 0;


		res.G1 = res.C1 * parseFloat(manifestation.taxRate) / 100;
		res.G2 = res.C2 * parseFloat(manifestation.taxRate) / 100;
		res.G3 = res.C3 * parseFloat(manifestation.taxRate) / 100;
		res.G4 = res.C4 * parseFloat(manifestation.taxRate) / 100;
		res.G5 = res.C5 * parseFloat(manifestation.taxRate) / 100;

		res.H1 = res.D1 * parseFloat(manifestation.taxRate) / 100;
		res.H2 = res.D2 * parseFloat(manifestation.taxRate) / 100;
		res.H3 = res.D3 * parseFloat(manifestation.taxRate) / 100;
		res.H4 = res.D4 * parseFloat(manifestation.taxRate) / 100;
		res.H5 = res.D5 * parseFloat(manifestation.taxRate) / 100;

		res.I1 = res.G1 + res.C1;
		res.I2 = res.G2 + res.C2;
		res.I3 = res.G3 + res.C3;
		res.I4 = res.G4 + res.C4;
		res.I5 = res.G5 + res.C5;

		res.J1 = res.H1 + res.D1;
		res.J2 = res.H2 + res.D2;
		res.J3 = res.H3 + res.D3;
		res.J4 = res.H4 + res.D4;
		res.J5 = res.H5 + res.D5;

		res.K5 = res.I5;

		res.L5 = res.K5 - res.E5;

		res.notBNSContribution = res.K5;

		res.BNS = res.J5;

		res.wholeContribution = res.notBNSContribution + (res.BNS || 0);

		return res;






	}

}
