import axios from 'axios';
import {Zipcode} from "../models/Zipcode";

class ZipcodeParams {
    zipcode?:string;
    city?:string;
}

export class ZipcodeService {

    static BASE_URL = '/api/zipcode';

    static validate(zipcode) {

        return axios({
            method: 'post',
            url: '',
            data: {zipcode: zipcode},
            baseURL: this.BASE_URL
        })
    }

    static get(params:ZipcodeParams):Promise<Zipcode> {
        return axios({
            method: 'get',
            url: '',
            params: params,
            baseURL: this.BASE_URL
        }).then(res=>res.data);
    }

}