import axios from 'axios';

export class ParagraphService {

	static BASE_URL = '/api/paragraphs/';

	static find(query){
		return axios({
			method: 'get',
			url: '',
			params: query,
			baseURL: this.BASE_URL
		});
	}
	static insert(data){
		return axios({
			method: 'post',
			url: '',
			data: data,
			baseURL: this.BASE_URL
		});
	}
	static update(id, data){
		return axios({
			method: 'put',
			url: '/'+id,
			data: data,
			baseURL: this.BASE_URL
		});
	}
	static delete(id){
		return axios({
			method: 'delete',
			url: '/'+id,
			baseURL: this.BASE_URL
		});
	}
	static copyFrToEn(versionId:number, group:number){
		return axios({
			method: 'post',
			url: '/copyFrEn',
			data: {
				versionId: versionId,
                group: group,
			},
			baseURL: this.BASE_URL
		});
	}
	static copy(versionId){
		return axios({
			method: 'post',
			url: '/copy',
			data: { versionId: versionId },
			baseURL: this.BASE_URL
		});
	}
}