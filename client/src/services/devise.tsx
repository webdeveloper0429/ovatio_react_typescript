import axios from 'axios';

export class DevisesService {
    static BASE_URL = '/api/devises';
    static find():Promise<any> {
        return axios({
            method: 'get',
            url: '',
            baseURL: this.BASE_URL
        }).then(res=>res.data);
    }
}