import axios, {AxiosResponse} from 'axios'

export class CountryService {
    static _countries = axios.get('/api/countries').then((res:AxiosResponse) => res.data);
}