import axios from 'axios';

export class UserService {

	static BASE_URL = '/api/users/';
	static ADMIN_LABEL = 'ADMINISTRATOR';

	static promiseMe: Promise<any>;

	static getMe(){

		if(!this.promiseMe){
			this.promiseMe = axios({
				method: 'get',
				url: '/me',
				baseURL: this.BASE_URL
			});
		}
		return this.promiseMe;
	}

	static getOne(id){
		return axios({
			method: 'get',
			url: '/'+id,
			baseURL: this.BASE_URL
		});
	}

	static update(id, data){
		return axios({
			method: 'put',
			url: '/'+id,
			data: data,
			baseURL: this.BASE_URL
		})
	}

	static insert(data){
		return axios({
			method: 'post',
			url: '',
			data: data,
			baseURL: this.BASE_URL
		})
	}

	static findUsers(query){
		return axios({
			method: 'get',
			url: '',
			params: query,
			baseURL: this.BASE_URL
		});
	}

	static findUsersInOvatioGroup(){
		return axios({
			method: 'get',
			url: '',
			data: {
				isGroupOvation: true
			},
			baseURL: this.BASE_URL
		});
	}
	static findAccountManger(){
		return axios({
			method: 'get',
			url: '/accountManager',
			data: {
			},
			baseURL: this.BASE_URL
		});
	}

	static isAdmin(user) : boolean{
		if(user && user.role && user.role.label){
			return user.role.label == this.ADMIN_LABEL;
		} else{
			return false;
		}
	}

	static hasRight(user, rightToFound):boolean {
		if(!user) return false;
		let hasRight = false;
		user.role.rightList.map((right) => {
            if(right.label == rightToFound) {
            	hasRight = true
            }
        });
		return hasRight;
	}

}

export var SkipRole = [
	"COMPANY",
	"BUSINESS_GETTER",
	"Client",
	"EXPERT"
]