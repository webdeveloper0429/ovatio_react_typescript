import axios from 'axios';

export function getBusinessStatutses(){
	return axios({
		method: 'get',
		url: '',
		baseURL: "/api/client-businessStatuses/"
	});
}

export function getStates(){
	return [
		{id: 1, label: "Prospect"},
		{id: 2, label: "Prospect inactive"},
		{id: 3, label: "Client"},
		{id: 4, label: "Client inactif"},
		{id: 5, label: "Blacklist"},
		{id: 6, label: "Choisir"}
	]
}

export class ClientService {

	static BASE_URL = '/api/client/';

	static getAutocompleteClients(query){
		return axios({
			method: 'get',
			url: '/autocomplete',
			params: query,
			baseURL: this.BASE_URL
		});
	}
	static getOneClient(id){
		return axios({
			method: 'get',
			url: '/'+id,
			baseURL: this.BASE_URL
		});
	}
	static findClients(query){
		return axios({
			method: 'get',
			url: '',
			params: query,
			baseURL: this.BASE_URL
		});
	}
	static countClients(query){
		return axios({
			method: 'get',
			url: '/count',
			params: query,
			baseURL: this.BASE_URL
		});
	}
	static insert(data){
		return axios({
			method: 'post',
			url: '',
			data: data,
			baseURL: this.BASE_URL
		});
	}
	static update(id, data){
		return axios({
			method: 'put',
			url: '/'+id,
			data: data,
			baseURL: this.BASE_URL
		});
	}
	// static delete(id){
	// 	return axios({
	// 		method: 'delete',
	// 		url: '/'+id,
	// 		baseURL: this.BASE_URL
	// 	});
	// }
}
