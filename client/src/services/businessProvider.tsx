import axios from 'axios';

export class BusinessProviderService {

	static BASE_URL = '/api/businessProviders/';

	static find(query){
		return axios({
			method: 'get',
			url: '',
			params: query, 
			baseURL: this.BASE_URL
		});
	}
    static getById(id){
        return axios({
            method: 'get',
            url: '/'+id,
            baseURL: this.BASE_URL
        });
    }
	static insert(data){
		return axios({
			method: 'post',
			url: '',
			data: data,
			baseURL: this.BASE_URL
		});
	}
	static update(id, data){
		return axios({
			method: 'put',
			url: '/'+id,
			data: data,
			baseURL: this.BASE_URL
		})
	}
}