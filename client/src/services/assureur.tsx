import axios from 'axios';

export class AssureurService {

    static BASE_URL = '/api/insuranceCompany/';

    static count(query){
        return axios({
            method: 'get',
            url: '/count',
            params: query,
            baseURL: this.BASE_URL
        })
    }
    static getAssurreurById(id, populate = false){
        return axios({
            method: 'get',
            url: '/'+id,
            params: {
                populate: populate
            },
            baseURL: this.BASE_URL
        });
    }
    static find(query){
        return axios({
            method: 'get',
            url: '',
            params: query,
            baseURL: this.BASE_URL
        });
    }
    static insert(data){
        return axios({
            method: 'post',
            url: '',
            data: data,
            baseURL: this.BASE_URL
        })
    }
    static update(id, data){
        return axios({
            method: 'put',
            url: '/'+id,
            data: data,
            baseURL: this.BASE_URL
        })
    }
}